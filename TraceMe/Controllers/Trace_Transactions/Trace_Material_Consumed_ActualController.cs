﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.TraceTransactions;
using Model.Trace_Transactions;
using Repository;
namespace TraceMe.Controllers.Trace_Transactions
{
    [Route("api/[controller]")]
    [ApiController]
    public class Trace_Material_Consumed_ActualController : ControllerBase
    {
      
        private IRepositoryWrapper _repoWrapper;
        public Trace_Material_Consumed_ActualController( IRepositoryWrapper repowrapper)
        {
            _repoWrapper = repowrapper;
           
        }

        // GET: api/Trace_Material_Consumed_Actual
        [HttpGet]
        public async Task<IActionResult> GetTrace_Material_Consumed_Actual()
        {
            try
            {
                var result = await _repoWrapper.IMaterialConsumedActual.GetTrace_Material_Consumed_Actual();
                return Ok(result);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

          
        }

        // GET: api/Trace_Material_Consumed_Actual/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTrace_Material_Consumed_Actual(int id)
        {

            try
            {
                var trace_Material_Consumed_Actual = await _repoWrapper.IMaterialConsumedActual.GetTrace_Material_Consumed_Actual((id));
                if (trace_Material_Consumed_Actual == null)
                {
                    return NotFound();
                }
                return Ok(trace_Material_Consumed_Actual);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }

        // PUT: api/Trace_Material_Consumed_Actual/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTrace_Material_Consumed_Actual(int id, TraceMaterialConsumedActualDTO trace_Material_Consumed_Actual)
        {
            if (id != trace_Material_Consumed_Actual.MC_Id)
            {
                return BadRequest();
            }

          
            try
            {
                if (trace_Material_Consumed_Actual == null)

                {
                    return BadRequest("Object is null");
                }
                var ifexist = _repoWrapper.IMaterialConsumedActual.GetTrace_Material_Consumed_Actual(id);
                if (ifexist == null)
                {

                    return NotFound();
                }
                _repoWrapper.IMaterialConsumedActual.UpdateTrace_Material_Consumed_Actual(trace_Material_Consumed_Actual);
                await _repoWrapper.SaveAsync();
                return Ok(trace_Material_Consumed_Actual);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }

        // POST: api/Trace_Material_Consumed_Actual
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<IActionResult> PostTrace_Material_Consumed_Actual(TraceMaterialConsumedActualDTO trace_Material_Consumed_Actual)
        {
            try
            {
                if (trace_Material_Consumed_Actual == null)
                {
                    // _logger.LogError("Owner object sent from client is null.");
                    return BadRequest(" object is null");
                }
                _repoWrapper.IMaterialConsumedActual.CreateTrace_Material_Consumed_Actual(trace_Material_Consumed_Actual);
                await _repoWrapper.SaveAsync();
                return CreatedAtAction("GetTrace_Material_Consumed_Actual", new { id = trace_Material_Consumed_Actual.MC_Id }, trace_Material_Consumed_Actual);
            }
            catch (Exception ex)
            {
                // _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

          
        }

        // DELETE: api/Trace_Material_Consumed_Actual/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTrace_Material_Consumed_Actual(int id)
        {
            try
            {
                var ifexist = _repoWrapper.IMaterialConsumedActual.GetTrace_Material_Consumed_Actual(id);
                if (ifexist == null)
                {

                    return NotFound();
                }

                _repoWrapper.IMaterialConsumedActual.DeleteTrace_Material_Consumed_Actual(id);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

      
    }
}
