﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.FGS_FIFO;
using Model.Tread_FIFO;
using Repository;
using Entity.TreadFIFO;
namespace TraceMe.Controllers.Tread_FIFO
{
    [Route("api/[controller]")]
    [ApiController]
    public class Trace_Production_RemarksController : ControllerBase
    {

        private IRepositoryWrapper _repoWrapper;
        public Trace_Production_RemarksController(IRepositoryWrapper repowrapper)
        {
            _repoWrapper = repowrapper;

        }

        // GET: api/Trace_Production_Remarks
        [HttpGet]
        public async Task<IActionResult> GetTrace_Production_Remarks()
        {
            try
            {
                var result = await _repoWrapper.IProductionRemarks.GetTrace_Production_Remarks();
                return Ok(result);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }

        // GET: api/Trace_Production_Remarks/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTrace_Production_Remarks(int id)
        {


            try
            {
                var trace_Production_Remarks = await _repoWrapper.IProductionRemarks.GetTrace_Production_Remarks((id));
                if (trace_Production_Remarks == null)
                {
                    return NotFound();
                }
                return Ok(trace_Production_Remarks);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // PUT: api/Trace_Production_Remarks/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTrace_Production_Remarks(int id, TraceProductionRemarksDTO trace_Production_Remarks)
        {
            if (id != trace_Production_Remarks.Remark_Id)
            {
                return BadRequest();
            }


            try
            {
                if (trace_Production_Remarks == null)

                {
                    return BadRequest("Object is null");
                }
                var ifexist = _repoWrapper.IProductionRemarks.GetTrace_Production_Remarks(id);
                if (ifexist == null)
                {

                    return NotFound();
                }
                _repoWrapper.IProductionRemarks.UpdateTrace_Production_Remarks(trace_Production_Remarks);
                await _repoWrapper.SaveAsync();
                return Ok(trace_Production_Remarks);

            }

            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }


        }

        // POST: api/Trace_Production_Remarks
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<IActionResult> PostTrace_Production_Remarks(TraceProductionRemarksDTO trace_Production_Remarks)
        {

            try
            {
                if (trace_Production_Remarks == null)
                {
                    // _logger.LogError("Owner object sent from client is null.");
                    return BadRequest(" object is null");
                }
                _repoWrapper.IProductionRemarks.CreateTrace_Production_Remarks(trace_Production_Remarks);
                await _repoWrapper.SaveAsync();
                return CreatedAtAction("GetTrace_Production_Remarks", new { id = trace_Production_Remarks.Remark_Id }, trace_Production_Remarks);
            }
            catch (Exception ex)
            {
                // _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }



        }

        // DELETE: api/Trace_Production_Remarks/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTrace_Production_Remarks(int id)
        {
            try
            {
                var ifexist = _repoWrapper.IProductionRemarks.GetTrace_Production_Remarks(id);
                if (ifexist == null)
                {

                    return NotFound();
                }

                _repoWrapper.IProductionRemarks.DeleteTrace_Production_Remarks(id);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }
    }
}
