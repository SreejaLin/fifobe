﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.FGS_FIFO;
using Model.Tread_FIFO;
using Repository;
using Entity.TreadFIFO;
namespace TraceMe.Controllers.Tread_FIFO
{
    [Route("api/[controller]")]
    [ApiController]
    public class Trace_Status_MasterController : ControllerBase
    {
      
        private IRepositoryWrapper _repoWrapper;
        public Trace_Status_MasterController( IRepositoryWrapper repowrapper)
        {
            _repoWrapper = repowrapper;
          
        }

        // GET: api/Trace_Status_Master
        [HttpGet]
        public async Task<IActionResult> GetTrace_Status_Master()
        {
            try
            {
                var result = await _repoWrapper.IStatusMaster.GetTrace_Status_Master();
                return Ok(result);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
            
        }

        // GET: api/Trace_Status_Master/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTrace_Status_Master(int id)
        {
            try
            {
                var trace_Status_Master = await _repoWrapper.IStatusMaster.GetTrace_Status_Master((id));
                if (trace_Status_Master == null)
                {
                    return NotFound();
                }
                return Ok(trace_Status_Master);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
         
        }

        // PUT: api/Trace_Status_Master/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTrace_Status_Master(int id, TraceStatusMasterDTO trace_Status_Master)
        {
            if (id != trace_Status_Master.Status_Id)
            {
                return BadRequest();
            }

            try
            {
                if (trace_Status_Master == null)

                {
                    return BadRequest("Object is null");
                }
                var ifexist = _repoWrapper.IStatusMaster.GetTrace_Status_Master(id);
                if (ifexist == null)
                {

                    return NotFound();
                }
                _repoWrapper.IStatusMaster.UpdateTrace_Status_Master(trace_Status_Master);
                await _repoWrapper.SaveAsync();
                return Ok(trace_Status_Master);

            }
            catch (DbUpdateConcurrencyException)
            {
                return StatusCode(500, "Internal server error");
            }

           
        }

        // POST: api/Trace_Status_Master
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<IActionResult> PostTrace_Status_Master(TraceStatusMasterDTO trace_Status_Master)
        {

            try
            {
                if (trace_Status_Master == null)
                {
                    // _logger.LogError("Owner object sent from client is null.");
                    return BadRequest("Owner object is null");
                }
                _repoWrapper.IStatusMaster.CreateTrace_Status_Master(trace_Status_Master);
                await _repoWrapper.SaveAsync();
                return CreatedAtAction("GetTrace_statusmaster", new { id = trace_Status_Master.Status_Id }, trace_Status_Master);
            }
            catch (Exception ex)
            {
                // _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
           
        }

        //// DELETE: api/Trace_Status_Master/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTrace_Taken_ToTB(int id)
        {

            try
            {
                var ifexist = _repoWrapper.IStatusMaster.GetTrace_Status_Master(id);
                if (ifexist == null)
                {

                    return NotFound();
                }

                _repoWrapper.IStatusMaster.DeleteTrace_Status_Master(id);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }

    }
}
