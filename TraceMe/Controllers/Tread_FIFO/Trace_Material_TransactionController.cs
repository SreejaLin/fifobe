﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.FGS_FIFO;
using Model.Tread_FIFO;
using Repository;
using Entity.TreadFIFO;
namespace TraceMe.Controllers.Tread_FIFO
{
    [Route("api/[controller]")]
    [ApiController]
    public class Trace_Material_TransactionController : ControllerBase
    {

        private IRepositoryWrapper _repoWrapper;
        public Trace_Material_TransactionController(Trace_DBContext context, IRepositoryWrapper repowrapper)
        {
            _repoWrapper = repowrapper;

        }

        // GET: api/Trace_Material_Transaction
        [HttpGet]
        public async Task<IActionResult> GetTrace_Material_Transaction()
        {
            try
            {
                var trace_Material_Transaction = await _repoWrapper.IMaterialTransaction.GetTrace_Material_Transaction();
                return Ok(trace_Material_Transaction);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // GET: api/Trace_Material_Transaction/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTrace_Material_Transaction(int id)
        {

            try
            {
                var trace_Material_Transaction = await _repoWrapper.IMaterialTransaction.GetTrace_Material_Transaction((id));
                if (trace_Material_Transaction == null)
                {
                    return NotFound();
                }
                return Ok(trace_Material_Transaction);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }


        }

        // PUT: api/Trace_Material_Transaction/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTrace_Material_Transaction(int id, TraceMaterialTransactionDTO trace_Material_Transaction)
        {
            if (id != trace_Material_Transaction.Material_Transaction_Id)
            {
                return BadRequest();
            }



            try
            {


                if (trace_Material_Transaction == null)

                {
                    return BadRequest("Object is null");
                }
                var ifexist = _repoWrapper.IMaterialTransaction.GetTrace_Material_Transaction(id);
                if (ifexist == null)
                {

                    return NotFound();
                }
                _repoWrapper.IMaterialTransaction.UpdateTrace_Material_Transaction(trace_Material_Transaction);
                await _repoWrapper.SaveAsync();
                return Ok(trace_Material_Transaction);
            }
            catch (DbUpdateConcurrencyException)
            {
                return StatusCode(500, "Internal server error");
            }


        }

        // POST: api/Trace_Material_Transaction
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<IActionResult> PostTrace_Material_Transaction(Trace_Material_Transaction trace_Material_Transaction)
        {

            if (trace_Material_Transaction == null)
            {
                // _logger.LogError("Owner object sent from client is null.");
                return BadRequest(" object is null");
            }
            try
            {
                _repoWrapper.IMaterialTransaction.Create(trace_Material_Transaction);
                await _repoWrapper.SaveAsync();
                return CreatedAtAction("GetTrace_Taken_ToTB", new { id = trace_Material_Transaction.Material_Transaction_Id }, trace_Material_Transaction);
            }

            catch (Exception ex)
            {
                // _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }




        }

        // DELETE: api/Trace_Material_Transaction/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTrace_Material_Transaction(int id)
        {

            try
            {
                var ifexist = _repoWrapper.IMaterialTransaction.GetTrace_Material_Transaction(id);
                if (ifexist == null)
                {

                    return NotFound();
                }

                _repoWrapper.IMaterialTransaction.DeleteTrace_Material_Transaction(id);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }
    }
}
