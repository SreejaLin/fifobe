﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.FGS_FIFO;
using Model.Tread_FIFO;
using Repository;
using Entity.TreadFIFO;
namespace TraceMe.Controllers.Tread_FIFO
{
    [Route("api/[controller]")]
    [ApiController]
    public class Trace_Product_AgeingController : ControllerBase
    {
      
        private IRepositoryWrapper _repoWrapper;
        public Trace_Product_AgeingController( IRepositoryWrapper repowrapper)
        {
            _repoWrapper = repowrapper;
          
        }

        // GET: api/Trace_Product_Ageing
        [HttpGet]
        public async Task<IActionResult> GetTrace_Product_Ageing()
        {
            try
            {
                var result = await _repoWrapper.IProductAgeing.GetTrace_Product_Ageing();
                return Ok(result);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
         
        }

        // GET: api/Trace_Product_Ageing/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTrace_Product_Ageing(int id)
        {
            try
            {
                var trace_Product_Ageing = await _repoWrapper.IProductAgeing.GetTrace_Product_Ageing((id));
                if (trace_Product_Ageing == null)
                {
                    return NotFound();
                }
                return Ok(trace_Product_Ageing);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
           
        }

        // PUT: api/Trace_Product_Ageing/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTrace_Product_Ageing(int id, TraceProductAgeingDTO trace_Product_Ageing)
        {
            if (id != trace_Product_Ageing.Product_Ageing_Id)
            {
                return BadRequest();
            }

            

            try
            {
                if (trace_Product_Ageing == null)

                {
                    return BadRequest("Object is null");
                }
                var ifexist = _repoWrapper.IProductAgeing.GetTrace_Product_Ageing(id);
                if (ifexist == null)
                {

                    return NotFound();
                }
                _repoWrapper.IProductAgeing.UpdateTrace_Product_Ageing(trace_Product_Ageing);
               await _repoWrapper.SaveAsync();
                return Ok(trace_Product_Ageing);
            }
            catch (DbUpdateConcurrencyException)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }


        }

        // POST: api/Trace_Product_Ageing
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<IActionResult> PostTrace_Product_Ageing(TraceProductAgeingDTO trace_Product_Ageing)
        {
            try
            {
                if (trace_Product_Ageing == null)
                {
                    // _logger.LogError("Owner object sent from client is null.");
                    return BadRequest(" object is null");
                }
                _repoWrapper.IProductAgeing.CreateTrace_Product_Ageing(trace_Product_Ageing);
                await _repoWrapper.SaveAsync();
                return CreatedAtAction("GetTrace_Product_Ageing", new { id = trace_Product_Ageing.Product_Ageing_Id }, trace_Product_Ageing);
            }
            catch (Exception ex)
            {
                // _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

          
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTrace_Product_Ageing(int id)
        {

            try
            {
                var ifexist = _repoWrapper.IProductAgeing.GetTrace_Product_Ageing(id);
                if (ifexist == null)
                {

                    return NotFound();
                }

                _repoWrapper.IProductAgeing.DeleteTrace_Product_Ageing(id);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }

    }
}
