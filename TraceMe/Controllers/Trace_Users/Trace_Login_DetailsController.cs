﻿using System;

using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

using Entity.TraceUsers;

using Repository;
namespace TraceMe.Controllers.Trace_Users
{
    [Route("api/[controller]")]

    public class Trace_Login_DetailsController : ControllerBase
    {
        
        private IRepositoryWrapper _repoWrapper;
        public Trace_Login_DetailsController( IRepositoryWrapper repowrapper)
        {
            _repoWrapper = repowrapper;
          
        }

        // GET: api/Trace_Production
        [HttpGet]
        public async Task<IActionResult> GetTrace_Login_Details()
        {
            try
            {
                var result = await _repoWrapper.ILoginDetails.GetTrace_Login_Details();
                return Ok(result);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // GET: api/Trace_Production/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTrace_Login_Details(int id)
        {

            try
            {
                var trace_logindetails = await _repoWrapper.ILoginDetails.GetTrace_Login_Details(id);
                if (trace_logindetails == null)
                {
                    return NotFound();
                }
                return Ok(trace_logindetails);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        
        }

        // PUT: api/Trace_Production/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTrace_Login_Details(int id, Trace_Login_DetailsDTO trace_logindetails)
        {
            if (id != trace_logindetails.Id)
            {
                return BadRequest();
            }

          

            try
            {
                if (trace_logindetails == null)

                {
                    return BadRequest("Object is null");
                }
                var ifexist = _repoWrapper.ILoginDetails.GetTrace_Login_Details(id);
                if (ifexist == null)
                {

                    return NotFound();
                }
                _repoWrapper.ILoginDetails.UpdateTrace_Login_Details(trace_logindetails);
                await _repoWrapper.SaveAsync();
                return Ok(trace_logindetails);

            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");

            }
        }

            
        

        // POST: api/Trace_Production
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<IActionResult> PostTrace_Application_Login_Details(Trace_Login_DetailsDTO trace_logindetails)
        {
            try
            {
                if (trace_logindetails == null)
                {
                    // _logger.LogError("Owner object sent from client is null.");
                    return BadRequest(" object is null");
                }
                _repoWrapper.ILoginDetails.CreateTrace_Login_Details(trace_logindetails);
                await _repoWrapper.SaveAsync();
                return CreatedAtAction("GetTrace_Production", new { id = trace_logindetails.Id }, trace_logindetails);
            }
            catch (Exception ex)
            {
                // _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

         
        }

        // DELETE: api/Trace_Production/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTrace_Application_Login_Details(int id)
        {

            try
            {
                var ifexist = _repoWrapper.ILoginDetails.GetTrace_Login_Details(id);
                if (ifexist == null)
                {

                    return NotFound();
                }

                _repoWrapper.ILoginDetails.DeleteTrace_Login_Details(id);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

    }
}
