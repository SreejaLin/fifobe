﻿using System;

using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

using Entity.TraceUsers;

using Repository;
namespace TraceMe.Controllers.Trace_Users
{
    [Route("api/[controller]")]

    public class Trace_Application_Employee_MappingController : ControllerBase
    {
        
        private IRepositoryWrapper _repoWrapper;
        public Trace_Application_Employee_MappingController( IRepositoryWrapper repowrapper)
        {
            _repoWrapper = repowrapper;
          
        }

        // GET: api/Trace_Production
        [HttpGet]
        public async Task<IActionResult> GetTrace_Application_Employee_Mapping()
        {
            try
            {
                var result = await _repoWrapper.IApplicationEmployeeMapping.GetTrace_Application_Employee_Mapping();
                return Ok(result);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // GET: api/Trace_Production/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTrace_Application_Employee_Mapping(int id)
        {

            try
            {
                var trace_AppEmpMapping = _repoWrapper.IApplicationEmployeeMapping.GetTrace_Application_Employee_Mapping(id);
                if (trace_AppEmpMapping == null)
                {
                    return NotFound();
                }
                return Ok(trace_AppEmpMapping);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        
        }

        // PUT: api/Trace_Production/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTrace_Application_Employee_Mapping(int id, Trace_Application_Employee_MappingDTO trace_AppEmpMapping)
        {
            if (id != trace_AppEmpMapping.Id)
            {
                return BadRequest();
            }

          

            try
            {
                if (trace_AppEmpMapping == null)

                {
                    return BadRequest("Object is null");
                }
                var ifexist = _repoWrapper.IApplicationEmployeeMapping.GetTrace_Application_Employee_Mapping(id);
                if (ifexist == null)
                {

                    return NotFound();
                }
                _repoWrapper.IApplicationEmployeeMapping.UpdateTrace_Application_Employee_Mapping(trace_AppEmpMapping);
                await _repoWrapper.SaveAsync();
                return Ok(trace_AppEmpMapping);

            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");

            }
        }

            
        

        // POST: api/Trace_Production
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<IActionResult> PostTrace_Application_Employee_Mapping(Trace_Application_Employee_MappingDTO trace_AppEmpMapping)
        {
            try
            {
                if (trace_AppEmpMapping == null)
                {
                    // _logger.LogError("Owner object sent from client is null.");
                    return BadRequest(" object is null");
                }
                _repoWrapper.IApplicationEmployeeMapping.CreateTrace_Application_Employee_Mapping(trace_AppEmpMapping);
                await _repoWrapper.SaveAsync();
                return CreatedAtAction("GetTrace_Production", new { id = trace_AppEmpMapping.Id }, trace_AppEmpMapping);
            }
            catch (Exception ex)
            {
                // _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

         
        }

        // DELETE: api/Trace_Production/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTrace_Application_Employee_Mapping(int id)
        {

            try
            {
                var ifexist = _repoWrapper.IApplicationEmployeeMapping.GetTrace_Application_Employee_Mapping(id);
                if (ifexist == null)
                {

                    return NotFound();
                }

                _repoWrapper.IApplicationEmployeeMapping.DeleteTrace_Application_Employee_Mapping(id);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

    }
}
