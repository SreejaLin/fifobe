﻿using System;

using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

using Entity.TraceUsers;

using Repository;
namespace TraceMe.Controllers.Trace_Users
{
    [Route("api/[controller]")]

    public class Trace_Module_MasterController : ControllerBase
    {
        
        private IRepositoryWrapper _repoWrapper;
        public Trace_Module_MasterController( IRepositoryWrapper repowrapper)
        {
            _repoWrapper = repowrapper;
          
        }

        // GET: api/Trace_Production
        [HttpGet]
        public async Task<IActionResult> GetTrace_Module_Master()
        {
            try
            {
                var result = await _repoWrapper.IModuleMaster.GetTrace_Module_Master();
                return Ok(result);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // GET: api/Trace_Production/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTrace_Module_Master(int id)
        {

            try
            {
                var trace_moduleMaster = await _repoWrapper.IModuleMaster.GetTrace_Module_Master(id);
                if (trace_moduleMaster == null)
                {
                    return NotFound();
                }
                return Ok(trace_moduleMaster);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        
        }

        // PUT: api/Trace_Production/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTrace_Module_Master(int id, Trace_Module_MasterDTO trace_moduleMaster)
        {
            if (id != trace_moduleMaster.Id)
            {
                return BadRequest();
            }

          

            try
            {
                if (trace_moduleMaster == null)

                {
                    return BadRequest("Object is null");
                }
                var ifexist = _repoWrapper.IModuleMaster.GetTrace_Module_Master(id);
                if (ifexist == null)
                {

                    return NotFound();
                }
                _repoWrapper.IModuleMaster.UpdateTrace_Module_Master(trace_moduleMaster);
                await _repoWrapper.SaveAsync();
                return Ok(trace_moduleMaster);

            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");

            }
        }

            
        

        // POST: api/Trace_Production
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<IActionResult> PostTrace_Application_Module_Master(Trace_Module_MasterDTO trace_moduleMaster)
        {
            try
            {
                if (trace_moduleMaster == null)
                {
                    // _logger.LogError("Owner object sent from client is null.");
                    return BadRequest(" object is null");
                }
                _repoWrapper.IModuleMaster.CreateTrace_Module_Master(trace_moduleMaster);
                await _repoWrapper.SaveAsync();
                return CreatedAtAction("GetTrace_Production", new { id = trace_moduleMaster.Id }, trace_moduleMaster);
            }
            catch (Exception ex)
            {
                // _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

         
        }

        // DELETE: api/Trace_Production/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTrace_Application_Module_Master(int id)
        {

            try
            {
                var ifexist = _repoWrapper.IModuleMaster.GetTrace_Module_Master(id);
                if (ifexist == null)
                {

                    return NotFound();
                }

                _repoWrapper.IModuleMaster.DeleteTrace_Module_Master(id);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

    }
}
