﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.TraceMasters;
using Model.Trace_Masters;
using Repository;

namespace TraceMe.Controllers.Trace_Masters
{
    [Route("api/[controller]")]
    [ApiController]
    public class Trace_MHE_MasterController : ControllerBase
    {

        private IRepositoryWrapper _repoWrapper;
        public Trace_MHE_MasterController(IRepositoryWrapper repowrapper)
        {
            _repoWrapper = repowrapper;

        }

        // GET: api/Trace_MHE_Master
        [HttpGet]
        public async Task<IActionResult> GetTrace_MHE_Master()
        {
            try
            {
                var result = await _repoWrapper.IMHEMaster.GetTrace_MHE_Master();
                return Ok(result);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // GET: api/Trace_MHE_Master/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTrace_MHE_Master(int id)
        {
            try
            {
                var trace_MHE_Master = await _repoWrapper.IMHEMaster.GetTrace_MHE_Master((id));
                if (trace_MHE_Master == null)
                {
                    return NotFound();
                }
                return Ok(trace_MHE_Master);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // PUT: api/Trace_MHE_Master/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTrace_MHE_Master(int id, TraceMHEMasterDTO trace_MHE_Master)
        {
            if (id != trace_MHE_Master.MHE_Id)
            {
                return BadRequest();
            }



            try
            {
                if (trace_MHE_Master == null)

                {
                    return BadRequest("Object is null");
                }
                var ifexist = _repoWrapper.IMHEMaster.GetTrace_MHE_Master(id);
                if (ifexist == null)
                {

                    return NotFound();
                }
                _repoWrapper.IMHEMaster.UpdateTrace_MHE_Master(trace_MHE_Master);
                await _repoWrapper.SaveAsync();
                return Ok(trace_MHE_Master);



            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }


        }

        // POST: api/Trace_MHE_Master
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Trace_MHE_Master>> PostTrace_MHE_Master(TraceMHEMasterDTO trace_MHE_Master)
        {

            try
            {
                if (trace_MHE_Master == null)
                {
                    // _logger.LogError("Owner object sent from client is null.");
                    return BadRequest(" object is null");
                }
                _repoWrapper.IMHEMaster.CreateTrace_MHE_Master(trace_MHE_Master);
                await _repoWrapper.SaveAsync();
                return CreatedAtAction("GetTrace_Material_Consumed_Spec", new { id = trace_MHE_Master.MHE_Id }, trace_MHE_Master);
            }
            catch (Exception ex)
            {
                // _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }

        // DELETE: api/Trace_MHE_Master/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTrace_MHE_Master(int id)
        {
            try
            {
                var ifexist = _repoWrapper.IMHEMaster.GetTrace_MHE_Master(id);
                if (ifexist == null)
                {

                    return NotFound();
                }

                _repoWrapper.IMHEMaster.DeleteTrace_MHE_Master(id);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }
    }
}
