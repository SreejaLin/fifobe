﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.TraceMasters;
using Model.Trace_Masters;
using Repository;

namespace TraceMe.Controllers.Trace_Masters
{
    [Route("api/[controller]")]
    [ApiController]
    public class Trace_Plant_MasterController : ControllerBase
    {
        
        private IRepositoryWrapper _repoWrapper;
        public Trace_Plant_MasterController( IRepositoryWrapper repowrapper)
        {
            _repoWrapper = repowrapper;
          
        }

        // GET: api/Trace_Plant_Master
        [HttpGet]
        public async Task<IActionResult> GetTrace_Plant_Master()
        {
            try
            {
                var result = await _repoWrapper.IPlantMaster.GetTrace_Plant_Master();
                return Ok(result);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // GET: api/Trace_Plant_Master/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Trace_Plant_Master>> GetTrace_Plant_Master(int id)
        {



            try
            {
                var trace_Plant_Master = await _repoWrapper.IPlantMaster.GetTrace_Plant_Master((id));
                if (trace_Plant_Master == null)
                {
                    return NotFound();
                }
                return Ok(trace_Plant_Master);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

            
        }

        // PUT: api/Trace_Plant_Master/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTrace_Plant_Master(int id, TracePlantMasterDTO trace_Plant_Master)
        {
            if (id != trace_Plant_Master.Plant_Id)
            {
                return BadRequest();
            }

            try
            {

                if (trace_Plant_Master == null)

                {
                    return BadRequest("Object is null");
                }
                var ifexist = _repoWrapper.IPlantMaster.GetTrace_Plant_Master(id);
                if (ifexist == null)
                {

                    return NotFound();
                }
                _repoWrapper.IPlantMaster.UpdateTrace_Plant_Master(trace_Plant_Master);
                await _repoWrapper.SaveAsync();
                return Ok(trace_Plant_Master);


            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }


        }

        // POST: api/Trace_Plant_Master
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<IActionResult> PostTrace_Plant_Master(TracePlantMasterDTO trace_Plant_Master)
        {
            try
            {
                if (trace_Plant_Master == null)
                {
                    // _logger.LogError("Owner object sent from client is null.");
                    return BadRequest(" object is null");
                }
                _repoWrapper.IPlantMaster.CreateTrace_Plant_Master(trace_Plant_Master);
                await _repoWrapper.SaveAsync();
                return CreatedAtAction("GetTrace_Plant_Master", new { id = trace_Plant_Master.Plant_Id }, trace_Plant_Master);
            }
            catch (Exception ex)
            {
                // _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }

        // DELETE: api/Trace_Plant_Master/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTrace_Plant_Master(int id)
        {
            try
            {
                var ifexist = _repoWrapper.IPlantMaster.GetTrace_Plant_Master(id);
                if (ifexist == null)
                {

                    return NotFound();
                }

                _repoWrapper.IPlantMaster.DeleteTrace_Plant_Master(id);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

    }
}
