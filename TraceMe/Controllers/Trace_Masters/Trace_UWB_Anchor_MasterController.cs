﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.TraceMasters;
using Model.Trace_Masters;
using Repository;
namespace TraceMe.Controllers.Trace_Masters
{
    [Route("api/[controller]")]
    [ApiController]
    public class Trace_UWB_Anchor_MasterController : ControllerBase
    {
       
        private IRepositoryWrapper _repoWrapper;
        public Trace_UWB_Anchor_MasterController( IRepositoryWrapper repowrapper)
        {
            _repoWrapper = repowrapper;
          
        }

        // GET: api/Trace_UWB_Anchor_Master
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Trace_UWB_Anchor_Master>>> GetTrace_UWB_Anchor_Master()
        {
            try
            {
                var result = await _repoWrapper.IUWBAnchorMaster.GetTrace_UWB_Anchor_Master();
                return Ok(result);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
            
        }

        // GET: api/Trace_UWB_Anchor_Master/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTrace_UWB_Anchor_Master(int id)
        {


            try
            {
                var trace_UWB_Anchor_Master = await _repoWrapper.IUWBAnchorMaster.GetTrace_UWB_Anchor_Master((id));
                if (trace_UWB_Anchor_Master == null)
                {
                    return NotFound();
                }
                return Ok(trace_UWB_Anchor_Master);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

       
        
    }

        // PUT: api/Trace_UWB_Anchor_Master/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTrace_UWB_Anchor_Master(int id, TraceUWBAnchorMasterDTO trace_UWB_Anchor_Master)
        {
            if (id != trace_UWB_Anchor_Master.UWB_Anchor_Id)
            {
                return BadRequest();
            }

      

            try
            {
                if (trace_UWB_Anchor_Master == null)

                {
                    return BadRequest("Object is null");
                }
                var ifexist = _repoWrapper.IUWBAnchorMaster.GetTrace_UWB_Anchor_Master(id);
                if (ifexist == null)
                {

                    return NotFound();
                }
                _repoWrapper.IUWBAnchorMaster.UpdateTrace_UWB_Anchor_Master(trace_UWB_Anchor_Master);
                await _repoWrapper.SaveAsync();
                return Ok(trace_UWB_Anchor_Master);


            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }


        }

        // POST: api/Trace_UWB_Anchor_Master
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<IActionResult> PostTrace_UWB_Anchor_Master(TraceUWBAnchorMasterDTO trace_UWB_Anchor_Master)
        {
            try
            {
                if (trace_UWB_Anchor_Master == null)
                {
                    // _logger.LogError("Owner object sent from client is null.");
                    return BadRequest(" object is null");
                }
                _repoWrapper.IUWBAnchorMaster.CreateTrace_UWB_Anchor_Master(trace_UWB_Anchor_Master);
                await _repoWrapper.SaveAsync();
                return CreatedAtAction("GetTrace_UWB_Anchor_Master", new { id = trace_UWB_Anchor_Master.UWB_Anchor_Id }, trace_UWB_Anchor_Master);
            }
            catch (Exception ex)
            {
                // _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
          
        }

        // DELETE: api/Trace_UWB_Anchor_Master/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTrace_UWB_Anchor_Master(int id)
        {
            try
            {
                var ifexist = _repoWrapper.IUWBAnchorMaster.GetTrace_UWB_Anchor_Master(id);
                if (ifexist == null)
                {

                    return NotFound();
                }

                _repoWrapper.IUWBAnchorMaster.DeleteTrace_UWB_Anchor_Master(id);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

    }
}
