﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.TraceMasters;
using Model.Trace_Masters;
using Repository;

namespace TraceMe.Controllers.Trace_Masters
{
    [Route("api/[controller]")]
    [ApiController]
    public class Trace_Storage_Location_MasterController : ControllerBase
    {
       
        private IRepositoryWrapper _repoWrapper;
        public Trace_Storage_Location_MasterController( IRepositoryWrapper repowrapper)
        {
            _repoWrapper = repowrapper;
           
        }

        // GET: api/Trace_Storage_Location_Master
        [HttpGet]
        public async Task<IActionResult> GetTrace_Storage_Location_Master()
        {
            try
            {
                var result = await _repoWrapper.IStorageLocationMaster.GetTrace_Storage_Location_Master();
                return Ok(result);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // GET: api/Trace_Storage_Location_Master/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTrace_Storage_Location_Master(int id)
        {

            try
            {
                var trace_Storage_Location_Master = await _repoWrapper.IStorageLocationMaster.GetTrace_Storage_Location_Master((id));
                if (trace_Storage_Location_Master == null)
                {
                    return NotFound();
                }
                return Ok(trace_Storage_Location_Master);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }

        // PUT: api/Trace_Storage_Location_Master/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public  async Task<IActionResult>  PutTrace_Storage_Location_Master(int id, TraceStorageLocationMasterDTO trace_Storage_Location_Master)
        {
            if (id != trace_Storage_Location_Master.Storage_Loc_Id)
            {
                return BadRequest();
            }


            try
            {
                if (trace_Storage_Location_Master == null)

                {
                    return BadRequest("Object is null");
                }
                var ifexist = _repoWrapper.IMaterialConsumedSpec.GetTrace_Material_Consumed_Spec(id);
                if (ifexist == null)
                {

                    return NotFound();
                }
                _repoWrapper.IStorageLocationMaster.UpdateTrace_Storage_Location_Master(trace_Storage_Location_Master);
                await _repoWrapper.SaveAsync();
                return Ok(trace_Storage_Location_Master);


            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // POST: api/Trace_Storage_Location_Master
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<IActionResult> PostTrace_Storage_Location_Master(TraceStorageLocationMasterDTO trace_Storage_Location_Master)
        {

            try
            {
                if (trace_Storage_Location_Master == null)
                {
                    // _logger.LogError("Owner object sent from client is null.");
                    return BadRequest(" object is null");
                }
                _repoWrapper.IStorageLocationMaster.CreateTrace_Storage_Location_Master(trace_Storage_Location_Master);
                await _repoWrapper.SaveAsync();
                return CreatedAtAction("GetTrace_Storage_Location_Master", new { id = trace_Storage_Location_Master.Storage_Loc_Id }, trace_Storage_Location_Master);
            }
            catch (Exception ex)
            {
                // _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

           
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTrace_Storage_Location_Master(int id)
        {
            try
            {
                var ifexist = _repoWrapper.IStorageLocationMaster.GetTrace_Storage_Location_Master(id);
                if (ifexist == null)
                {

                    return NotFound();
                }

                _repoWrapper.IStorageLocationMaster.DeleteTrace_Storage_Location_Master(id);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }

    }
}
