﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.FGSFIFO;
using Model.FGS_FIFO;
using Repository;

namespace TraceMe
{
    [Route("api/[controller]")]
    [ApiController]
    public class FGS_FIFO_MaterialType_MasterController : ControllerBase
    {
        
         private IRepositoryWrapper _repoWrapper;

        public FGS_FIFO_MaterialType_MasterController( IRepositoryWrapper repowrapper)
        {
          
            _repoWrapper = repowrapper;
        }

        
        //Modified HttpGet
        [HttpGet]
        public async Task<IActionResult> GetFGS_FIFO_MaterialType_Master()
        {
            try
            {
                var result = await _repoWrapper.IMaterialType_Master.GetFGS__Material_Master();
                return Ok(result);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
          
        }

        

        [HttpGet("{id}")]
        public async Task<IActionResult> GetFGS_FIFO_MaterialType_Master(int id)
        {

            try
            {
                var fGS_FIFO_MaterialType_Master = await _repoWrapper.IMaterialType_Master.GetFGS__Material_Master((id));
                if (fGS_FIFO_MaterialType_Master == null)
                {
                    return NotFound();
                }
                return Ok(fGS_FIFO_MaterialType_Master);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }



          
        }

      
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFGS_FIFO_MaterialType_Master(int id, MaterialTypeMasterDTO fGS_FIFO_MaterialType_Master)
        {
            if (id != fGS_FIFO_MaterialType_Master.Material_Type_Id)
            {
                return BadRequest();
            }

        
            try
            {
                if (fGS_FIFO_MaterialType_Master == null)

                {
                    return BadRequest("Object is null");
                }
                var ifexist = _repoWrapper.IMaterialType_Master.GetFGS__Material_Master(id);
                if (ifexist == null)
                {

                    return NotFound();
                }
                _repoWrapper.IMaterialType_Master.UpdateFGS__Material_Master(fGS_FIFO_MaterialType_Master);
                await _repoWrapper.SaveAsync();
                return Ok(fGS_FIFO_MaterialType_Master);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

            // return NoContent();
        }

        
        [HttpPost]
        public async Task<IActionResult> PostFGS_FIFO_MaterialType_Master([FromBody] MaterialTypeMasterDTO model)
        {


            try
            {
                if (model == null)
                {
                    // _logger.LogError("Owner object sent from client is null.");
                    return BadRequest(" object is null");
                }
                _repoWrapper.IMaterialType_Master.CreateFGS__Material_Master(model);
                await _repoWrapper.SaveAsync();
                return CreatedAtAction("GetFGS_FIFO_MaterialType_Master", new { id = model.Material_Type_Id }, model);
            }
            catch (Exception ex)
            {
                // _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }


        [HttpDelete("{id}")]

        public async Task<IActionResult> DeleteFGS_FIFO_Customer_Master(int id)
        {

            try
            {
                //var ifexist = _repoWrapper.IMaterialType_Master.GetFGS__Material_Master(id);
                //if (ifexist == null)
                //{

                //    return NotFound();
                //}

                _repoWrapper.IMaterialType_Master.DeleteFGS__Material_Master(id);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
       
    }
}
