﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.FGSFIFO;
using Model.FGS_FIFO;
using Repository;

namespace TraceMe
{
    [Route("api/[controller]")]
    [ApiController]
    public class FGS_FIFO_Despatch_RequestController : ControllerBase
    {
        // private readonly Trace_DBContext _context;
        private IRepositoryWrapper _repoWrapper;

        public FGS_FIFO_Despatch_RequestController(IRepositoryWrapper repowrapper)
        {

            _repoWrapper = repowrapper;
        }


        [HttpGet]
        public async Task<IActionResult> GetFGS_FIFO_Despatch_Request()
        {
            try
            {
                var result = await _repoWrapper.IDespatch_Request.GetFGS_Despatch_Request();
                foreach (var despid in result)
                {
                   

                    var totdesp = _repoWrapper.IDaily_DespatchTranscation.Get_tot_despatchedForRequestId(despid.Desp_Req_Id);
                    despid.Totdespatched = totdesp;

               
                   if (despid.Quantity == totdesp)
                    {
                      despid.Request_Status =0;
                        despid.statusName = "Despatch Complete";
                    }
                   else if (totdesp == 0)
                    {
                        despid.statusName = "Open";
                    }
                    else if (totdesp < despid.Quantity)
                    {
                        despid.statusName = "Partially Despatched";
                    }
                   //find location in receipt transcation for matid
                    var locations = "";
                    locations = _repoWrapper.IDaily_ReceiptTranscation.GetLocationsMatID(despid.Material_Id);
                    despid.Locations = locations;
                    //find barcode in receipt transcation for matid
                    var barcode = "";
                    barcode = _repoWrapper.IDaily_ReceiptTranscation.GetBarcodeforMatID(despid.Material_Id);
                    despid.Barcode = barcode;
                    despid.ReqNDespacthed = string.Concat(despid.Quantity, "/", despid.Totdespatched);

                }

                return Ok(result.OrderByDescending(x => x.Requested_DateTime).ToList());
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }




        [HttpGet("{id}")]
        public async Task<IActionResult> GetFGS_FIFO_Despatch_Request(int id)
        {

            try
            {
                var fGS_FIFO_Despatch_Request = await _repoWrapper.IDespatch_Request.GetFGS_Despatch_Request((id));
                if (fGS_FIFO_Despatch_Request == null)
                {
                    return NotFound();
                }
                return Ok(fGS_FIFO_Despatch_Request);

            }
            catch (Exception ex)
            {
               // _logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }


        [HttpPut("{id}")]
        public async Task<IActionResult> PutFGS_FIFO_Despatch_Request(int id, DailyDespatchRequestDTO fGS_FIFO_Despatch_Request)
        {
            if (id != fGS_FIFO_Despatch_Request.Desp_Req_Id)
            {
                return BadRequest();
            }


            try
            {
                if (fGS_FIFO_Despatch_Request == null)

                {
                    return BadRequest("Object is null");
                }
                var ifexist = _repoWrapper.IDespatch_Request.GetFGS_Despatch_Request(id);
                if (ifexist == null)
                {

                    return NotFound();
                }
                _repoWrapper.IDespatch_Request.UpdateFGS_Despatch_Request(fGS_FIFO_Despatch_Request);
                await _repoWrapper.SaveAsync();
                return Ok(fGS_FIFO_Despatch_Request);


            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

            // return NoContent();
        }


        [HttpPost]
        public async Task<IActionResult> PostFGS_FIFO_Despatch_Request([FromBody] DailyDespatchRequestDTO model)
        {

            try
            {
                if (model == null)
                {
                    // _logger.LogError("Owner object sent from client is null.");
                    return BadRequest(" object is null");
                }

                int SuppliedStock = _repoWrapper.IDaily_ReceiptTranscation.Get_Supplied_Stock(model.Material_Id);
                int totdesptchreqMID = _repoWrapper.IDespatch_Request.TotQuantity_Despatch_Request_Placed_For_MID(model.Material_Id);
                int availablestock = SuppliedStock - totdesptchreqMID;
                if (availablestock <= model.Quantity)
                {
                    return StatusCode(500, "Not enough stock");
                }


                _repoWrapper.IDespatch_Request.CreateFGS_Despatch_Request(model);
                await _repoWrapper.SaveAsync();
                return Ok("Sucess");
            }
            catch (Exception ex)
            {
                // _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }


        }


        [HttpDelete("")]
        public async Task<IActionResult> DeleteFGS_FIFO_Despatch_Request(int id)
        {
            try
            {
                //var ifexist = _repoWrapper.IDespatch_Request.GetFGS_Despatch_Request(id);
                //if (ifexist == null)
                //{

                //    return NotFound();
                //}

                _repoWrapper.IDespatch_Request.DeleteFGS_Despatch_Request(id);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpGet("GetMaterial_ForCustomer_MaterialType/{customerid}/{materialtypeId}")]
        public  IActionResult GetMaterial_ForCustomer_MaterialType(int customerid, int materialtypeId )
        {
            var result = _repoWrapper.IDespatch_Request.GetMaterial_ForCustomer_MaterialType(customerid, materialtypeId);

            return Ok(result);


        }
    }
}
