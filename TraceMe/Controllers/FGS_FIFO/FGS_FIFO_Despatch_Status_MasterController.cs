﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.FGSFIFO;
using Model.FGS_FIFO;
using Repository;

namespace TraceMe
{
    [Route("api/[controller]")]
    [ApiController]
    public class FGS_FIFO_Despatch_Status_MasterController : ControllerBase
    {
       
         private IRepositoryWrapper _repoWrapper;

        public FGS_FIFO_Despatch_Status_MasterController( IRepositoryWrapper repowrapper)
        {
           
            _repoWrapper = repowrapper;
        }

        
        //Modified HttpGet
        [HttpGet]
        public async Task<IActionResult> GetFGS_FIFO_Despatch_Status_Master()
        {
            try
            {
                var result = await _repoWrapper.IDespatchStatus_Master.GetFGS_Despatch_Status_Master();
                return Ok(result);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

       
  

        [HttpGet("{id}")]
        public async Task<IActionResult> GetFGS_FIFO_Despatch_Status_Master(int id)
        {
            try
            {
                var model = await _repoWrapper.IDespatchStatus_Master.GetFGS_Despatch_Status_Master((id));
                if (model == null)
                {
                    return NotFound();
                }
                return Ok(model);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }


        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFGS_FIFO_Despatch_Status_Master(int id, DespatchStatusMasterDTO fGS_FIFO_Despatch_Status_Master)
        {
            if (id != fGS_FIFO_Despatch_Status_Master.Desp_Status_Id)
            {
                return BadRequest();
            }

        
            try
            {
                if (fGS_FIFO_Despatch_Status_Master == null)

                {
                    return BadRequest("Object is null");
                }
                var ifexist = _repoWrapper.IMaterialConsumedSpec.GetTrace_Material_Consumed_Spec(id);
                if (ifexist == null)
                {

                    return NotFound();
                }
                _repoWrapper.IDespatchStatus_Master.UpdateFGS_Despatch_Status_Master(fGS_FIFO_Despatch_Status_Master);
                await _repoWrapper.SaveAsync();
                return Ok(fGS_FIFO_Despatch_Status_Master);
               
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

            // return NoContent();
        }

       
        [HttpPost]
        public async Task<IActionResult> PostFGS_FIFO_Despatch_Status_Master([FromBody] DespatchStatusMasterDTO model)
        {
            try
            {
                if (model == null)
                {
                    // _logger.LogError("Owner object sent from client is null.");
                    return BadRequest(" object is null");
                }
                _repoWrapper.IDespatchStatus_Master.CreateFGS_Despatch_Status_Master(model);
                await _repoWrapper.SaveAsync();
                return CreatedAtAction("GetTrace_Material_Consumed_Spec", new { id = model.Desp_Status_Id }, model);
            }
            catch (Exception ex)
            {
                // _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
          
        }

     
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFGS_FIFO_Despatch_Status_Master(int id)
        {

            try
            {
                //var ifexist = _repoWrapper.IDespatchStatus_Master.GetFGS_Despatch_Status_Master(id);
                //if (ifexist == null)
                //{

                //    return NotFound();
                //}

                _repoWrapper.IDespatchStatus_Master.DeleteFGS_Despatch_Status_Master(id);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }
        }
}
