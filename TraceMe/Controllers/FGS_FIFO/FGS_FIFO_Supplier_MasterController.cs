﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.FGSFIFO;
using Model.FGS_FIFO;
using Repository;

namespace TraceMe
{
    [Route("api/[controller]")]
    [ApiController]
    public class FGS_FIFO_Supplier_MasterController : ControllerBase
    {
        
         private IRepositoryWrapper _repoWrapper;

        public FGS_FIFO_Supplier_MasterController( IRepositoryWrapper repowrapper)
        {
           
            _repoWrapper = repowrapper;
        }

        
        //Modified HttpGet
        [HttpGet]
        public async Task<IActionResult> GetFGS_FIFO_Supplier_Master()
        {
            try
            {
                var result = await _repoWrapper.ISupplier_Master.GetFGS__Supplier_Master();
                return Ok(result);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }




        [HttpGet("{id}")]
        public async Task<IActionResult> GetFGS_FIFO_Supplier_Master(int id)
        {
            var fGS_FIFO_Supplier_Master = _repoWrapper.ISupplier_Master.FindByCondition(x=> x.Supplier_Id.Equals(id) );

            if (fGS_FIFO_Supplier_Master == null)
            {
                return NotFound();
            }

            return Ok(fGS_FIFO_Supplier_Master);
        }

      
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFGS_FIFO_Supplier_Master(int id,  SupplierMasterDTO fGS_FIFO_Supplier_Master)
        {
            if (id != fGS_FIFO_Supplier_Master.Supplier_Id)
            {
                return BadRequest();
            }

        
            try
            {
                if (fGS_FIFO_Supplier_Master == null)

                {
                    return BadRequest("Object is null");
                }
                var ifexist = _repoWrapper.ISupplier_Master.GetFGS__Supplier_Master(id);
                if (ifexist == null)
                {

                    return NotFound();
                }
                _repoWrapper.ISupplier_Master.UpdateFGS__Supplier_Master(fGS_FIFO_Supplier_Master);
                await _repoWrapper.SaveAsync();
                return Ok(fGS_FIFO_Supplier_Master);
              
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
            // return NoContent();
        }

       
        [HttpPost]
        public async Task<IActionResult> PostFGS_FIFO_Supplier_Master([FromBody] FGS_FIFO_Supplier_Master model)
        {
            _repoWrapper.ISupplier_Master.Create(model);
            _repoWrapper.SaveAsync();
            return this.Ok(model);
        }

        //// DELETE: api/FGS_FIFO_Material_Master/5
        //[HttpDelete("")]
        //public IActionResult DeleteFGS_FIFO_Supplier_Master([FromBody] FGS_FIFO_Supplier_Master model)
        //{

        //    var entity = new FGS_FIFO_Supplier_Master();
        //    if (model == null)
        //    {
        //        return NotFound();
        //    }
        //   if (FGS_FIFO_Supplier_MasterExists(model.Supplier_Id))
        //    {
        //        model.Supplier_Status = 0;
        //        _repoWrapper.ISupplier_Master.Update(model);
        //        _repoWrapper.Save();
        //    }

        //    return NoContent();
        //}

        [HttpDelete("{id}")]

        public async Task<IActionResult> DeleteFGS_FIFO_Supplier_Master(int id)
        {

            try
            {
                //var ifexist = _repoWrapper.IMaterialConsumedSpec.GetTrace_Material_Consumed_Spec(id);
                //if (ifexist == null)
                //{

                //    return NotFound();
                //}

                _repoWrapper.ISupplier_Master.DeleteFGS__Supplier_Master(id);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }


    }
}
