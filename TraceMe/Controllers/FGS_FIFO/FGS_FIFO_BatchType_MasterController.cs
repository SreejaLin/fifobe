﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.FGS_FIFO;
using Model.FGS_FIFO;
using Repository;
using Entity.FGSFIFO;
namespace TraceMe
{
    [Route("api/[controller]")]
    [ApiController]
    public class FGS_FIFO_BatchType_MasterController : ControllerBase
    {
     
         private IRepositoryWrapper _repoWrapper;

        public FGS_FIFO_BatchType_MasterController( IRepositoryWrapper repowrapper)
        {
           
            _repoWrapper = repowrapper;
        }

        // GET: api/FGS_FIFO_BatchType_Master
        //Modified HttpGet
        [HttpGet]
        public async Task<IActionResult> GetFGS_FIFO_BatchType_Master()
        {
            try
            {
                var result = await _repoWrapper.IBatchType_Master.GetFGS_Batch_Type_Master();
                return Ok(result); 
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
         
        }

        // GET: api/FGS_FIFO_BatchType_Master/5


        [HttpGet("{id}")]
        public async Task<IActionResult> GetFGS_FIFO_BatchType_Master(int id)
        {
            var fGS_FIFO_BatchType_Master = _repoWrapper.IBatchType_Master.GetFGS_Batch_Type_Master(id);

            if (fGS_FIFO_BatchType_Master == null)
            {
                return NotFound();
            }

            return Ok(fGS_FIFO_BatchType_Master);
        }

        // PUT: api/FGS_FIFO_BatchType_Master/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFGS_FIFO_BatchType_Master(int id, BatchTypeMasterDTO fGS_FIFO_BatchType_Master)
        {
            if (id != fGS_FIFO_BatchType_Master.Batch_Type_Id)
            {
                return BadRequest();
            }

        
            try
            {

                if (fGS_FIFO_BatchType_Master == null)

                {
                    return BadRequest("Object is null");
                }
                var ifexist = _repoWrapper.IBatchType_Master.GetFGS_Batch_Type_Master(id);
                if (ifexist == null)
                {

                    return NotFound();
                }
                _repoWrapper.IBatchType_Master.UpdateFGS_Batch_Type_Master(fGS_FIFO_BatchType_Master);
                await _repoWrapper.SaveAsync();
                return Ok(fGS_FIFO_BatchType_Master);
              
            }
            catch (DbUpdateConcurrencyException)
            {
                return StatusCode(500, "Internal server error");
            }

           // return NoContent();
        }

        // POST: api/FGS_FIFO_BatchType_Master
        [HttpPost]
        public async Task<IActionResult> PostFGS_FIFO_BatchType_Master([FromBody] BatchTypeMasterDTO model)
        {
            try
            {
                if (model == null)
                {
                    // _logger.LogError("Owner object sent from client is null.");
                    return BadRequest(" object is null");
                }
                _repoWrapper.IBatchType_Master.CreateFGS_Batch_Type_Master(model);
                await _repoWrapper.SaveAsync();
                return CreatedAtAction("GetTrace_Material_Consumed_Spec", new { id = model.Batch_Type_Id }, model);
            }
            catch (Exception ex)
            {
                // _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

         
        }

        // DELETE: api/FGS_FIFO_BatchType_Master/5
        //[HttpDelete("")]
        //public IActionResult DeleteFGS_FIFO_BatchType_Master([FromBody] FGS_FIFO_Batch_Type_Master model)
        //{

        //    var entity = new FGS_FIFO_Batch_Type_Master();
        //    if (model == null)
        //    {
        //        return NotFound();
        //    }
        //   if (FGS_FIFO_BatchType_MasterExists(model.Batch_Type_Id))
        //    {
        //        model.Batch_Type_Status = 0;
        //        _repoWrapper.IBatchType_Master.Update(model);
        //        _repoWrapper.Save();
        //    }

        //    return NoContent();
        //}



        [HttpDelete("{id}")]

        public async Task<IActionResult> DeleteFGS_FIFO_BatchType_Master(int id)
        {

            try
            {
                 _repoWrapper.IBatchType_Master.DeleteFGS_Batch_Type_Master(id);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        
    }
}
