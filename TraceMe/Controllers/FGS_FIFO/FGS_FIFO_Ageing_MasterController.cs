﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.FGS_FIFO;
using Model.FGS_FIFO;
using Repository;
using Entity.FGSFIFO;

namespace TraceMe
{
    [Route("api/[controller]")]
    [ApiController]
    public class FGS_FIFO_Ageing_MasterController : ControllerBase
    {
      
         private readonly IRepositoryWrapper _repoWrapper;

        public FGS_FIFO_Ageing_MasterController( IRepositoryWrapper repowrapper)
        {
           
            _repoWrapper = repowrapper;
        }

        // GET: api/FGS_FIFO_Material_Master
        //Modified HttpGet
        [HttpGet]
        public async Task<IActionResult> GetFGS_Ageing()
        {
            try
            {
                var result = await _repoWrapper.IAgeing_Master.GetFGS_Ageing();
                return Ok(result);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // GET: api/FGS_FIFO_Material_Master/5
  

        [HttpGet("{id}")]
        public async Task<IActionResult> GetFGS_Ageing(int id)
        {
            try
            {
            var fGS_FIFO_Ageing_Master =await _repoWrapper.IAgeing_Master.GetFGS_Ageing((id));
            if (fGS_FIFO_Ageing_Master == null)
            {
                return NotFound();
            }

            return Ok(fGS_FIFO_Ageing_Master);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

      
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFGS_FIFO_Ageing_Master(int id, AgeingMasterDTO fGS_FIFO_Ageing_Master)
        {
            if (id != fGS_FIFO_Ageing_Master.Ageing_Id)
            {
                return BadRequest();
            }

        
            try
            {
                if (fGS_FIFO_Ageing_Master == null)

                {
                    return BadRequest("Object is null");
                }
                var ifexist = _repoWrapper.IAgeing_Master.GetFGS_Ageing(id);
                if (ifexist == null)
                {

                    return NotFound();
                }
                _repoWrapper.IAgeing_Master.UpdateFGS_Ageing(fGS_FIFO_Ageing_Master);
                await _repoWrapper.SaveAsync();
                return Ok(fGS_FIFO_Ageing_Master);
            }
            catch (DbUpdateConcurrencyException)
            {

                return StatusCode(500, "Internal server error");

            }

           // return NoContent();
        }

        // POST: api/FGS_FIFO_Ageing_Master
        [HttpPost]
        public async Task<IActionResult> PostFGS_FIFO_Ageing_Master([FromBody] AgeingMasterDTO model)
        {
            try
            {
                if (model == null)
                {
                    // _logger.LogError("Owner object sent from client is null.");
                    return BadRequest("Owner object is null");
                }
                _repoWrapper.IAgeing_Master.CreateFGS_FGS_Ageing(model);
           await _repoWrapper.SaveAsync();
            return this.Ok(model);
            }
            catch (Exception ex)
            {
                // _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpDelete("{id}")]

        public async Task<IActionResult> DeleteFGS_FIFO_Ageing_Master(int id)
        {

           
            try
            {
             

                _repoWrapper.IAgeing_Master.DeleteFGS_FGS_Ageing(id);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

    }
}
