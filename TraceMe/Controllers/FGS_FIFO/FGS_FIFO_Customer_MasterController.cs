﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.FGSFIFO;
using Model.FGS_FIFO;
using Repository;

namespace TraceMe
{
    [Route("api/[controller]")]
    [ApiController]
    public class FGS_FIFO_Customer_MasterController : ControllerBase
    {
       
         private IRepositoryWrapper _repoWrapper;

        public FGS_FIFO_Customer_MasterController( IRepositoryWrapper repowrapper)
        {
           
            _repoWrapper = repowrapper;
        }

        // GET: api/FGS_FIFO_Customer_Master
        //Modified HttpGet
        [HttpGet]
        public async Task<IActionResult> GetFGS_FIFO_Customer_Master()
        {
            try
            {
                var result = await _repoWrapper.ICustomer_Master.GetFGS_Customer_Master();
                return Ok(result);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // GET: api/FGS_FIFO_Customer_Master/5
  

        [HttpGet("{id}")]
        public async Task<IActionResult> GetFGS_FIFO_Customer_Master(int id)
        {

            try
            {
                var fGS_FIFO_Customer_Master = await _repoWrapper.ICustomer_Master.GetFGS_Customer_Master((id));
                if (fGS_FIFO_Customer_Master == null)
                {
                    return NotFound();
                }
                return Ok(fGS_FIFO_Customer_Master);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

           
        }

        [HttpGet("GetFGS_Customer_MasterByCustType/{id}")]
        public async Task<IActionResult> GetFGS_Customer_MasterByCustType(int id)
        {

            try
            {
                var fGS_FIFO_Customer_Master = await _repoWrapper.ICustomer_Master.GetFGS_Customer_MasterByCustType((id));
                if (fGS_FIFO_Customer_Master == null)
                {
                    return NotFound();
                }
                return Ok(fGS_FIFO_Customer_Master);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }


        }

        // PUT: api/FGS_FIFO_Customer_Master/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFGS_FIFO_Customer_Master(int id, CustomerMasterDTO fGS_FIFO_Customer_Master)
        {
            if (id != fGS_FIFO_Customer_Master.Customer_Id)
            {
                return BadRequest();
            }

        
            try
            {
                if (fGS_FIFO_Customer_Master == null)

                {
                    return BadRequest("Object is null");
                }
                var ifexist = _repoWrapper.ICustomer_Master.GetFGS_Customer_Master(id);
                if (ifexist == null)
                {

                    return NotFound();
                }
                _repoWrapper.ICustomer_Master.UpdateFGS_Customer_Master(fGS_FIFO_Customer_Master);
                await _repoWrapper.SaveAsync();
                
                return Ok(fGS_FIFO_Customer_Master);
               
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

            // return NoContent();
        }

        // POST: api/FGS_FIFO_Customer_Master
        [HttpPost]
        public async Task<IActionResult> PostFGS_FIFO_Customer_Master([FromBody] CustomerMasterDTO model)
        {
            try
            {
                if (model == null)
                {
                    // _logger.LogError("Owner object sent from client is null.");
                    return BadRequest(" object is null");
                }
                _repoWrapper.ICustomer_Master.CreateFGS_Customer_Master(model);
                await _repoWrapper.SaveAsync();
                return CreatedAtAction("GetFGS_Customer_Master", new { id = model.Customer_Id }, model);
            }
            catch (Exception ex)
            {
                // _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
      
        }

        // DELETE: api/Trace_Material_Consumed_Spec/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTrace_Customer_Master(int id)
        {
            try
            {
                //var ifexist = _repoWrapper.ICustomer_Master.GetFGS_Customer_Master(id);
                //if (ifexist == null)
                //{

                //    return NotFound();
                //}

                 _repoWrapper.ICustomer_Master.DeleteFGS_Customer_Master(id);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }


    }
}
