﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Trace_Masters;
using Entity.FGS_FIFO;
using Entity.TraceMasters;
using Microsoft.EntityFrameworkCore;
namespace Repository.Trace_Masters
{
    public class MaterialMasterRepository : RepositoryBase<Trace_Material_Master>, IMaterialMasterRepository
    {
        public MaterialMasterRepository(Trace_DBContext repositoryContext): base(repositoryContext)
        {
        }
        public async Task<IEnumerable<TraceMaterialMasterDTO>> GetFGS__Material_Master()
        {

            var rows = FindAll();
            var results = rows.Select(x => new TraceMaterialMasterDTO()
            {
                Material_Id = x.Material_Id,
                Material_Code = x.Material_Code,
                Material_CreatedBy = x.Material_CreatedBy,
                Material_CreationDate = x.Material_CreationDate,
                Material_Type = x.Material_Type,
                WorkCent_Id = x.WorkCent_Id,
                Status = x.Status

            }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<TraceMaterialMasterDTO>> GetFGS__Material_Master(int id)
        {


            var rows = FindByCondition(x => x.Material_Id.Equals(id));
            var results = rows.Select(x => new TraceMaterialMasterDTO()
            {
                Material_Id = x.Material_Id,
                Material_Code = x.Material_Code,
                Material_CreatedBy = x.Material_CreatedBy,
                Material_CreationDate = x.Material_CreationDate,
                Material_Type = x.Material_Type,
                WorkCent_Id = x.WorkCent_Id,
                Status = x.Status
            }).ToListAsync();

            return await results;


        }
        public void CreateFGS__Material_Master(TraceMaterialMasterDTO model)
        {
            var entity = new Trace_Material_Master();

            entity.Material_Id = model.Material_Id;
            entity.Material_Code = model.Material_Code;
            entity.Material_CreatedBy = model.Material_CreatedBy;
            entity.Material_CreationDate = model.Material_CreationDate;
            entity.Material_Type = model.Material_Type;
            entity.WorkCent_Id = model.WorkCent_Id;
            entity.Status = model.Status;
            Create(entity);
        }
        public void UpdateFGS__Material_Master(TraceMaterialMasterDTO model)
        {
            var entity = new Trace_Material_Master();
            if (model != null)
            {
                if (model.Material_Id > 0)
                {
                    var data = FindByCondition(x => x.Material_Id == model.Material_Id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
                entity.Material_Id = model.Material_Id;
                entity.Material_Code = model.Material_Code;
                entity.Material_CreatedBy = model.Material_CreatedBy;
                entity.Material_CreationDate = model.Material_CreationDate;
                entity.Material_Type = model.Material_Type;
                entity.WorkCent_Id = model.WorkCent_Id;
                entity.Status = model.Status;


            }
            Update(entity);
        }
        public void DeleteFGS__Material_Master(int id)
        {
            var entity = new Trace_Material_Master();
            if (id != 0)

            {
                var data = FindByCondition(x => x.Material_Id == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                //updated entity status to 0 after finding the invoice id
                entity.Material_Id = 0;
            }

            Update(entity);
        }
    }
}
