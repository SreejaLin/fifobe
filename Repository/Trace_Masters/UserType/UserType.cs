﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Trace_Masters;
using Entity.FGS_FIFO;
using Entity.TraceMasters;
using Microsoft.EntityFrameworkCore;
namespace Repository.Trace_Masters
{
    public class UserTypeRepository : RepositoryBase<Trace_User_Type>, IUserTypeRepository
    {
        public UserTypeRepository(Trace_DBContext repositoryContext): base(repositoryContext)
        {
        }
        public async Task<IEnumerable<TraceUserTypeDTO>> GetTrace_User_Type()
        {

            var rows = FindAll();
            var results = rows.Select(x => new TraceUserTypeDTO()
            {
                User_Type_Id = x.User_Type_Id,
                User_Type_Name = x.User_Type_Name,
                User_Type_Status = x.User_Type_Status,


            }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<TraceUserTypeDTO>> GetTrace_User_Type(int id)
        {


            var rows = FindByCondition(x => x.User_Type_Id.Equals(id));
            var results = rows.Select(x => new TraceUserTypeDTO()
            {
                User_Type_Id = x.User_Type_Id,
                User_Type_Name = x.User_Type_Name,
                User_Type_Status = x.User_Type_Status,
            }).ToListAsync();

            return await results;


        }
        public void CreateTrace_User_Type(TraceUserTypeDTO model)
        {
            var entity = new Trace_User_Type();
            entity.User_Type_Id = model.User_Type_Id;
            entity.User_Type_Name = model.User_Type_Name;
            entity.User_Type_Status = model.User_Type_Status;

            Create(entity);
        }
        public void UpdateTrace_User_Type(TraceUserTypeDTO model)
        {
            var entity = new Trace_User_Type();
            if (model != null)
            {
                if (model.User_Type_Id > 0)
                {
                    var data = FindByCondition(x => x.User_Type_Id == model.User_Type_Id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }

                entity.User_Type_Id = model.User_Type_Id;
                entity.User_Type_Name = model.User_Type_Name;
                entity.User_Type_Status = model.User_Type_Status;

            }
            Update(entity);
        }
        public void DeleteTrace_User_Type(int id)
        {
            var entity = new Trace_User_Type();
            if (id != 0)

            {
                var data = FindByCondition(x => x.User_Type_Id == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                entity.User_Type_Id = 0;
            }

            Update(entity);
        }
    }
}
