﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Trace_Masters;
using Entity.TraceMasters;
namespace Repository.Trace_Masters
{
    public interface IWorkCenterMasterRepository // : IRepositoryBase<Trace_WorkCenter_Master>
    {
        Task<IEnumerable<TraceWorkCenterMasterDTO>> GetTrace_WorkCenter_Master();

        Task<IEnumerable<TraceWorkCenterMasterDTO>> GetTrace_WorkCenter_Master(int id);

        void CreateTrace_WorkCenter_Master(TraceWorkCenterMasterDTO model);

        void UpdateTrace_WorkCenter_Master(TraceWorkCenterMasterDTO model);

       void DeleteTrace_WorkCenter_Master(int id);
        
    }
}
