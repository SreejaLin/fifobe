﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Trace_Masters;
using Entity.TraceMasters;
namespace Repository.Trace_Masters
{
    public interface IPlantMasterRepository 
        //: IRepositoryBase<Trace_Plant_Master>
    {
       Task<IEnumerable<TracePlantMasterDTO>> GetTrace_Plant_Master();

       Task<IEnumerable<TracePlantMasterDTO>> GetTrace_Plant_Master(int id);

        void CreateTrace_Plant_Master(TracePlantMasterDTO model);

        void UpdateTrace_Plant_Master(TracePlantMasterDTO model);

        void DeleteTrace_Plant_Master(int id);
        
    }
}
