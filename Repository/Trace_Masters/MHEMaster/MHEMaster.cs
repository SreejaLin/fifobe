﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Trace_Masters;
using Entity.FGS_FIFO;
using Entity.TraceMasters;
using Microsoft.EntityFrameworkCore;
namespace Repository.Trace_Masters
{
    public class MHEMasterRepository : RepositoryBase<Trace_MHE_Master>, IMHEMasterRepository
    {
        public MHEMasterRepository(Trace_DBContext repositoryContext): base(repositoryContext)
        {
        }
        public async Task<IEnumerable<TraceMHEMasterDTO>> GetTrace_MHE_Master()
        {

            var rows = FindAll();
            var results = rows.Select(x => new TraceMHEMasterDTO()
            {
                MHE_Id = x.MHE_Id,
                MHE_Name = x.MHE_Name,
                MHE_Type = x.MHE_Type,
                Max_Capacity = x.Max_Capacity,
                Unit_Of_Measurement = x.Unit_Of_Measurement,
                MHE_Status = x.MHE_Status,
             

            }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<TraceMHEMasterDTO>> GetTrace_MHE_Master(int id)
        {


            var rows = FindByCondition(x => x.MHE_Id.Equals(id));
            var results = rows.Select(x => new TraceMHEMasterDTO()
            {
                MHE_Id = x.MHE_Id,
                MHE_Name = x.MHE_Name,
                MHE_Type = x.MHE_Type,
                Max_Capacity = x.Max_Capacity,
                Unit_Of_Measurement = x.Unit_Of_Measurement,
                MHE_Status = x.MHE_Status,
            }).ToListAsync();

            return await results;


        }
        public void CreateTrace_MHE_Master(TraceMHEMasterDTO model)
        {
            var entity = new Trace_MHE_Master();

            entity.MHE_Id = model.MHE_Id;
            entity.MHE_Name = model.MHE_Name;
            entity.MHE_Type = model.MHE_Type;
            entity.Max_Capacity = model.Max_Capacity;
            entity.Unit_Of_Measurement = model.Unit_Of_Measurement;
            entity.MHE_Status = model.MHE_Status;
            
            Create(entity);
        }
        public void UpdateTrace_MHE_Master(TraceMHEMasterDTO model)
        {
            var entity = new Trace_MHE_Master();
            if (model != null)
            {
                if (model.MHE_Id > 0)
                {
                    var data = FindByCondition(x => x.MHE_Id == model.MHE_Id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
                entity.MHE_Id = model.MHE_Id;
                entity.MHE_Name = model.MHE_Name;
                entity.MHE_Type = model.MHE_Type;
                entity.Max_Capacity = model.Max_Capacity;
                entity.Unit_Of_Measurement = model.Unit_Of_Measurement;
                entity.MHE_Status = model.MHE_Status;


            }
            Update(entity);
        }
        public void DeleteTrace_MHE_Master(int id)
        {
            var entity = new Trace_MHE_Master();
            if (id != 0)

            {
                var data = FindByCondition(x => x.MHE_Id == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                //updated entity status to 0 after finding the invoice id
                entity.MHE_Id = 0;
            }

            Update(entity);
        }
    }
}
