﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Trace_Transactions;
using Entity.TraceTransactions;
namespace Repository.Trace_Transactions
{
    public interface IMaterialConsumedActualRepository// : IRepositoryBase<Trace_Material_Consumed_Actual>
    {
        Task<IEnumerable<TraceMaterialConsumedActualDTO>> GetTrace_Material_Consumed_Actual();
        Task<IEnumerable<TraceMaterialConsumedActualDTO>> GetTrace_Material_Consumed_Actual(int id);
        void CreateTrace_Material_Consumed_Actual(TraceMaterialConsumedActualDTO model);
        void DeleteTrace_Material_Consumed_Actual(int id);
        void UpdateTrace_Material_Consumed_Actual(TraceMaterialConsumedActualDTO model);

    }
}
