﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Trace_Transactions;
using Entity.FGS_FIFO;
using Entity.TraceTransactions;
using Microsoft.EntityFrameworkCore;
namespace Repository.Trace_Transactions
{
    public class MaterialConsumedActualRepository : RepositoryBase<Trace_Material_Consumed_Actual>, IMaterialConsumedActualRepository
    {
        public MaterialConsumedActualRepository(Trace_DBContext repositoryContext) : base(repositoryContext)
        {
        }
        public async Task<IEnumerable<TraceMaterialConsumedActualDTO>> GetTrace_Material_Consumed_Actual()
        {

            var rows = FindAll();
            var results = rows.Select(x => new TraceMaterialConsumedActualDTO()
            {
                MC_Id = x.MC_Id,
                Production_Id = x.Production_Id,
                Production_Date = x.Production_Date,
                Production_Shift = x.Production_Shift,
                Schedule_ID = x.Schedule_ID,
                Status = x.Status
            }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<TraceMaterialConsumedActualDTO>> GetTrace_Material_Consumed_Actual(int id)
        {


            var rows = FindByCondition(x => x.WorkCent_Id.Equals(id));
            var results = rows.Select(x => new TraceMaterialConsumedActualDTO()
            {
                MC_Id = x.MC_Id,
                Production_Id = x.Production_Id,
                Production_Date = x.Production_Date,
                Production_Shift = x.Production_Shift,
                Schedule_ID = x.Schedule_ID,
                Status = x.Status
            }).ToListAsync();

            return await results;


        }
        public void CreateTrace_Material_Consumed_Actual(TraceMaterialConsumedActualDTO model)
        {
            var entity = new Trace_Material_Consumed_Actual();
            entity.MC_Id = model.MC_Id;
                entity.Production_Id = model.Production_Id;
            entity.Production_Date = model.Production_Date;
            entity.Production_Shift = model.Production_Shift;
            entity.Schedule_ID = model.Schedule_ID;
            entity.Status = model.Status;

            Create(entity);
        }
        public void UpdateTrace_Material_Consumed_Actual(TraceMaterialConsumedActualDTO model)
        {
            var entity = new Trace_Material_Consumed_Actual();
            if (model != null)
            {
                if (model.WorkCent_Id > 0)
                {
                    var data = FindByCondition(x => x.MC_Id == model.MC_Id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
                entity.MC_Id = model.MC_Id;
                entity.Production_Id = model.Production_Id;
                entity.Production_Date = model.Production_Date;
                entity.Production_Shift = model.Production_Shift;
                entity.Schedule_ID = model.Schedule_ID;
                entity.Status = model.Status;

            }
            Update(entity);
        }
        public void DeleteTrace_Material_Consumed_Actual(int id)
        {
            var entity = new Trace_Material_Consumed_Actual();
            if (id != 0)

            {
                var data = FindByCondition(x => x.MC_Id == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                entity.MC_Id = 0;
            }

            Update(entity);
        }
    }
}