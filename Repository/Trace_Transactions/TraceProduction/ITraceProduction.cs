﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Trace_Transactions;
using Entity.TraceTransactions;
using Entity.TreadFIFO;
namespace Repository.Trace_Transactions
{
    public interface ITraceProductionRepository //: IRepositoryBase<Trace_Production>
    {
        Task<IEnumerable<TraceProductionDTO>> GetTrace_Production();

        Task<IEnumerable<TraceProductionDTO>> GetTrace_Production(int id);

        void CreateTrace_Production(TraceProductionDTO model);

        void UpdateTrace_Production(TraceProductionDTO model);

        void DeleteTrace_Production(int id);

        List<TraceSchdeuleForDualShiftDTO> GetscheduleForDual_Shift(int id, string shift);
    }
}
