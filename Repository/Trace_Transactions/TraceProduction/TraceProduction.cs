﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Trace_Transactions;
using Entity.FGS_FIFO;
using Entity.TreadFIFO;
using Entity.TraceTransactions;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
namespace Repository.Trace_Transactions
{
    public class TraceProductionRepository : RepositoryBase<Trace_Production>, ITraceProductionRepository
    {
        private Trace_DBContext _context;
        public TraceProductionRepository(Trace_DBContext repositoryContext): base(repositoryContext)
        {
            _context = repositoryContext;
        }
        public async Task<IEnumerable<TraceProductionDTO>> GetTrace_Production()
        {

            var rows = FindAll();
            var results = rows.Select(x => new TraceProductionDTO()
            {
                Production_Id = x.Production_Id,
                Prod_Date = x.Prod_Date,
                Prod_Time = x.Prod_Time,
                Barcode_Id = x.Barcode_Id,
                Material_Id = x.Material_Id,
                MHE_Id = x.MHE_Id,
                Unit_Of_Measurement = x.Unit_Of_Measurement,
                CrewDetails = x.CrewDetails,
                WorkCent_Id = x.WorkCent_Id,
                AdditionalFields = x.AdditionalFields,
                Shift_Id = x.Shift_Id,
                Quantity = x.Quantity,
                Remarks = x.Remarks,
                Status = x.Status,

            }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<TraceProductionDTO>> GetTrace_Production(int id)
        {


            var rows = FindByCondition(x => x.Production_Id.Equals(id));
            var results = rows.Select(x => new TraceProductionDTO()
            {
                Production_Id = x.Production_Id,
                Prod_Date = x.Prod_Date,
                Prod_Time = x.Prod_Time,
                Barcode_Id = x.Barcode_Id,
                Material_Id = x.Material_Id,
                MHE_Id = x.MHE_Id,
                Unit_Of_Measurement = x.Unit_Of_Measurement,
                CrewDetails = x.CrewDetails,
                WorkCent_Id = x.WorkCent_Id,
                AdditionalFields = x.AdditionalFields,
                Shift_Id = x.Shift_Id,
                Quantity = x.Quantity,
                Remarks = x.Remarks,
                Status = x.Status,
            }).ToListAsync();

            return await results;


        }
        public void CreateTrace_Production(TraceProductionDTO model)
        {
            var entity = new Trace_Production();
            entity.Production_Id = model.Production_Id;
            entity.Prod_Date = model.Prod_Date;
            entity.Prod_Time = model.Prod_Time;
            entity.Barcode_Id = model.Barcode_Id;
            entity.Material_Id = model.Material_Id;
            entity.MHE_Id = model.MHE_Id;
            entity.Unit_Of_Measurement = model.Unit_Of_Measurement;
            entity.CrewDetails = model.CrewDetails;
            entity.WorkCent_Id = model.WorkCent_Id;
            entity.AdditionalFields = model.AdditionalFields;
            entity.Shift_Id = model.Shift_Id;
            entity.Quantity = model.Quantity;
            entity.Remarks = model.Remarks;
            entity.Status = model.Status;
            Create(entity);
        }
        public void UpdateTrace_Production(TraceProductionDTO model)
        {
            var entity = new Trace_Production();
            if (model != null)
            {
                if (model.Production_Id > 0)
                {
                    var data = FindByCondition(x => x.Production_Id == model.Production_Id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
                entity.Production_Id = model.Production_Id;
                entity.Prod_Date = model.Prod_Date;
                entity.Prod_Time = model.Prod_Time;
                entity.Barcode_Id = model.Barcode_Id;
                entity.Material_Id = model.Material_Id;
                entity.MHE_Id = model.MHE_Id;
                entity.Unit_Of_Measurement = model.Unit_Of_Measurement;
                entity.CrewDetails = model.CrewDetails;
                entity.WorkCent_Id = model.WorkCent_Id;
                entity.AdditionalFields = model.AdditionalFields;
                entity.Shift_Id = model.Shift_Id;
                entity.Quantity = model.Quantity;
                entity.Remarks = model.Remarks;
                entity.Status = model.Status;
            }
            Update(entity);
        }
        public void DeleteTrace_Production(int id)
        {
            var entity = new Trace_Production();
            if (id != 0)

            {
                var data = FindByCondition(x => x.Production_Id == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                entity.Production_Id = 0;
            }

            Update(entity);
        }

        public string BindCurrentDateandShift()
        {
            string shift;
            DateTime ShiftB ,ShiftC,  EndDayTime , NextDayStartTime , lastDay ,  ShiftA;
            DateTime now = DateTime.Now;
            DateTime curTime = DateTime.Now;
            ShiftA = Convert.ToDateTime("06:01:00 AM");
            ShiftB = Convert.ToDateTime("02:01:00 PM");
            ShiftC = Convert.ToDateTime("10:01:00 PM");
            EndDayTime = Convert.ToDateTime("11:59:55 PM");
            NextDayStartTime = Convert.ToDateTime("12:00:00 AM");

            if (curTime >= ShiftA && curTime < ShiftB)
            { shift = "A";            }
            else if (curTime >= ShiftB && curTime < ShiftC)
            { shift = "B";}
            else if (curTime >= ShiftC && curTime <= EndDayTime)
            { shift = "C"; }
            else if (curTime >= NextDayStartTime && curTime < ShiftA)
            { shift = "C"; }
            else
            { shift = "G"; }
            return shift;
        }

        public List<TraceSchdeuleForDualShiftDTO> GetscheduleForDual_Shift(int id, string shift)
        {
            string dshift = BindCurrentDateandShift();
            if (shift=="PreviousShift")
            {
             if(dshift == "A") { dshift = "C"; }
             else if (dshift == "B") { dshift = "A"; }
             else if   (dshift == "C") { dshift = "B"; }

            }
                      
            var cnn = _context.Database.GetConnectionString();
            var commandParameters = new List<SqlParameter>();
            SqlParameter param;

            param = new SqlParameter("@DualId", SqlDbType.Int)
            { Value = id };
            commandParameters.Add(param);
            param = new SqlParameter("@shift ", SqlDbType.NVarChar)
            { Value = dshift };
            commandParameters.Add(param);
            string sp = "[GetScheduleForDual_Shift]";
            //int resultsIndex = 0;
            var resultslist = new List<TraceSchdeuleForDualShiftDTO>();
            var databaseResult = SqlHelper.ExecuteDataTable(cnn, CommandType.StoredProcedure, sp, commandParameters.ToArray());
            if (databaseResult.Rows != null)
            {

                foreach (DataRow dataRow in databaseResult.Rows)

                {
                    var resultsRow = new TraceSchdeuleForDualShiftDTO();
                    if (dataRow.GetValue("Ext_Code") != DBNull.Value)
                    {
                        resultsRow.Ext_Code = Convert.ToString(dataRow.GetValue("Ext_Code"));
                    }
                    if (dataRow.GetValue("Priority_No") != DBNull.Value)
                    {
                        resultsRow.Priority_No = Convert.ToInt32(dataRow.GetValue("Priority_No"));
                    }
                    if (dataRow.GetValue("Sch_value") != DBNull.Value)
                    {
                        resultsRow.Sch_value = Convert.ToInt32(dataRow.GetValue("Sch_value"));
                    }
                    if (dataRow.GetValue("Tread_Code") != DBNull.Value)
                    {
                        resultsRow.Tread_Code = Convert.ToString(dataRow.GetValue("Tread_Code"));

                    }
                    if (dataRow.GetValue("Tread_Name") != DBNull.Value)
                    {
                        resultsRow.Tread_Name = Convert.ToString(dataRow.GetValue("Tread_Name"));
                    }
                    if (dataRow.GetValue("Comp_Cap") != DBNull.Value)
                    {
                        resultsRow.Comp_Cap = Convert.ToString(dataRow.GetValue("Comp_Cap"));
                    }
                    if (dataRow.GetValue("Comp_Base") != DBNull.Value)
                    {
                        resultsRow.Comp_Base = Convert.ToString(dataRow.GetValue("Comp_Base"));
                    }
                    if (dataRow.GetValue("Comp_Cushion") != DBNull.Value)
                    {
                        resultsRow.Comp_Cushion = Convert.ToString(dataRow.GetValue("Comp_Cushion"));

                    }
                 // resultsRow.MatCodeProdDate = string.Concat(resultsRow.Material_Code, " || ", (resultsRow.ProdDate).ToShortDateString());
                    resultslist.Add(resultsRow);

                    //resultsIndex++;
                }

            }
            return resultslist;

        }
    }
}
