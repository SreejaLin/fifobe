﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Trace_Transactions;
using Entity.FGS_FIFO;
using Entity.TraceTransactions;
using Microsoft.EntityFrameworkCore;

namespace Repository.Trace_Transactions
{
   public class UWBTagMHEMappingRepository : RepositoryBase<Trace_UWBTag_MHEMapping>, IUWBTagMHEMappingRepository
    {
        public UWBTagMHEMappingRepository(Trace_DBContext repositoryContext) : base(repositoryContext)
        {
        }
        public async Task<IEnumerable<TraceUWBTagMHEMappingDTO>> GetTrace_TraceUWBTAgMHEMapping()
        {

            var rows = FindAll();
            var results = rows.Select(x => new TraceUWBTagMHEMappingDTO()
            {
                Tag_MHE_Id =x.MHE_Id,
                 UWB_Tag_Id=x.UWB_Tag_Id,
                MHE_Id=x.MHE_Id,
                  Installed_Date=x.Installed_Date,
                 Status =x.Status
                }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<TraceUWBTagMHEMappingDTO>> GetTrace_TraceUWBTAgMHEMapping(int id)
        {


            var rows = FindByCondition(x => x.Tag_MHE_Id.Equals(id));
            var results = rows.Select(x => new TraceUWBTagMHEMappingDTO()
            {
                Tag_MHE_Id = x.MHE_Id,
                UWB_Tag_Id = x.UWB_Tag_Id,
                MHE_Id = x.MHE_Id,
                Installed_Date = x.Installed_Date,
                Status = x.Status
            }).ToListAsync();

            return await results;


        }
        public void CreateTrace_TraceUWBTAgMHEMapping(TraceUWBTagMHEMappingDTO model)
        {
            var entity = new Trace_UWBTag_MHEMapping();
            entity.Tag_MHE_Id = model.MHE_Id;
            entity.UWB_Tag_Id = model.UWB_Tag_Id;
            entity.MHE_Id = model.MHE_Id;
                  entity.Installed_Date = model.Installed_Date;
            entity.Status = model.Status;

            Create(entity);
        }
        public void UpdateTrace_TraceUWBTAgMHEMapping(TraceUWBTagMHEMappingDTO model)
        {
            var entity = new Trace_UWBTag_MHEMapping();
            if (model != null)
            {
                if (model.UWB_Tag_Id > 0)
                {
                    var data = FindByCondition(x => x.UWB_Tag_Id == model.UWB_Tag_Id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
                entity.Tag_MHE_Id = model.MHE_Id;
                entity.UWB_Tag_Id = model.UWB_Tag_Id;
                entity.MHE_Id = model.MHE_Id;
                entity.Installed_Date = model.Installed_Date;
                entity.Status = model.Status;
            }
            Update(entity);
        }
        public void DeleteTrace_TraceUWBTAgMHEMapping(int id)
        {
            var entity = new Trace_UWBTag_MHEMapping();
            if (id != 0)

            {
                var data = FindByCondition(x => x.UWB_Tag_Id == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                entity.UWB_Tag_Id = 0;
            }

            Update(entity);
        }
    }
}
