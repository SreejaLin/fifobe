﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Trace_Transactions;
using Entity.FGS_FIFO;
using Entity.TraceTransactions;
using Microsoft.EntityFrameworkCore;
namespace Repository.Trace_Transactions
{
    public class MaterialConsumedSpecRepository : RepositoryBase<Trace_Material_Consumed_Spec>, IMaterialConsumedSpecRepository
    {
        public MaterialConsumedSpecRepository(Trace_DBContext repositoryContext): base(repositoryContext)
        {
        }
        public async Task<IEnumerable<TraceMaterialConsumedSpecDTO>> GetTrace_Material_Consumed_Spec()
        {

            var rows = FindAll();
            var results = rows.Select(x => new TraceMaterialConsumedSpecDTO()
            {
                MCS_Id = x.MCS_Id,
                Incoming_Material_Id = x.Incoming_Material_Id,
                Incoming_Material_Type = x.Incoming_Material_Type,
                Production_Id = x.Production_Id,
                status = x.status,
              
            }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<TraceMaterialConsumedSpecDTO>> GetTrace_Material_Consumed_Spec(int id)
        {


            var rows = FindByCondition(x => x.MCS_Id.Equals(id));
            var results = rows.Select(x => new TraceMaterialConsumedSpecDTO()
            {
                MCS_Id = x.MCS_Id,
                Incoming_Material_Id = x.Incoming_Material_Id,
                Incoming_Material_Type = x.Incoming_Material_Type,
                Production_Id = x.Production_Id,
                status = x.status,
            }).ToListAsync();

            return await results;


        }
        public void CreateTrace_Material_Consumed_Spec(TraceMaterialConsumedSpecDTO model)
        {
            var entity = new Trace_Material_Consumed_Spec();
            entity.MCS_Id = model.MCS_Id;
            entity.Incoming_Material_Id = model.Incoming_Material_Id;
            entity.Incoming_Material_Type = model.Incoming_Material_Type;
            entity.Production_Id = model.Production_Id;
            entity.status = model.status;

            Create(entity);
        }
        public void UpdateTrace_Material_Consumed_Spec(TraceMaterialConsumedSpecDTO model)
        {
            var entity = new Trace_Material_Consumed_Spec();
            if (model != null)
            {
                if (model.MCS_Id > 0)
                {
                    var data = FindByCondition(x => x.MCS_Id == model.MCS_Id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
                entity.MCS_Id = model.MCS_Id;
                entity.Incoming_Material_Id = model.Incoming_Material_Id;
                entity.Incoming_Material_Type = model.Incoming_Material_Type;
                entity.Production_Id = model.Production_Id;
                entity.status = model.status;

            }
            Update(entity);
        }
        public void DeleteTrace_Material_Consumed_Spec(int id)
        {
            var entity = new Trace_Material_Consumed_Spec();
            if (id != 0)

            {
                var data = FindByCondition(x => x.MCS_Id == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                entity.status = 0;
            }

            Update(entity);
        }
    }
}
