﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Trace_Transactions;
using Entity.FGS_FIFO;
using Entity.TraceTransactions;
using Microsoft.EntityFrameworkCore;
namespace Repository.Trace_Transactions
{
    public class TagGenerationRepository : RepositoryBase<Trace_Tag_Generation>, ITagGenerationRepository
    {
        public TagGenerationRepository(Trace_DBContext repositoryContext): base(repositoryContext)
        {
        }
        public async Task<IEnumerable<TraceTagGenerationDTO>> GetTrace_Tag_Generation()
        {

            var rows = FindAll();
            var results = rows.Select(x => new TraceTagGenerationDTO()
            {
                TagId = x.TagId,
                Location_Id = x.Location_Id,
                Printer_Name = x.Printer_Name,
               // Printer_Ip = x.Printer_Ip,
                Printed_DateTime = x.Printed_DateTime,
                Module_Name = x.Module_Name,
                User_Id =  x.User_Id,
                Print_Status = x.Print_Status,
              //  InvTranId = x.InvTranId,
                
            }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<TraceTagGenerationDTO>> GetTrace_Tag_Generation(int id)
        {


            var rows = FindByCondition(x => x.TagId.Equals(id));
            var results = rows.Select(x => new TraceTagGenerationDTO()
            {
                TagId = x.TagId,
                Location_Id = x.Location_Id,
                Printer_Name = x.Printer_Name,
              //  Printer_Ip = x.Printer_Ip,
                Printed_DateTime = x.Printed_DateTime,
                Module_Name = x.Module_Name,
                User_Id = x.User_Id,
                Print_Status = x.Print_Status,
               // InvTranId = x.InvTranId,
            }).ToListAsync();

            return await results;


        }
        public void CreateTrace_Tag_Generation(TraceTagGenerationDTO model)
        {
            var entity = new Trace_Tag_Generation();
            entity.TagId = model.TagId;
            entity.Location_Id =Convert.ToInt32(model.Location_Id);
            entity.Printer_Name = model.Printer_Name;
           // entity.Printer_Ip = model.Printer_Ip;
            entity.Printed_DateTime = model.Printed_DateTime;
            entity.Module_Name = model.Module_Name;
            entity.User_Id = model.User_Id;
            entity.Print_Status = model.Print_Status;
            entity.InvTranId = model.InvTranId;
            entity.TagDetails = model.TagDetails;
            entity.Printer_Ip = model.Printer_Ip;

            Create(entity);
        }
        public void UpdateTrace_Tag_Generation(TraceTagGenerationDTO model)
        {
            var entity = new Trace_Tag_Generation();
            if (model != null)
            {
                if (model.TagId > 0)
                {
                    var data = FindByCondition(x => x.TagId == model.TagId);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
                entity.TagId = model.TagId;
                entity.Location_Id = model.Location_Id;
                entity.Printer_Name = model.Printer_Name;
              //  entity.Printer_Ip = model.Printer_Ip;
                entity.Printed_DateTime = model.Printed_DateTime;
                entity.Module_Name = model.Module_Name;
                entity.User_Id = model.User_Id;
                entity.Print_Status = model.Print_Status;
              //  entity.InvTranId = model.InvTranId;
            }
            Update(entity);
        }
        public void DeleteTrace_Tag_Generation(int id)
        {
            var entity = new Trace_Tag_Generation();
            if (id != 0)

            {
                var data = FindByCondition(x => x.TagId == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
              //  entity.TagId = 0;
            }

            Update(entity);
        }
    }
}
