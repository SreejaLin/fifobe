﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Repository.FGS_FIFO;
using Repository.Trace_Transactions;
using Repository.Trace_Masters;
using Repository.Tread_FIFO;
using Repository.Trace_Users;


namespace Repository
{
    public interface IRepositoryWrapper
    {
        IFGSMaterialMasterRepository IMaterial_Master { get; }
        IFGSAgeingMasterRepository IAgeing_Master { get; }
        IFGSBatchTypeMasterRepository IBatchType_Master { get; }
        IFGSCustomerMasterRepository ICustomer_Master { get; }
        IFGSCustAgeingMappingRepository ICustomerAgeing_Mapping { get; }
        IFGSCustomerTypeMasterRepository ICustomer_Type { get; }
        IFGSDailyDespatchTransactionRepository IDaily_DespatchTranscation { get; }
        IFGSLocationMasterRepository ILocation_Master{ get; }
        IFGSMaterialTypeMasterRepository IMaterialType_Master { get; }
        IFGSSupplierMasterRepository ISupplier_Master { get; }
        IFGSDespatchStatusMasterRepository IDespatchStatus_Master { get; }
        IFGSFGSDespatchRequestRepository IDespatch_Request { get; }
        IFGSDailyReceiptTransactionRepository IDaily_ReceiptTranscation { get; }
        //Tread FIFO
        IHoldRepository IHold { get; }
        IBarcodeGenerationRepository IBarcodeGeneration { get; }
        IHoldReasonsRepository IHoldReasons { get; }
        IHoldReleaseRepository IHoldRelease { get; }
        IMaterialTransactionRepository IMaterialTransaction { get; }
        IPrintGenerationRepository IPrintGeneration { get; }
        IProductAgeingRepository IProductAgeing { get; }
        IProductionRemarksRepository IProductionRemarks { get; }
        IScrapEntryRepository IScrapEntry { get; }
        IStatusMasterRepository IStatusMaster { get; }
        ITakenToTBRepository ITakenToTB { get; }

        //transcation
        IMaterialConsumedActualRepository IMaterialConsumedActual { get; }
        IMaterialConsumedSpecRepository IMaterialConsumedSpec { get; }
        ITraceProductionRepository ITraceProduction { get; }
        ITagGenerationRepository ITagGeneration { get; }
        IUWBTagMHEMappingRepository IUWBTagMHEMapping { get; }

        //masters
        IMaterialMasterRepository IMaterialMaster { get; }
        IMHEMasterRepository IMHEMaster { get; }
        IPlantMasterRepository IPlantMaster { get; }
        IStorageLocationMasterRepository IStorageLocationMaster { get; }
        IUserTypeRepository IUserType { get; }
        IUWBAnchorMasterRepository IUWBAnchorMaster { get; }
        IUWBTagMasterRepository IUWBTagMaster { get; }
        IWorkCenterMasterRepository IWorkCenterMaster { get; }

        //Users
        IModuleMasterRespository IModuleMaster { get; }
        IApplicationEmployeeMappingRepository IApplicationEmployeeMapping { get; }
        IApplicationMasterRepository IApplicationMaster { get; }
        ILoginDetailsRepository ILoginDetails { get; }
        ILoginMasterRepository ILoginMaster { get; }

        Task SaveAsync();

    }








}



