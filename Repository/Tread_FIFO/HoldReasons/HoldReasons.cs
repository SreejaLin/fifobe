﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Tread_FIFO;
using Entity.FGS_FIFO;
using Entity.TreadFIFO;
using Microsoft.EntityFrameworkCore;
namespace Repository.Tread_FIFO
{
    public class HoldReasonsRepository : RepositoryBase<Trace_Hold_Reasons>, IHoldReasonsRepository
    {
        public HoldReasonsRepository(Trace_DBContext repositoryContext) : base(repositoryContext)
        {
        }
        public async Task<IEnumerable<TraceHoldReasonsDTO>> GetTrace_Hold_Reasons()
        {

            var rows = FindAll();
            var results = rows.Select(x => new TraceHoldReasonsDTO()
            {
                Hold_Reason_Id = x.Hold_Reason_Id,
                Hold_Reason = x.Hold_Reason,
                Hold_Reason_status = x.Hold_Reason_status,
             
            }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<TraceHoldReasonsDTO>> GetTrace_Hold_Reasons(int id)
        {


            var rows = FindByCondition(x => x.Hold_Reason_Id.Equals(id));
            var results = rows.Select(x => new TraceHoldReasonsDTO()
            {
                Hold_Reason_Id = x.Hold_Reason_Id,
                Hold_Reason = x.Hold_Reason,
                Hold_Reason_status = x.Hold_Reason_status,
            }).ToListAsync();

            return await results;


        }
        public void CreateTrace_Hold_Reasons(TraceHoldReasonsDTO model)
        {
            var entity = new Trace_Hold_Reasons();
            entity.Hold_Reason_Id = model.Hold_Reason_Id;
            entity.Hold_Reason = model.Hold_Reason;
            entity.Hold_Reason_status = model.Hold_Reason_status;
            
            Create(entity);
        }
        public void UpdateTrace_Hold_Reasons(TraceHoldReasonsDTO model)
        {
            var entity = new Trace_Hold_Reasons();
            if (model != null)
            {
                if (model.Hold_Reason_Id > 0)
                {
                    var data = FindByCondition(x => x.Hold_Reason_Id == model.Hold_Reason_Id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
                entity.Hold_Reason_Id = model.Hold_Reason_Id;
                entity.Hold_Reason = model.Hold_Reason;
                entity.Hold_Reason_status = model.Hold_Reason_status;
            }
            Update(entity);
        }
        public void DeleteTrace_Hold_Reasons(int id)
        {
            var entity = new Trace_Hold_Reasons();
            if (id != 0)

            {
                var data = FindByCondition(x => x.Hold_Reason_Id == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                entity.Hold_Reason_Id = 0;
            }

            Update(entity);
        }
    }
}
