﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Tread_FIFO;
using Entity.TreadFIFO;
namespace Repository.Tread_FIFO
{
    public interface IHoldReasonsRepository : IRepositoryBase<Trace_Hold_Reasons>
    {
    Task<IEnumerable<TraceHoldReasonsDTO>> GetTrace_Hold_Reasons();

    Task<IEnumerable<TraceHoldReasonsDTO>> GetTrace_Hold_Reasons(int id);

     void CreateTrace_Hold_Reasons(TraceHoldReasonsDTO model);

     void UpdateTrace_Hold_Reasons(TraceHoldReasonsDTO model);

     void DeleteTrace_Hold_Reasons(int id);
        
    
}
}
