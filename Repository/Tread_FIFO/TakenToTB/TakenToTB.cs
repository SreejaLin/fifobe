﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Tread_FIFO;
using Entity.FGS_FIFO;
using Entity.TreadFIFO;
using Microsoft.EntityFrameworkCore;
namespace Repository.Tread_FIFO
{
    public class TakenToTBRepository : RepositoryBase<Trace_Taken_ToTB>, ITakenToTBRepository
    {
        public TakenToTBRepository(Trace_DBContext repositoryContext): base(repositoryContext)
        {

        }
        public async Task<IEnumerable<TraceTakenToTbDTO>> GetTrace_Taken_ToTB()
        {

            var rows = FindAll();
            var results = rows.Select(x => new TraceTakenToTbDTO()
            {
                TakenToTb_Id = x.TakenToTb_Id,
                TakenToTb_Date = x.TakenToTb_Date,
                TakenToTB_Time = x.TakenToTB_Time,
                TakenToTb_Status = x.TakenToTb_Status,
                TakenToTB_By = x.TakenToTB_By,
                Ageing_Id = x.Ageing_Id,
                Production_Id = x.Production_Id

            }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<TraceTakenToTbDTO>> GetTrace_Taken_ToTB(int id)
        {


            var rows = FindByCondition(x => x.TakenToTb_Id.Equals(id));
            var results = rows.Select(x => new TraceTakenToTbDTO()
            {
                TakenToTb_Id = x.TakenToTb_Id,
                TakenToTb_Date = x.TakenToTb_Date,
                TakenToTB_Time = x.TakenToTB_Time,
                TakenToTb_Status = x.TakenToTb_Status,
                TakenToTB_By = x.TakenToTB_By,
                Ageing_Id = x.Ageing_Id,
                Production_Id = x.Production_Id
            }).ToListAsync();

            return await results;


        }
        public void CreateTrace_Taken_ToTB(TraceTakenToTbDTO model)
        {
            var entity = new Trace_Taken_ToTB();
            entity.TakenToTb_Id = model.TakenToTb_Id;
            entity.TakenToTb_Date = model.TakenToTb_Date;
            entity.TakenToTB_Time = model.TakenToTB_Time;
            entity.TakenToTb_Status = model.TakenToTb_Status;
            entity.TakenToTB_By = model.TakenToTB_By;
            entity.Ageing_Id = model.Ageing_Id;
            entity.Production_Id = model.Production_Id;
            Create(entity);
        }
        public void UpdateTrace_Taken_ToTB(TraceTakenToTbDTO model)
        {
            var entity = new Trace_Taken_ToTB();
            if (model != null)
            {
                if (model.TakenToTb_Id > 0)
                {
                    var data = FindByCondition(x => x.TakenToTb_Id == model.TakenToTb_Id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
                entity.TakenToTb_Id = model.TakenToTb_Id;
                entity.TakenToTb_Date = model.TakenToTb_Date;
                entity.TakenToTB_Time = model.TakenToTB_Time;
                entity.TakenToTb_Status = model.TakenToTb_Status;
                entity.TakenToTB_By = model.TakenToTB_By;
                entity.Ageing_Id = model.Ageing_Id;
                entity.Production_Id = model.Production_Id;
            }
            Update(entity);
        }
        public void DeleteTrace_Taken_ToTB(int id)
        {
            var entity = new Trace_Taken_ToTB();
            if (id != 0)

            {
                var data = FindByCondition(x => x.TakenToTb_Id == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                entity.TakenToTb_Id = 0;
            }

            Update(entity);
        }
    }
}
