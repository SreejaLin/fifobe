﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Tread_FIFO;
using Entity.TreadFIFO;
namespace Repository.Tread_FIFO
{
    public interface ITakenToTBRepository// : IRepositoryBase<Trace_Taken_ToTB>
    {
        Task<IEnumerable<TraceTakenToTbDTO>> GetTrace_Taken_ToTB();

        Task<IEnumerable<TraceTakenToTbDTO>> GetTrace_Taken_ToTB(int id);

        void CreateTrace_Taken_ToTB(TraceTakenToTbDTO model);

        void UpdateTrace_Taken_ToTB(TraceTakenToTbDTO model);

        void DeleteTrace_Taken_ToTB(int id);
      
    }
}
