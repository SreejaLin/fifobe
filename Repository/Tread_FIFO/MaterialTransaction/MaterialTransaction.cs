﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Tread_FIFO;
using Entity.FGS_FIFO;
using Entity.TreadFIFO;
using Microsoft.EntityFrameworkCore;
namespace Repository.Tread_FIFO
{
    public class MaterialTransactionRepository : RepositoryBase<Trace_Material_Transaction>, IMaterialTransactionRepository
    {
        public MaterialTransactionRepository(Trace_DBContext repositoryContext): base(repositoryContext)
        {
        }
        public async Task<IEnumerable<TraceMaterialTransactionDTO>> GetTrace_Material_Transaction()
        {

            var rows = FindAll();
            var results = rows.Select(x => new TraceMaterialTransactionDTO()
            {
                Material_Transaction_Id = x.Material_Transaction_Id,
                Production_Id = x.Production_Id,
                Created_Time = x.Created_Time,
                Start_Time = x.Start_Time,
                End_Time = x.End_Time,
                Created_By = x.Created_By,
                Material_Status = x.Material_Status,
                Trans_Status = x.Trans_Status
            }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<TraceMaterialTransactionDTO>> GetTrace_Material_Transaction(int id)
        {


            var rows = FindByCondition(x => x.Material_Transaction_Id.Equals(id));
            var results = rows.Select(x => new TraceMaterialTransactionDTO()
            {
                Material_Transaction_Id = x.Material_Transaction_Id,
                Production_Id = x.Production_Id,
                Created_Time = x.Created_Time,
                Start_Time = x.Start_Time,
                End_Time = x.End_Time,
                Created_By = x.Created_By,
                Material_Status = x.Material_Status,
                Trans_Status = x.Trans_Status
            }).ToListAsync();

            return await results;


        }
        public void CreateTrace_Material_Transaction(TraceMaterialTransactionDTO model)
        {
            var entity = new Trace_Material_Transaction();
            entity.Material_Transaction_Id = model.Material_Transaction_Id;
            entity.Production_Id = model.Production_Id;
            entity.Created_Time = model.Created_Time;
            entity.Start_Time = model.Start_Time;
            entity.End_Time = model.End_Time;
            entity.Created_By = model.Created_By;
            entity.Material_Status = model.Material_Status;
            entity.Trans_Status = model.Trans_Status;

            Create(entity);
        }
        public void UpdateTrace_Material_Transaction(TraceMaterialTransactionDTO model)
        {
            var entity = new Trace_Material_Transaction();
            if (model != null)
            {
                if (model.Material_Transaction_Id > 0)
                {
                    var data = FindByCondition(x => x.Material_Transaction_Id == model.Material_Transaction_Id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
                entity.Material_Transaction_Id = model.Material_Transaction_Id;
                entity.Production_Id = model.Production_Id;
                entity.Created_Time = model.Created_Time;
                entity.Start_Time = model.Start_Time;
                entity.End_Time = model.End_Time;
                entity.Created_By = model.Created_By;
                entity.Material_Status = model.Material_Status;
                entity.Trans_Status = model.Trans_Status;
            }
            Update(entity);
        }
        public void DeleteTrace_Material_Transaction(int id)
        {
            var entity = new Trace_Material_Transaction();
            if (id != 0)

            {
                var data = FindByCondition(x => x.Material_Transaction_Id == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                entity.Material_Transaction_Id = 0;
            }

            Update(entity);
        }
    }
}
