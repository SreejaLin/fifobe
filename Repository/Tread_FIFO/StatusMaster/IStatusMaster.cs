﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Tread_FIFO;
using Entity.TreadFIFO;
namespace Repository.Tread_FIFO
{
    public interface IStatusMasterRepository //: IRepositoryBase<Trace_Status_Master>
    {
        Task<IEnumerable<TraceStatusMasterDTO>> GetTrace_Status_Master();

         Task<IEnumerable<TraceStatusMasterDTO>> GetTrace_Status_Master(int id);

        void CreateTrace_Status_Master(TraceStatusMasterDTO model);

        void UpdateTrace_Status_Master(TraceStatusMasterDTO model);

       void DeleteTrace_Status_Master(int id);
       
    }
}
