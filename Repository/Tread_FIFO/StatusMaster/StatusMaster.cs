﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Tread_FIFO;
using Entity.FGS_FIFO;
using Entity.TreadFIFO;
using Microsoft.EntityFrameworkCore;
namespace Repository.Tread_FIFO
{
    public class StatusMasterRepository : RepositoryBase<Trace_Status_Master>, IStatusMasterRepository
    {
        public StatusMasterRepository(Trace_DBContext repositoryContext): base(repositoryContext)
        {
        }
        public async Task<IEnumerable<TraceStatusMasterDTO>> GetTrace_Status_Master()
        {

            var rows = FindAll();
            var results = rows.Select(x => new TraceStatusMasterDTO()
            {
                Status_Id = x.Status_Id,
                Status_Code = x.Status_Code,
                Status_Name = x.Status_Name,
                

            }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<TraceStatusMasterDTO>> GetTrace_Status_Master(int id)
        {


            var rows = FindByCondition(x => x.Status_Id.Equals(id));
            var results = rows.Select(x => new TraceStatusMasterDTO()
            {
                Status_Id = x.Status_Id,
                Status_Code = x.Status_Code,
                Status_Name = x.Status_Name,
            }).ToListAsync();

            return await results;


        }
        public void CreateTrace_Status_Master(TraceStatusMasterDTO model)
        {
            var entity = new Trace_Status_Master();
            entity.Status_Id = model.Status_Id;
            entity.Status_Name = model.Status_Name;
            entity.Status_Code = model.Status_Code;
           



            Create(entity);
        }
        public void UpdateTrace_Status_Master(TraceStatusMasterDTO model)
        {
            var entity = new Trace_Status_Master();
            if (model != null)
            {
                if (model.Status_Id > 0)
                {
                    var data = FindByCondition(x => x.Status_Id == model.Status_Id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
                entity.Status_Id = model.Status_Id;
                entity.Status_Name = model.Status_Name;
                entity.Status_Code = model.Status_Code;
            }
            Update(entity);
        }
        public void DeleteTrace_Status_Master(int id)
        {
            var entity = new Trace_Status_Master();
            if (id != 0)

            {
                var data = FindByCondition(x => x.Status_Id == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                entity.Status_Id = 0;
            }

            Update(entity);
        }
    }
}
