﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Tread_FIFO;
using Entity.FGS_FIFO;
using Entity.TreadFIFO;
using Microsoft.EntityFrameworkCore;
namespace Repository.Tread_FIFO
{
    public class HoldReleaseRepository : RepositoryBase<Trace_Hold_Release>, IHoldReleaseRepository
    {
        public HoldReleaseRepository(Trace_DBContext repositoryContext): base(repositoryContext)
        {
        }
        public async Task<IEnumerable<TraceHoldReleaseDTO>> GetTrace_Hold_Release()
        {

            var rows = FindAll();
            var results = rows.Select(x => new TraceHoldReleaseDTO()
            {
                Hold_Release_Id = x.Hold_Release_Id,
                Hold_Id = x.Hold_Id,
                Realesed_By = x.Realesed_By,
                Realesed_Count = x.Realesed_Count,
                Realesed_date = x.Realesed_date,
                Released_Reason = x.Released_Reason,
                Released_Time = x.Released_Time,
                status = x.status

            }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<TraceHoldReleaseDTO>> GetTrace_Hold_Release(int id)
        {


            var rows = FindByCondition(x => x.Hold_Release_Id.Equals(id));
            var results = rows.Select(x => new TraceHoldReleaseDTO()
            {
                Hold_Release_Id = x.Hold_Release_Id,
                Hold_Id = x.Hold_Id,
                Realesed_By = x.Realesed_By,
                Realesed_Count = x.Realesed_Count,
                Realesed_date = x.Realesed_date,
                Released_Reason = x.Released_Reason,
                Released_Time = x.Released_Time,
                status = x.status
            }).ToListAsync();

            return await results;


        }
        public void CreateTrace_Hold(TraceHoldReleaseDTO model)
        {
            var entity = new Trace_Hold_Release();
            entity.Hold_Release_Id = model.Hold_Release_Id;
            entity.Hold_Id = model.Hold_Id;
            entity.Realesed_By = model.Realesed_By;
            entity.Realesed_Count = model.Realesed_Count;
            entity.Realesed_date = model.Realesed_date;
            entity.Released_Reason = model.Released_Reason;
            entity.Released_Time = model.Released_Time;
            entity.status = model.status;

            Create(entity);
        }
        public void UpdateTrace_Hold_Release(TraceHoldReleaseDTO model)
        {
            var entity = new Trace_Hold_Release();
            if (model != null)
            {
                if (model.Hold_Release_Id > 0)
                {
                    var data = FindByCondition(x => x.Hold_Release_Id == model.Hold_Release_Id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
              
                entity.Hold_Release_Id = model.Hold_Release_Id;
                entity.Hold_Id = model.Hold_Id;
                entity.Realesed_By = model.Realesed_By;
                entity.Realesed_Count = model.Realesed_Count;
                entity.Realesed_date = model.Realesed_date;
                entity.Released_Reason = model.Released_Reason;
                entity.Released_Time = model.Released_Time;
                entity.status = model.status;
            }
            Update(entity);
        }
        public void DeleteTrace_Hold_Release(int id)
        {
            var entity = new Trace_Hold_Release();
            if (id != 0)

            {
                var data = FindByCondition(x => x.Hold_Release_Id == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                entity.Hold_Release_Id = 0;
            }

            Update(entity);
        }
    }
}

