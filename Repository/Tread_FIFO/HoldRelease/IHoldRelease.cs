﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Tread_FIFO;
using Entity.TreadFIFO;
namespace Repository.Tread_FIFO
{
    public interface IHoldReleaseRepository //: IRepositoryBase<Trace_Hold_Release>
    {
       Task<IEnumerable<TraceHoldReleaseDTO>> GetTrace_Hold_Release();

        Task<IEnumerable<TraceHoldReleaseDTO>> GetTrace_Hold_Release(int id);

        void CreateTrace_Hold(TraceHoldReleaseDTO model);

         void UpdateTrace_Hold_Release(TraceHoldReleaseDTO model);

         void DeleteTrace_Hold_Release(int id);
       
    }
}
