﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Tread_FIFO;
using Entity.FGS_FIFO;
using Entity.TreadFIFO;
using Microsoft.EntityFrameworkCore;
namespace Repository.Tread_FIFO
{
    public class ProductAgeingRepository : RepositoryBase<Trace_Product_Ageing>, IProductAgeingRepository
    {
        public ProductAgeingRepository(Trace_DBContext repositoryContext): base(repositoryContext)
        {
        }
        public async Task<IEnumerable<TraceProductAgeingDTO>> GetTrace_Product_Ageing()
        {

            var rows = FindAll();
            var results = rows.Select(x => new TraceProductAgeingDTO()
            {
                Product_Ageing_Id = x.Product_Ageing_Id,
                Material_ID = x.Material_ID,
                Ageing_Time = x.Ageing_Time,
                Ageing_Unit = x.Ageing_Unit,
                OverAgeing_Time = x.OverAgeing_Time,
              
            }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<TraceProductAgeingDTO>> GetTrace_Product_Ageing(int id)
        {


            var rows = FindByCondition(x => x.Product_Ageing_Id.Equals(id));
            var results = rows.Select(x => new TraceProductAgeingDTO()
            {
                Product_Ageing_Id = x.Product_Ageing_Id,
                Material_ID = x.Material_ID,
                Ageing_Time = x.Ageing_Time,
                Ageing_Unit = x.Ageing_Unit,
                OverAgeing_Time = x.OverAgeing_Time,
            }).ToListAsync();

            return await results;


        }
        public void CreateTrace_Product_Ageing(TraceProductAgeingDTO model)
        {
            var entity = new Trace_Product_Ageing();
            entity.Product_Ageing_Id = model.Product_Ageing_Id;
            entity.Material_ID = model.Material_ID;
            entity.Ageing_Time = model.Ageing_Time;
            entity.Ageing_Unit = model.Ageing_Unit;
            entity.OverAgeing_Time = model.OverAgeing_Time;
           

            Create(entity);
        }
        public void UpdateTrace_Product_Ageing(TraceProductAgeingDTO model)
        {
            var entity = new Trace_Product_Ageing();
            if (model != null)
            {
                if (model.Product_Ageing_Id > 0)
                {
                    var data = FindByCondition(x => x.Product_Ageing_Id == model.Product_Ageing_Id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
                entity.Product_Ageing_Id = model.Product_Ageing_Id;
                entity.Material_ID = model.Material_ID;
                entity.Ageing_Time = model.Ageing_Time;
                entity.Ageing_Unit = model.Ageing_Unit;
                entity.OverAgeing_Time = model.OverAgeing_Time;
            }
            Update(entity);
        }
        public void DeleteTrace_Product_Ageing(int id)
        {
            var entity = new Trace_Product_Ageing();
            if (id != 0)

            {
                var data = FindByCondition(x => x.Product_Ageing_Id == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                entity.Product_Ageing_Id = 0;
            }

            Update(entity);
        }
    }
}
