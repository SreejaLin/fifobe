﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Tread_FIFO;
using Entity.FGS_FIFO;
using Entity.TreadFIFO;
using Microsoft.EntityFrameworkCore;
namespace Repository.Tread_FIFO
{
    public class HoldRepository: RepositoryBase<Trace_Hold>, IHoldRepository
    {
        public HoldRepository(Trace_DBContext repositoryContext): base(repositoryContext)
        {
        }
        public async Task<IEnumerable<TraceHoldDTO>> GetTrace_Hold()
        {

            var rows = FindAll();
            var results = rows.Select(x => new TraceHoldDTO()
            {
                Hold_Id = x.Hold_Id,
                Hold_Date = x.Hold_Date,
                Hold_Time = x.Hold_Time,
                Hold_By = x.Hold_By,
                Hold_Reason_Id = x.Hold_Reason_Id,
                Hold_Status = x.Hold_Status,
                Production_ID = x.Production_ID
            }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<TraceHoldDTO>> GetTrace_Hold(int id)
        {


            var rows = FindByCondition(x => x.Hold_Id.Equals(id));
            var results = rows.Select(x => new TraceHoldDTO()
            {
                Hold_Id = x.Hold_Id,
                Hold_Date = x.Hold_Date,
                Hold_Time = x.Hold_Time,
                Hold_By = x.Hold_By,
                Hold_Reason_Id = x.Hold_Reason_Id,
                Hold_Status = x.Hold_Status,
                Production_ID = x.Production_ID
            }).ToListAsync();

            return await results;


        }
        public void CreateTrace_Hold(TraceHoldDTO model)
        {
            var entity = new Trace_Hold();
            entity.Hold_Id = model.Hold_Id;
            entity.Hold_Date = model.Hold_Date;
            entity.Hold_Time = model.Hold_Time;
            entity.Hold_By = model.Hold_By;
            entity.Hold_Reason_Id = model.Hold_Reason_Id;
            entity.Hold_Status = model.Hold_Status;
            entity.Production_ID = model.Production_ID;

            Create(entity);
        }
        public void UpdateTrace_Hold(TraceHoldDTO model)
        {
            var entity = new Trace_Hold();
            if (model != null)
            {
                if (model.Hold_Id > 0)
                {
                    var data = FindByCondition(x => x.Hold_Id == model.Hold_Id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
                entity.Hold_Id = model.Hold_Id;
                entity.Hold_Date = model.Hold_Date;
                entity.Hold_Time = model.Hold_Time;
                entity.Hold_By = model.Hold_By;
                entity.Hold_Reason_Id = model.Hold_Reason_Id;
                entity.Hold_Status = model.Hold_Status;
                entity.Production_ID = model.Production_ID;
            }
            Update(entity);
        }
        public void DeleteTrace_Hold(int id)
        {
            var entity = new Trace_Hold();
            if (id != 0)

            {
                var data = FindByCondition(x => x.Hold_Id == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                entity.Hold_Id = 0;
            }

            Update(entity);
        }
    }
}
