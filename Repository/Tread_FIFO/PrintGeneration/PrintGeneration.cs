﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Tread_FIFO;
using Entity.FGS_FIFO;
using Entity.TreadFIFO;
using Microsoft.EntityFrameworkCore;
namespace Repository.Tread_FIFO
{
    public class PrintGenerationRepository : RepositoryBase<Trace_Print_Generation>, IPrintGenerationRepository
    {
        public PrintGenerationRepository(Trace_DBContext repositoryContext): base(repositoryContext)
        {
        }
        public async Task<IEnumerable<TracePrintGenerationDTO>> GetTrace_PrintGeneration()
        {

            var rows = FindAll();
            var results = rows.Select(x => new TracePrintGenerationDTO()
            {
                Print_Id = x.Print_Id,
                Production_Id = x.Production_Id,
                Print_Time = x.Print_Time,
                Barcode_Id = x.Barcode_Id,
                Printed_By = x.Printed_By,
                Status = x.Status,
               
            }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<TracePrintGenerationDTO>> GetTrace_PrintGeneration(int id)
        {


            var rows = FindByCondition(x => x.Print_Id.Equals(id));
            var results = rows.Select(x => new TracePrintGenerationDTO()
            {
                Print_Id = x.Print_Id,
                Production_Id = x.Production_Id,
                Print_Time = x.Print_Time,
                Barcode_Id = x.Barcode_Id,
                Printed_By = x.Printed_By,
                Status = x.Status,
            }).ToListAsync();

            return await results;


        }
        public void CreateTrace_PrintGeneration(TracePrintGenerationDTO model)
        {
            var entity = new Trace_Print_Generation();
            entity.Print_Id = model.Print_Id;
            entity.Production_Id = model.Production_Id;
            entity.Print_Time = model.Print_Time;
            entity.Barcode_Id = model.Barcode_Id;
            entity.Printed_By = model.Printed_By;
            entity.Status = model.Status;
     

            Create(entity);
        }
        public void UpdateTrace_PrintGeneration(TracePrintGenerationDTO model)
        {
            var entity = new Trace_Print_Generation();
            if (model != null)
            {
                if (model.Print_Id > 0)
                {
                    var data = FindByCondition(x => x.Print_Id == model.Print_Id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
                entity.Print_Id = model.Print_Id;
                entity.Production_Id = model.Production_Id;
                entity.Print_Time = model.Print_Time;
                entity.Barcode_Id = model.Barcode_Id;
                entity.Printed_By = model.Printed_By;
                entity.Status = model.Status;
            }
            Update(entity);
        }
        public void DeleteTrace_PrintGeneration(int id)
        {
            var entity = new Trace_Print_Generation();
            if (id != 0)

            {
                var data = FindByCondition(x => x.Print_Id == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                entity.Print_Id = 0;
            }

            Update(entity);
        }
    }
}
