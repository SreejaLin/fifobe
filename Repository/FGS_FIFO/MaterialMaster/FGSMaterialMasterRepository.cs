﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.FGS_FIFO;
using Entity.FGS_FIFO;
using Entity.FGSFIFO;
using Microsoft.EntityFrameworkCore;

namespace Repository.FGS_FIFO
{
    public class FGSMaterialMasterRepository : RepositoryBase<FGS_FIFO_Material_Master>, IFGSMaterialMasterRepository
    {
        public FGSMaterialMasterRepository(Trace_DBContext repositoryContext) : base(repositoryContext)
        {
        }
        public async Task<IEnumerable<MaterialMasterDTO>> GetFGS__Material_Master()
        {

            var rows = FindAll().Include(x => x.FGS_FIFO_Material_Type_Master);
            var results = rows.Select(x => new MaterialMasterDTO()
            {
                Material_Id = x.Material_Id,
                Material_Code = x.Material_Code,
                Material_Description = x.Material_Description,
                Material_Type_Id = x.Material_Type_Id,
                Material_Status = x.Material_Status,
                EntryDate = x.EntryDate.ToShortDateString(),
                Material_Type_Name = x.FGS_FIFO_Material_Type_Master.Material_Type_Name,
                Material = string.Concat(x.Material_Code," / ",x.Material_Description)
                
                
            }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<MaterialMasterDTO>> GetFGS__Material_Master(int id)
        {


            var rows = FindByCondition(x => x.Material_Id.Equals(id)).Include(x => x.FGS_FIFO_Material_Type_Master);
            var results = rows.Select(x => new MaterialMasterDTO()
            {
                Material_Id = x.Material_Id,
                Material_Code = x.Material_Code,
                Material_Description = x.Material_Description,
                Material_Type_Id = x.Material_Type_Id,
                Material_Status = x.Material_Status,
                EntryDate = x.EntryDate.ToShortDateString(),
                Material_Type_Name = x.FGS_FIFO_Material_Type_Master.Material_Type_Name

            }).ToListAsync();

            return await results;


        }
        public void CreateFGS__Material_Master(MaterialMasterDTO model)
        {
            //var ifpresent = FindByCondition(x => x.Material_Code == model.Material_Code);
            //if (ifpresent == null)
            //{ 
                var entity = new FGS_FIFO_Material_Master();
            entity.Material_Id = model.Material_Id;
            entity.Material_Code = model.Material_Code;
            entity.Material_Description = model.Material_Description;
            entity.Material_Type_Id = model.Material_Type_Id;
            entity.Material_Status = model.Material_Status;
            entity.EntryDate =Convert.ToDateTime(model.EntryDate);
            Create(entity);
            //    return true;
            // }
            //return false;
        }
        public void UpdateFGS__Material_Master(MaterialMasterDTO model)
        {
            var entity = new FGS_FIFO_Material_Master();
            if (model != null)
            {
                if (model.Material_Id > 0)
                {
                    var data = FindByCondition(x => x.Material_Id == model.Material_Id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
                entity.Material_Id = model.Material_Id;
                entity.Material_Code = model.Material_Code;
                entity.Material_Description = model.Material_Description;
                entity.Material_Type_Id = model.Material_Type_Id;
                entity.Material_Status = model.Material_Status;
                entity.EntryDate = Convert.ToDateTime(model.EntryDate);


            }
            Update(entity);
        }
        public void DeleteFGS__Material_Master(int id)
        {
            var entity = new FGS_FIFO_Material_Master();
            if (id != 0)

            {
                var data = FindByCondition(x => x.Material_Id == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                //updated entity status to 0 after finding the invoice id
                entity.Material_Status = 0;
            }

            Update(entity);
        }
    }
}
