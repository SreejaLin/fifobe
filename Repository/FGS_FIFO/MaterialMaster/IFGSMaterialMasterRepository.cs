﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.FGS_FIFO;
using Entity.FGSFIFO;
namespace Repository.FGS_FIFO
{
    public interface IFGSMaterialMasterRepository //: IRepositoryBase<FGS_FIFO_Material_Master>
    {
        Task<IEnumerable<MaterialMasterDTO>> GetFGS__Material_Master();

        Task<IEnumerable<MaterialMasterDTO>> GetFGS__Material_Master(int id);

        void CreateFGS__Material_Master(MaterialMasterDTO model);

        void UpdateFGS__Material_Master(MaterialMasterDTO model);

        void DeleteFGS__Material_Master(int id);
        
    }
}
