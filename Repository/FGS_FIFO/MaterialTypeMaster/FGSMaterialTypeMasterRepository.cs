﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.FGS_FIFO;
using Entity.FGS_FIFO;
using Entity.FGSFIFO;
using Microsoft.EntityFrameworkCore;

namespace Repository.FGS_FIFO
{
    public class FGSMaterialTypeMasterRepository : RepositoryBase<FGS_FIFO_Material_Type_Master>, IFGSMaterialTypeMasterRepository
    {
        public FGSMaterialTypeMasterRepository(Trace_DBContext repositoryContext) : base(repositoryContext)
        {
        }
        public async Task<IEnumerable<MaterialTypeMasterDTO>> GetFGS__Material_Master()
        {

            var rows = FindAll();
            var results = rows.Select(x => new MaterialTypeMasterDTO()
            {
                Material_Type_Name = x.Material_Type_Name,
                Material_Type_Status = x.Material_Type_Status,
               
                Material_Type_Id = x.Material_Type_Id
              
            }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<MaterialTypeMasterDTO>> GetFGS__Material_Master(int id)
        {


            var rows = FindByCondition(x => x.Material_Type_Id.Equals(id));
            var results = rows.Select(x => new MaterialTypeMasterDTO()
            {
                Material_Type_Name = x.Material_Type_Name,
                Material_Type_Status = x.Material_Type_Status,

                Material_Type_Id = x.Material_Type_Id
            }).ToListAsync();

            return await results;


        }
        public void CreateFGS__Material_Master(MaterialTypeMasterDTO model)
        {
            var entity = new FGS_FIFO_Material_Type_Master();
           
            entity.Material_Type_Id = model.Material_Type_Id;
            entity.Material_Type_Name = model.Material_Type_Name;
            entity.Material_Type_Status = model.Material_Type_Status;
            Create(entity);
        }
        public void UpdateFGS__Material_Master(MaterialTypeMasterDTO model)
        {
            var entity = new FGS_FIFO_Material_Type_Master();
            if (model != null)
            {
                if (model.Material_Type_Id > 0)
                {
                    var data = FindByCondition(x => x.Material_Type_Id == model.Material_Type_Id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
                entity.Material_Type_Id = model.Material_Type_Id;
                entity.Material_Type_Name = model.Material_Type_Name;
                entity.Material_Type_Status = model.Material_Type_Status;


            }
            Update(entity);
        }
        public void DeleteFGS__Material_Master(int id)
        {
            var entity = new FGS_FIFO_Material_Type_Master();
            if (id != 0)

            {
                var data = FindByCondition(x => x.Material_Type_Id == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                //updated entity status to 0 after finding the invoice id
                entity.Material_Type_Status = 0;
            }

            Update(entity);
        }
    }
    
}
