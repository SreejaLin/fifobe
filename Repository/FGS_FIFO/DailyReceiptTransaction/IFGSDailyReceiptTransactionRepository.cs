﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.FGS_FIFO;
using Entity.FGSFIFO;
namespace Repository.FGS_FIFO
{
    public interface IFGSDailyReceiptTransactionRepository 
        //: IRepositoryBase<FGS_FIFO_Daily_Receipt_Transaction>
    {
        Task<IEnumerable<DailyReceiptTransactionDTO>>GetFGS_Daily_Receipt_Transaction();

        int CreateFGS_Daily_Receipt_Transaction(DailyReceiptTransactionDTO model);
        void UpdateFGS_Daily_Receipt_Transaction(DailyReceiptTransactionDTO model);
       void DeleteFGS_Daily_Receipt_Transaction(int id);
        Task<IEnumerable<DailyReceiptTransactionDTO>> GetFGS_Daily_Receipt_Transaction(int id);
       Task<IEnumerable<DailyReceiptTransactionDTO>> GetFGS_Daily_Receipt_Transaction_Barcode(string barcode);
        int Get_Supplied_Stock_Barcode(string bc);
        int Get_Supplied_Stock(int id);
        string GetLocationsMatID(int materialid);
        string GetBarcodeforMatID(int materialid);
    }
}
