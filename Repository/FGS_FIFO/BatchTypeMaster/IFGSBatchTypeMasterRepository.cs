﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.FGS_FIFO;
using Entity.FGSFIFO;
namespace Repository.FGS_FIFO
{
    public interface IFGSBatchTypeMasterRepository //: IRepositoryBase<FGS_FIFO_Batch_Type_Master>
    {
       Task<IEnumerable<BatchTypeMasterDTO>> GetFGS_Batch_Type_Master();

       Task<IEnumerable<BatchTypeMasterDTO>> GetFGS_Batch_Type_Master(int id);

        void CreateFGS_Batch_Type_Master(BatchTypeMasterDTO model);

       void UpdateFGS_Batch_Type_Master(BatchTypeMasterDTO model);

        void DeleteFGS_Batch_Type_Master(int id);
       

    }
}
