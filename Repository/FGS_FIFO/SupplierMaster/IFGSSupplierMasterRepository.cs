﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.FGS_FIFO;
using Entity.FGSFIFO;
namespace Repository.FGS_FIFO
{
    public interface IFGSSupplierMasterRepository : IRepositoryBase<FGS_FIFO_Supplier_Master>
    {
       Task<IEnumerable<SupplierMasterDTO>> GetFGS__Supplier_Master();

       Task<IEnumerable<SupplierMasterDTO>> GetFGS__Supplier_Master(int id);

        void CreateFGS__Supplier_Master(SupplierMasterDTO model);

        void UpdateFGS__Supplier_Master(SupplierMasterDTO model);

        void DeleteFGS__Supplier_Master(int id);
       
    }
}
