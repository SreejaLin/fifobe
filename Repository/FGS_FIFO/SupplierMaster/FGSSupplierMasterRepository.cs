﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.FGS_FIFO;
using Entity.FGS_FIFO;
using Entity.FGSFIFO;
using Microsoft.EntityFrameworkCore;
namespace Repository.FGS_FIFO
{
    public class FGSSupplierMasterRepository : RepositoryBase<FGS_FIFO_Supplier_Master>, IFGSSupplierMasterRepository
    {
        public FGSSupplierMasterRepository(Trace_DBContext repositoryContext) : base(repositoryContext)
        {
        }
        public async Task<IEnumerable<SupplierMasterDTO>> GetFGS__Supplier_Master()
        {

            var rows = FindAll();
            var results = rows.Select(x => new SupplierMasterDTO()
            {
                Supplier_Id = x.Supplier_Id,
                Supplier_Code = x.Supplier_Code,
                Supplier_Contact_No = x.Supplier_Contact_No,
                Supplier_Name = x.Supplier_Name,
                Supplier_Status = x.Supplier_Status,
                EntryDate = x.EntryDate.ToShortDateString()
            }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<SupplierMasterDTO>> GetFGS__Supplier_Master(int id)
        {


            var rows = FindByCondition(x => x.Supplier_Id.Equals(id));
            var results = rows.Select(x => new SupplierMasterDTO()
            {
                Supplier_Id = x.Supplier_Id,
                Supplier_Code = x.Supplier_Code,
                Supplier_Contact_No = x.Supplier_Contact_No,
                Supplier_Name = x.Supplier_Name,
                Supplier_Status = x.Supplier_Status,
                EntryDate = x.EntryDate.ToShortDateString()
            }).ToListAsync();

            return await results;


        }
        public void CreateFGS__Supplier_Master(SupplierMasterDTO model)
        {
            var entity = new FGS_FIFO_Supplier_Master();

            entity.Supplier_Id = model.Supplier_Id;
            entity.Supplier_Code = model.Supplier_Code;
            entity.Supplier_Contact_No = model.Supplier_Contact_No;
            entity.Supplier_Name = model.Supplier_Name;
            entity.Supplier_Status = model.Supplier_Status;
            entity.EntryDate =Convert.ToDateTime( model.EntryDate);
            Create(entity);
        }
        public void UpdateFGS__Supplier_Master(SupplierMasterDTO model)
        {
            var entity = new FGS_FIFO_Supplier_Master();
            if (model != null)
            {
                if (model.Supplier_Id > 0)
                {
                    var data = FindByCondition(x => x.Supplier_Id == model.Supplier_Id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
                entity.Supplier_Id = model.Supplier_Id;
                entity.Supplier_Code = model.Supplier_Code;
                entity.Supplier_Contact_No = model.Supplier_Contact_No;
                entity.Supplier_Name = model.Supplier_Name;
                entity.Supplier_Status = model.Supplier_Status;
                entity.EntryDate = Convert.ToDateTime(model.EntryDate);


            }
            Update(entity);
        }
        public void DeleteFGS__Supplier_Master(int id)
        {
            var entity = new FGS_FIFO_Supplier_Master();
            if (id != 0)

            {
                var data = FindByCondition(x => x.Supplier_Id == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                //updated entity status to 0 after finding the invoice id
                entity.Supplier_Status = 0;
            }

            Update(entity);
        }
    }
    
}
