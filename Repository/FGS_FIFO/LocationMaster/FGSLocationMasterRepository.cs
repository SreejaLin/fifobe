﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.FGS_FIFO;
using Entity.FGSFIFO;
using Entity.FGS_FIFO;
using Microsoft.EntityFrameworkCore;

namespace Repository.FGS_FIFO
{
    public class FGSLocationMasterRepository : RepositoryBase<FGS_FIFO_Location_Master>, IFGSLocationMasterRepository
    {
        public FGSLocationMasterRepository(Trace_DBContext repositoryContext) : base(repositoryContext)
        {
        }
        public async Task<IEnumerable<LocationMasterDTO>> GetFGS_Location_Master()
        {

            var rows = FindAll();
            var results = rows.Select(x => new LocationMasterDTO()
            {
                Location_Id = x.Location_Id,
                Location_Name = x.Location_Name,
                Location_Is_Empty = x.Location_Is_Empty,
                Location_Status = x.Location_Status,
            }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<LocationMasterDTO>> GetFGS_Location_Master(int id)
        {


            var rows = FindByCondition(x => x.Location_Id.Equals(id));
            var results = rows.Select(x => new LocationMasterDTO()
            {
                Location_Id = x.Location_Id,
                Location_Name = x.Location_Name,
                Location_Is_Empty = x.Location_Is_Empty,
                Location_Status = x.Location_Status,

            }).ToListAsync();

            return await results;


        }
        public void CreateFGS_Location_Master(LocationMasterDTO model)
        {
            var entity = new FGS_FIFO_Location_Master();
            entity.Location_Id = model.Location_Id;
            entity.Location_Name = model.Location_Name;
            entity.Location_Is_Empty = model.Location_Is_Empty;
            entity.Location_Status = model.Location_Status;


            Create(entity);
        }
        public void UpdateFGS_Location_Master(LocationMasterDTO model)
        {
            var entity = new FGS_FIFO_Location_Master();
            if (model != null)
            {
                if (model.Location_Id > 0)
                {
                    var data = FindByCondition(x => x.Location_Id == model.Location_Id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
                entity.Location_Id = model.Location_Id;
                entity.Location_Name = model.Location_Name;
                entity.Location_Is_Empty = model.Location_Is_Empty;
                entity.Location_Status = model.Location_Status;


            }
            Update(entity);
        }
        public void DeleteFGS_Location_Master(int id)
        {
            var entity = new FGS_FIFO_Location_Master();
            if (id != 0)

            {
                var data = FindByCondition(x => x.Location_Id == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                //updated entity status to 0 after finding the invoice id
                entity.Location_Status = 0;
            }

            Update(entity);
        }
    }
   
}
