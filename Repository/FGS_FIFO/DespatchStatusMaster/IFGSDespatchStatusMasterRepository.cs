﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.FGS_FIFO;
using Entity.FGSFIFO;
namespace Repository.FGS_FIFO
{
    public interface IFGSDespatchStatusMasterRepository 
        //: IRepositoryBase<FGS_FIFO_Despatch_Status_Master>
    {
        Task<IEnumerable<DespatchStatusMasterDTO>> GetFGS_Despatch_Status_Master();

        Task<IEnumerable<DespatchStatusMasterDTO>> GetFGS_Despatch_Status_Master(int id);

       void CreateFGS_Despatch_Status_Master(DespatchStatusMasterDTO model);

        void  UpdateFGS_Despatch_Status_Master(DespatchStatusMasterDTO model);
        void DeleteFGS_Despatch_Status_Master(int id);
       
    }
}
