﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.FGS_FIFO;
using Entity.FGS_FIFO;
using Entity.FGSFIFO;
using Microsoft.EntityFrameworkCore;

namespace Repository.FGS_FIFO
{
    public class FGSDespatchStatusMasterRepository : RepositoryBase<FGS_FIFO_Despatch_Status_Master>, IFGSDespatchStatusMasterRepository
    {
        public FGSDespatchStatusMasterRepository(Trace_DBContext repositoryContext) : base(repositoryContext)
        {
        }
        public async Task<IEnumerable<DespatchStatusMasterDTO>> GetFGS_Despatch_Status_Master()
        {

            var rows = FindAll();
            var results = rows.Select(x => new DespatchStatusMasterDTO()
            {
                Desp_Status_Id = x.Desp_Status_Id,
                Desp_Status_Name = x.Desp_Status_Name,
             }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<DespatchStatusMasterDTO>> GetFGS_Despatch_Status_Master(int id)
        {


            var rows = FindByCondition(x => x.Desp_Status_Id.Equals(id));
            var results = rows.Select(x => new DespatchStatusMasterDTO()
            {

                Desp_Status_Id = x.Desp_Status_Id,
                Desp_Status_Name = x.Desp_Status_Name,
                


            }).ToListAsync();

            return await results;


        }
        public void CreateFGS_Despatch_Status_Master(DespatchStatusMasterDTO model)
        {
            var entity = new FGS_FIFO_Despatch_Status_Master();
            entity.Desp_Status_Id = model.Desp_Status_Id;
            entity.Desp_Status_Name = model.Desp_Status_Name;
           

            Create(entity);
        }
        public void UpdateFGS_Despatch_Status_Master(DespatchStatusMasterDTO model)
        {
            var entity = new FGS_FIFO_Despatch_Status_Master();
            if (model != null)
            {
                if (model.Desp_Status_Id > 0)
                {
                    var data = FindByCondition(x => x.Desp_Status_Id == model.Desp_Status_Id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
                entity.Desp_Status_Id = model.Desp_Status_Id;
                entity.Desp_Status_Name = model.Desp_Status_Name;
              

            }
            Update(entity);
        }
        public void DeleteFGS_Despatch_Status_Master(int id)
        {
            var entity = new FGS_FIFO_Despatch_Status_Master();
            if (id != 0)

            {
                var data = FindByCondition(x => x.Desp_Status_Id == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                //updated entity status to 0 after finding the invoice id
                entity.Desp_Status_Name = "0";
            }

            Update(entity);
        }


    }
   
}
