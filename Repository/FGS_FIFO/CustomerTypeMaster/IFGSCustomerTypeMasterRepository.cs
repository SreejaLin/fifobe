﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.FGS_FIFO;
using Entity.FGSFIFO;
namespace Repository.FGS_FIFO
{
    public interface IFGSCustomerTypeMasterRepository : IRepositoryBase<FGS_FIFO_Customer_Type_Master>
    {
     Task<IEnumerable<CustomerTypeMasterDTO>> GetFGS_CustomerType_Master();
       
     Task<IEnumerable<CustomerTypeMasterDTO>> GetFGS_CustomerType_Master(int id);
     
    void CreateFGS_CustomerType_Master(CustomerTypeMasterDTO model);
       
     void UpdateFGS_CustomerType_Master(CustomerTypeMasterDTO model);
      
    void DeleteFGS_CustomerType_Master(int id);
      
    }
}
