﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.FGS_FIFO;
using Entity.FGS_FIFO;
using Entity.FGSFIFO;
using Microsoft.EntityFrameworkCore;

namespace Repository.FGS_FIFO
{
    public class FGSCustomerTypeMasterRepository : RepositoryBase<FGS_FIFO_Customer_Type_Master>, IFGSCustomerTypeMasterRepository
    {
        public FGSCustomerTypeMasterRepository(Trace_DBContext repositoryContext) : base(repositoryContext)
        {
        }
        public async Task<IEnumerable<CustomerTypeMasterDTO>> GetFGS_CustomerType_Master()
        {

            var rows = FindAll();
            var results = rows.Select(x => new CustomerTypeMasterDTO()
            {
                Customer_Type_Id = x.Customer_Type_Id,
                Customer_Type_Name = x.Customer_Type_Name,
                Customer_Type_Status = x.Customer_Type_Status,
         
             


            }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<CustomerTypeMasterDTO>> GetFGS_CustomerType_Master(int id)
        {


            var rows = FindByCondition(x => x.Customer_Type_Id.Equals(id));
            var results = rows.Select(x => new CustomerTypeMasterDTO()
            {

                Customer_Type_Id = x.Customer_Type_Id,
                Customer_Type_Name = x.Customer_Type_Name,
                Customer_Type_Status = x.Customer_Type_Status,
                

            }).ToListAsync();

            return await results;


        }
        public void CreateFGS_CustomerType_Master(CustomerTypeMasterDTO model)
        {
            var entity = new FGS_FIFO_Customer_Type_Master();
            entity.Customer_Type_Id = model.Customer_Type_Id;
            entity.Customer_Type_Name = model.Customer_Type_Name;
            entity.Customer_Type_Status = model.Customer_Type_Status;

            Create(entity);
        }
        public void UpdateFGS_CustomerType_Master(CustomerTypeMasterDTO model)
        {
            var entity = new FGS_FIFO_Customer_Type_Master();
            if (model != null)
            {
                if (model.Customer_Type_Id > 0)
                {
                    var data = FindByCondition(x => x.Customer_Type_Id == model.Customer_Type_Id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
                entity.Customer_Type_Id = model.Customer_Type_Id;
                entity.Customer_Type_Name = model.Customer_Type_Name;
                entity.Customer_Type_Status = model.Customer_Type_Status;
               
            }
            Update(entity);
        }
        public void DeleteFGS_CustomerType_Master(int id)
        {
            var entity = new FGS_FIFO_Customer_Type_Master();
            if (id != 0)

            {
                var data = FindByCondition(x => x.Customer_Type_Id == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                //updated entity status to 0 after finding the invoice id
                entity.Customer_Type_Status = 0;
            }

            Update(entity);
        }
    }
}
