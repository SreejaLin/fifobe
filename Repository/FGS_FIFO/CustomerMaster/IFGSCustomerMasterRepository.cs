﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.FGSFIFO;
namespace Repository.FGS_FIFO
{
    public interface IFGSCustomerMasterRepository
    //: IRepositoryBase<FGS_FIFO_Customer_Master>
    {
     
        Task<IEnumerable<CustomerMasterDTO>> GetFGS_Customer_Master();
        Task<IEnumerable<CustomerMasterDTO>> GetFGS_Customer_Master(int id);
        void CreateFGS_Customer_Master(CustomerMasterDTO model);

        void UpdateFGS_Customer_Master(CustomerMasterDTO model);
        void DeleteFGS_Customer_Master(int id);

        Task<IEnumerable<CustomerMasterDTO>> GetFGS_Customer_MasterByCustType(int id);
}
}
