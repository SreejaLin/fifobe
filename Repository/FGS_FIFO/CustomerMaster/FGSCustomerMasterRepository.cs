﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.FGS_FIFO;
using Entity.FGS_FIFO;
using Entity.FGSFIFO;
using Microsoft.EntityFrameworkCore;

namespace Repository.FGS_FIFO
{
    public class FGSCustomerMasterRepository : RepositoryBase<FGS_FIFO_Customer_Master>, IFGSCustomerMasterRepository
    {
        public FGSCustomerMasterRepository(Trace_DBContext repositoryContext) : base(repositoryContext)
        {
        }
        public async Task<IEnumerable<CustomerMasterDTO>> GetFGS_Customer_Master()
        {

            var rows = FindAll()
                .Include(x => x.FGS_FIFO_Customer_Type_Master);
            //.Include(x=> x.ba);
            var results = rows.Select(x => new CustomerMasterDTO()
            {
                Customer_Type_Id = x.Customer_Type_Id,
                Customer_Id = x.Customer_Id,
                Customer_Address = x.Customer_Address,
                Customer_Name = x.Customer_Name,
                Cust_Status = x.Cust_Status,
                Batch_Type_Id = x.Batch_Type_Id,
                Customer_Type_Name = x.FGS_FIFO_Customer_Type_Master.Customer_Type_Name,
                BatchTypeName = x.FKFGS_FIFO_Batch_Type_Master.Batch_Type_Name


            }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<CustomerMasterDTO>> GetFGS_Customer_Master(int id)
        {


            var rows = FindByCondition(x => x.Customer_Id.Equals(id)).Include(x => x.FGS_FIFO_Customer_Type_Master);
            var results = rows.Select(x => new CustomerMasterDTO()
            {

                Customer_Type_Id = x.Customer_Type_Id,
                Customer_Id = x.Customer_Id,
                Customer_Address = x.Customer_Address,
                Customer_Name = x.Customer_Name,
                Cust_Status = x.Cust_Status,
                Batch_Type_Id = x.Batch_Type_Id,
                Customer_Type_Name = x.FGS_FIFO_Customer_Type_Master.Customer_Type_Name,
                BatchTypeName = x.FKFGS_FIFO_Batch_Type_Master.Batch_Type_Name

            }).ToListAsync();

            return await results;


        }
        public void CreateFGS_Customer_Master(CustomerMasterDTO model)
        {
            var entity = new FGS_FIFO_Customer_Master();
            entity.Customer_Type_Id = model.Customer_Type_Id;
            entity.Customer_Id = model.Customer_Id;
            entity.Customer_Address = model.Customer_Address;
            entity.Customer_Name = model.Customer_Name;
            entity.Cust_Status = model.Cust_Status;
            entity.Batch_Type_Id = model.Batch_Type_Id;


            Create(entity);
        }
        public void UpdateFGS_Customer_Master(CustomerMasterDTO model)
        {
            var entity = new FGS_FIFO_Customer_Master();
            if (model != null)
            {
                if (model.Customer_Id > 0)
                {
                    var data = FindByCondition(x => x.Customer_Id == model.Customer_Id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
                entity.Customer_Type_Id = model.Customer_Type_Id;
                entity.Customer_Id = model.Customer_Id;
                entity.Customer_Address = model.Customer_Address;
                entity.Customer_Name = model.Customer_Name;
                entity.Cust_Status = model.Cust_Status; ;
                entity.Batch_Type_Id = model.Batch_Type_Id;
            }
            Update(entity);
        }
        public void DeleteFGS_Customer_Master(int id)
        {
            var entity = new FGS_FIFO_Customer_Master();
            if (id != 0)

            {
                var data = FindByCondition(x => x.Customer_Id == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                //updated entity status to 0 
                entity.Cust_Status = 0;
            }

            Update(entity);
        }




        public async Task<IEnumerable<CustomerMasterDTO>> GetFGS_Customer_MasterByCustType(int id)
        {


            var rows = FindByCondition(x => x.Customer_Type_Id.Equals(id));
            var results = rows.Select(x => new CustomerMasterDTO()
            {

                Customer_Type_Id = x.Customer_Type_Id,
                Customer_Id = x.Customer_Id,
                Customer_Address = x.Customer_Address,
                Customer_Name = x.Customer_Name,
                Cust_Status = x.Cust_Status,
                Batch_Type_Id = x.Batch_Type_Id,
                Customer_Type_Name = x.FGS_FIFO_Customer_Type_Master.Customer_Type_Name

            }).ToListAsync();

            return await results;


        }
    }
}