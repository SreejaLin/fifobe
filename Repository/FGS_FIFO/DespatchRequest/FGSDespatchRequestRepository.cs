﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.FGS_FIFO;
using Entity.FGS_FIFO;
using Entity.FGSFIFO;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
namespace Repository.FGS_FIFO
{
    public class FGSDespatchRequestRepository : RepositoryBase<FGS_FIFO_Despatch_Request>, IFGSFGSDespatchRequestRepository
    {
        private Trace_DBContext _context;
        public FGSDespatchRequestRepository(Trace_DBContext repositoryContext) : base(repositoryContext)
        {
            _context = repositoryContext;
        }
        public async Task<IEnumerable<DailyDespatchRequestDTO>> GetFGS_Despatch_Request()
        {
            //var loc = _repoWrapper.IDaily_ReceiptTranscation.Get_Location_For_MaterialId(id);
            var rows = FindAll()
                 .Include(sid => sid.FGS_FIFO_Daily_Despatch_Transaction)
                 .Include(mid => mid.FGS_FIFO_Material_Master)
                  ;
            var results = rows.Select(x => new DailyDespatchRequestDTO()
            {
                Desp_Req_Id = x.Desp_Req_Id,
                Material_Desc = x.FGS_FIFO_Material_Master.Material_Description,
                Customer_id = x.Customer_id,
                Quantity = x.Quantity,
                Requested_DateTime = x.Requested_DateTime.ToShortDateString(),
                Entered_By = x.Entered_By,
                Invoice_No = x.Invoice_No,
                Invoice_Date = x.Invoice_Date.ToShortDateString(),
                Request_Status = x.Request_Status,
                Customer_Name = x.FGS_FIFO_Customer_Master.Customer_Name,
                ICFGS_FIFO_Daily_Despatch_Transaction = x.FGS_FIFO_Daily_Despatch_Transaction,
                
                Material_Id = x.Material_Id,
                Material = string.Concat(x.FGS_FIFO_Material_Master.Material_Code, " / ", x.FGS_FIFO_Material_Master.Material_Description),
               ReqIdCustMat = string.Concat(x.Desp_Req_Id , " / ", x.FGS_FIFO_Customer_Master.Customer_Name, " / ", x.FGS_FIFO_Material_Master.Material_Description)
               

            }).ToListAsync() ;

           


            //using (var cnn = _context.Database.GetDbConnection())
            //{
            //    var cmm = cnn.CreateCommand();
            //    cmm.CommandType = CommandType.StoredProcedure;
            //    cmm.CommandText = "[dbo].[FetchMaterial]";
            //    cmm.Parameters.AddRange(param);
            //    cmm.Connection = cnn;
            //  //  cnn.Open();
            //    var reader = cmm.FromSqlRaw();

            //    while (reader.Read())
            //    {
            //        // name from student table 
            //        string Material_Id = Convert.ToString(reader["Material_Id"]);
            //    }
            //    reader.NextResult(); //move the next record set
            //    while (reader.Read())
            //    {
            //        // city from student address table
            //        string Material_Code = Convert.ToString(reader["Material_Code"]);
            //    }
            //}

            return await results;

        }
        public async Task<IEnumerable<DailyDespatchRequestDTO>> GetFGS_Despatch_Request(int id)
        {
            var rows = FindByCondition(x => x.Desp_Req_Id.Equals(id))
                  .Include(sid => sid.FGS_FIFO_Daily_Despatch_Transaction)
                 .Include(mid => mid.FGS_FIFO_Material_Master)
                  ;
            var results = rows.Select(x => new DailyDespatchRequestDTO()
            {
                Desp_Req_Id = x.Desp_Req_Id,
                Material_Desc = x.FGS_FIFO_Material_Master.Material_Description,
                Customer_id = x.Customer_id,
                Quantity = x.Quantity,
                Requested_DateTime = x.Requested_DateTime.ToShortDateString(),
                Entered_By = x.Entered_By,
                Invoice_No = x.Invoice_No,
                Request_Status = x.Request_Status,
                Customer_Name = x.FGS_FIFO_Customer_Master.Customer_Name,
                Material_Id = x.Material_Id
               

            }).ToListAsync();


            return await results ;

        }

        public List<MaterialMasterDTO> GetMaterial_ForCustomer_MaterialType(int custid, int materialtypeid)
        {
      

            var cnn = _context.Database.GetConnectionString();
            var commandParameters = new List<SqlParameter>();
            SqlParameter param;
            
           
            param = new SqlParameter("@CustomerId", SqlDbType.Int)
            { Value = custid };
            commandParameters.Add(param);
            param = new SqlParameter("@MaterialType", SqlDbType.Int)
            { Value = materialtypeid };
            commandParameters.Add(param);
            string sp = "[FetchMaterial_ProductionDate_For_Cust]";
            //int resultsIndex = 0;
            var resultslist = new List<MaterialMasterDTO>();
            var databaseResult = SqlHelper.ExecuteDataTable(cnn, CommandType.StoredProcedure, sp, commandParameters.ToArray());
            if (databaseResult.Rows != null)
            {
               
                foreach (DataRow dataRow in databaseResult.Rows)

                {
                    var resultsRow = new MaterialMasterDTO();
                    if (dataRow.GetValue("Material_Id") != DBNull.Value)
                    {
                        resultsRow.Material_Id = Convert.ToInt32(dataRow.GetValue("Material_Id"));
                    }
                    if (dataRow.GetValue("Material_Desc") != DBNull.Value)
                    {
                        resultsRow.Material_Description = Convert.ToString(dataRow.GetValue("Material_Description"));
                    }
                    if (dataRow.GetValue("Material_Code") != DBNull.Value)
                    {
                        resultsRow.Material_Code = Convert.ToString(dataRow.GetValue("Material_Code"));
                    }
                    if (dataRow.GetValue("ProdDate") != DBNull.Value)
                    {
                        resultsRow.ProdDate = Convert.ToDateTime(dataRow.GetValue("ProdDate"));
                    }
                    resultsRow.MatCodeProdDate = string.Concat(resultsRow.Material_Code, " || ",(resultsRow.ProdDate).ToShortDateString());
                    resultslist.Add(resultsRow);

                                     //resultsIndex++;
                }
               
            }
            return resultslist;


        }



        public void CreateFGS_Despatch_Request(DailyDespatchRequestDTO model)
        {
            var entity = new FGS_FIFO_Despatch_Request();
            if (model != null)
            {
                entity.Desp_Req_Id = model.Desp_Req_Id;
                entity.Material_Id = model.Material_Id;
                entity.Customer_id = model.Customer_id;
                entity.Quantity = model.Quantity;
                entity.Requested_DateTime =Convert.ToDateTime(model.Requested_DateTime);
                entity.Entered_By = model.Entered_By;
                entity.Invoice_No = model.Invoice_No;
                entity.Invoice_Date = Convert.ToDateTime(model.Invoice_Date);
                entity.Request_Status = 1;
                
            }

            Create(entity);
        }
        public void UpdateFGS_Despatch_Request(DailyDespatchRequestDTO model)
        {
            var entity = new FGS_FIFO_Despatch_Request();
            if (model != null)
            {
                if (model.Desp_Req_Id > 0)
                {
                    var data = FindByCondition(x => x.Desp_Req_Id == model.Desp_Req_Id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
                if (model.Desp_Req_Id > 0)
                {
                    entity.Desp_Req_Id = model.Desp_Req_Id;
                }
                if (model.Material_Id != 0)
                {
                    entity.Material_Id = model.Material_Id;
                }
                if (model.Customer_id != 0)
                {
                    entity.Customer_id = model.Customer_id;
                }
                if (model.Quantity != 0)
                {
                    entity.Quantity = model.Quantity;
                }
                if (Convert.ToDateTime(model.Requested_DateTime) != DateTime.MinValue && Convert.ToDateTime(model.Requested_DateTime) != DateTime.MaxValue) { entity.Requested_DateTime = Convert.ToDateTime(model.Requested_DateTime); }

                if (!string.IsNullOrEmpty(model.Invoice_No))
                {
                    entity.Invoice_No = model.Invoice_No;
                }
                if (!string.IsNullOrEmpty(model.Entered_By))
                {
                    entity.Entered_By = model.Entered_By;
                }
                if (Convert.ToDateTime(model.Invoice_Date) != DateTime.MinValue && Convert.ToDateTime(model.Invoice_Date) != DateTime.MaxValue) 
                { 
                    entity.Invoice_Date = Convert.ToDateTime(model.Invoice_Date);
                }
            }
            Update(entity);
        }
        public void DeleteFGS_Despatch_Request(int id)
        {
            var entity = new FGS_FIFO_Despatch_Request();
            if (id != 0)

            {
                var data = FindByCondition(x => x.Desp_Req_Id == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                //updated entity status to 0 after finding the invoice id
                entity.Request_Status = 0;
            }

            Update(entity);
        }
        public List<int> Depatch_RequestId_ForMaterial_ID(int materialId)
        {
            List<int> reqId = new List<int>();
          
            var data = FindByCondition(x => x.Material_Id == materialId);
            if(data.Any())
            {
                foreach(var id in data)
                {
                    reqId.Add(id.Desp_Req_Id);
                }
            }
            return reqId;

        }

        //Retrieves quantity of passed material id to be despatched
        public int TotQuantity_Despatch_Request_Placed_For_MID(int mid)
        {
            // List<int> reqId = new List<int>();
            int TotRequestPlaced = 0;
            var data = FindByCondition(x => x.Material_Id == mid);
            if (data.Any())
            {
                foreach (var id in data)
                {
                    TotRequestPlaced += id.Quantity;
                }
            }
            return TotRequestPlaced;
        }

        public int TotQuantity_Despatch_Request_Placed_For_RID(int rid)
        {
            int TotRequestPlaced = 0;
            var data = FindByCondition(x => x.Desp_Req_Id == rid);
            if (data.Any())
            {
                foreach (var id in data)
                {
                    TotRequestPlaced += id.Quantity;
                }
            }
            return TotRequestPlaced;
        }


        public void ChangeTranscationStatus(int rid, int status)
        {
            var entity = new FGS_FIFO_Despatch_Request();
            var data = FindByCondition(x => x.Desp_Req_Id == rid);
            if (data.Any())
            {
                entity = data.FirstOrDefault();
                entity.Request_Status = status;

            }
            Update(entity);
        }
    }
   
}
