﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.FGS_FIFO;
using Entity.FGSFIFO;
namespace Repository.FGS_FIFO
{
    public interface IFGSCustAgeingMappingRepository //: IRepositoryBase<FGS_FIFO_Cust_Ageing_Mapping>
    {

        Task<IEnumerable<CustomerAgeingMappingDTO>> GetFGS_Cust_Ageing_Mapping();

       Task<IEnumerable<CustomerAgeingMappingDTO>> GetFGS_Cust_Ageing_Mapping(int id);

       void CreateFGS_Cust_Ageing_Mapping(CustomerAgeingMappingDTO model);

       void UpdateFGS_Cust_Ageing_Mapping(CustomerAgeingMappingDTO model);

      void DeleteFGS_Cust_Ageing_Mapping(int id);
        

    }

}
