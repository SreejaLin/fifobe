﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.TraceUsers;
using Entity.FGS_FIFO;
using Entity.TraceUsers;
using Microsoft.EntityFrameworkCore;
namespace Repository.Trace_Users
{
    public class LoginMasterRepository : RepositoryBase<Model.TraceUsers.Trace_Login_Master>, ILoginMasterRepository
    {
        public LoginMasterRepository(Trace_DBContext repositoryContext): base(repositoryContext)
        {
        }
        public async Task<IEnumerable<Trace_Login_MasterDTO>> GetTrace_Login_Master()
        {

            var rows = FindAll();
            var results = rows.Select(x => new Trace_Login_MasterDTO()
            {
                Id = x.Id,
                Emp_Id = x.Emp_Id,
                Password = x.Password,
                User_Name = x.User_Name,
                Plant_Id = x.Plant_Id,
                UserRole = x.UserRole

            }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<Trace_Login_MasterDTO>> GetTrace_Login_Master(int id)
        {


            var rows = FindByCondition(x => x.Id.Equals(id));
            var results = rows.Select(x => new Trace_Login_MasterDTO()
            {
                Id = x.Id,
                Emp_Id = x.Emp_Id,
                Password = x.Password,
                User_Name = x.User_Name,
                Plant_Id= x.Plant_Id,
                UserRole = x.UserRole
            }).ToListAsync();

            return await results;


        }
        public void CreateTrace_Login_Master(Trace_Login_MasterDTO model)
        {
            var entity = new Model.TraceUsers.Trace_Login_Master();
          //  entity.Id = 0;
            entity.Emp_Id = model.Emp_Id;
            entity.Password = model.Password;
            entity.User_Name = model.User_Name;
            entity.Plant_Id = model.Plant_Id;
            entity.UserRole = model.UserRole;
            Create(entity);
        }
        public void UpdateTrace_Login_Master(Trace_Login_MasterDTO model)
        {
            var entity = new Model.TraceUsers.Trace_Login_Master();
            if (model != null)
            {
                if (model.Id > 0)
                {
                    var data = FindByCondition(x => x.Id == model.Id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
                entity.Id = model.Id;
                entity.Emp_Id = model.Emp_Id;
                entity.Password = model.Password;
                entity.User_Name = model.User_Name;
                entity.Plant_Id = model.Plant_Id;
                entity.UserRole = model.UserRole;
            }
            Update(entity);
        }
        public void DeleteTrace_Login_Master(int id)
        {
            var entity = new Trace_Login_Master();
            if (id != 0)

            {
                var data = FindByCondition(x => x.Id == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                entity.Id = 0;
            }

            Update(entity);
        }


        public Trace_Login_MasterDTO ValidateLogin(string UserName,string Password)
        {
          //  var entity = new Trace_Login_Master();
            var user = new Trace_Login_MasterDTO();
            if (UserName != null && Password != null)
            {
                var data = FindByCondition(x => x.User_Name == UserName && x.Password == Password).Include(map => map.AppEmpMapRelation);
                if (data.Any())
                {
                 //   entity = data.FirstOrDefault();
                  

                    foreach (var item in data)
                    {
                        user.Id = item.Emp_Id;
                        user.UserRole = item.UserRole;
                        user.Plant_Id = item.Plant_Id;
                       user.ICTrace_Application_Employee_Mapping = item.AppEmpMapRelation;

                    }

                }
            }
            return  user;
        }



    }
}
