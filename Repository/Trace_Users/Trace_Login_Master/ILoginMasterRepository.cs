﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.TraceUsers;
using Entity.TraceUsers;
namespace Repository.Trace_Users
{
    public interface ILoginMasterRepository //: IRepositoryBase<Trace_Barcode_Generation>
    {
       Task<IEnumerable<Trace_Login_MasterDTO>> GetTrace_Login_Master();

       Task<IEnumerable<Trace_Login_MasterDTO>> GetTrace_Login_Master(int id);

       void CreateTrace_Login_Master(Trace_Login_MasterDTO model);

       void UpdateTrace_Login_Master(Trace_Login_MasterDTO model);

       void DeleteTrace_Login_Master(int id);

        Trace_Login_MasterDTO ValidateLogin(string UserName, string Password);

    }
}
