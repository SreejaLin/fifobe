﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.TraceUsers;
using Entity.TraceUsers;
namespace Repository.Trace_Users
{
    public interface IModuleMasterRespository //: IRepositoryBase<Trace_Barcode_Generation>
    {
         Task<IEnumerable<Trace_Module_MasterDTO>> GetTrace_Module_Master();

        Task<IEnumerable<Trace_Module_MasterDTO>> GetTrace_Module_Master(int id);

       void CreateTrace_Module_Master(Trace_Module_MasterDTO model);

       void UpdateTrace_Module_Master(Trace_Module_MasterDTO model);

       void DeleteTrace_Module_Master(int id);
       


    }
}
