﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.FGS_FIFO;

namespace Entity.FGSFIFO
{
  public class CustomerTypeMasterDTO
    {
       
        public int Customer_Type_Id { get; set; }

        public string Customer_Type_Name { get; set; }

        public int? Customer_Type_Status { get; set; }
    }
}
