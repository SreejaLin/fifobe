﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.FGS_FIFO;

namespace Entity.FGSFIFO
{
  public  class AgeingMappingDTO
    {
      
        public int Cust_Ageing_Id { get; set; }

        public int Customer_Id { get; set; }
        public FGS_FIFO_Customer_Master FGS_FIFO_Customer_Master { get; set; }

        public int Ageing_Id { get; set; }
        public DateTime EntryDate { get; set; }

        public int Ageing_Status { get; set; }
    }
}
