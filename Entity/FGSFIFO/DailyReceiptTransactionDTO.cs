﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.FGS_FIFO;

namespace Entity.FGSFIFO
{
  public  class DailyReceiptTransactionDTO
    {
        
        public int Inv_TransId { get; set; }
        public string Barcode { get; set; }

        public int Supplier_Id { get; set; }
     
      public int Material_Id { get; set; }
             
        public int Batch_Type_Id { get; set; }
       
        public int Quantity { get; set; }

        public string Invoice_No { get; set; }
            
        public string Invoice_Date { get; set; }

       public int Location_Id { get; set; }
 
        public string ProdDate { get; set; }

        public string Exp_Date { get; set; }

        public string EntryDate { get; set; }

        public string Entered_By { get; set; }

       public int Status { get; set; }

       public string Suppliername { get; set; }

        public string MaterialCode { get; set; }
        public string MaterialDesc { get; set; }

        public string BatchTypeName { get; set; }

        public string LocationName { get; set; }

        public int CurrentStock { get; set; }

        public int TotalConsumed { get; set; }

        public int StockRemaining { get; set; }
    }
}
