﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.FGS_FIFO;


namespace Entity.FGSFIFO
{
 public class MaterialTypeMasterDTO
    {
        public int Material_Type_Id { get; set; }
       public string Material_Type_Name { get; set; }
       public int Material_Type_Status { get; set; }
    }
}
