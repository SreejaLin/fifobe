﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.TraceTransactions
{
  public class TraceBarcodeGenerationDTO
    {
       
        public int Barcode_Id { get; set; }
        public DateTime? Barcode_Gen_Date { get; set; }
        public DateTime? Barcode_Gen_Time { get; set; }
        public int Production_Id { get; set; }
        public string? Barcode { get; set; }
        public int? BarcodeGen_Status { get; set; }
    }
}
