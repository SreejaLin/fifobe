﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.TraceTransactions
{
  public  class TraceUWBTagMHEMappingDTO
    {
        public int Tag_MHE_Id { get; set; }
        public int UWB_Tag_Id { get; set; }
        public int MHE_Id { get; set; }
        public DateTime Installed_Date { get; set; }
        public int Status { get; set; }
    }
}
