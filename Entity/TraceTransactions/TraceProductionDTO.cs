﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.TraceTransactions
{
   public class TraceProductionDTO
    {
      
        public int Production_Id { get; set; }
        
        public int Material_Id { get; set; }
        
        public int WorkCent_Id { get; set; }
        public DateTime? Prod_Date { get; set; }
        public DateTime? Prod_Time { get; set; }
      
        public string? CrewDetails { get; set; }
       
        public string? AdditionalFields { get; set; }
       
        public string? Remarks { get; set; }
        public int? Quantity { get; set; }
        public string? Unit_Of_Measurement { get; set; }
        
        public int MHE_Id { get; set; }
        public int? Barcode_Id { get; set; }
        public int? Shift_Id { get; set; }
        public int? Status { get; set; }
    }
}
