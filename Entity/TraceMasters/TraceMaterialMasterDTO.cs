﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.TraceMasters
{
   public class TraceMaterialMasterDTO
    {
      
        public int Material_Id { get; set; }

        public string? Material_Code { get; set; }
      
        public int WorkCent_Id { get; set; }
        public string? Material_Type { get; set; }
        public DateTime Material_CreationDate { get; set; }
        public string? Material_CreatedBy { get; set; }
        public int Status { get; set; }
        public int Material { get; set; }
    }
}
