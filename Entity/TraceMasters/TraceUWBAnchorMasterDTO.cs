﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.TraceMasters
{
  public  class TraceUWBAnchorMasterDTO
    {
      
        public int UWB_Anchor_Id { get; set; }

        public string? UWD_Anchor_Name { get; set; }
        public string? UWD_Anchor_Make { get; set; }
        public DateTime? UWD_Anchor_Installed_Date { get; set; }
        public int? UWD_Anchor_Status { get; set; }
    }
}
