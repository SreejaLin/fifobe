﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.TraceMasters
{
   public class TraceUWBTagMasterDTO
    {
        
        public int UWB_Tag_Id { get; set; }

        public string? UWB_Tag_Name { get; set; }
        public int? UWB_Tag_Uniqe_Id { get; set; }
        public int? UWB_Tag_Status { get; set; }

    }
}
