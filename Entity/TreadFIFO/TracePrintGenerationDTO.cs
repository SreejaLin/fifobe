﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.TreadFIFO
{
   public class TracePrintGenerationDTO
    {

        
        public int Print_Id { get; set; }
       
        public int Production_Id { get; set; }
        public DateTime? Print_Time { get; set; }
       
        public string? Printed_By { get; set; }
        public int? Barcode_Id { get; set; }
        public int? Status { get; set; }
    }
}
