﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.TreadFIFO
{
  public class TraceSchdeuleForDualShiftDTO
    {
        public string Ext_Code { get; set; }
        public int Priority_No { get; set; }
        public int Sch_value { get; set; }
        public string Tread_Code { get; set; }
        public string Tread_Name { get; set; }
        public string Comp_Cap { get; set; }
        public string Comp_Base { get; set; }
        public string Comp_Cushion { get; set; }

    }
}
