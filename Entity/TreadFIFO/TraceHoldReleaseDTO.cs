﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.TreadFIFO
{
   public class TraceHoldReleaseDTO
    {
      
        public int Hold_Release_Id { get; set; }
       
        public int Hold_Id { get; set; }
     
        public string? Realesed_By { get; set; }

        public int Realesed_Count { get; set; }
        public DateTime? Released_Time { get; set; }
        public DateTime? Realesed_date { get; set; }
     
        public string? Released_Reason { get; set; }

        public int status { get; set; }
    }
}
