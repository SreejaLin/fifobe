﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.TreadFIFO
{
    public class TraceProductAgeingDTO
    {
        
        public int Product_Ageing_Id { get; set; }
      
        public int? Material_ID { get; set; }
        public int? Ageing_Time { get; set; }
        public int? OverAgeing_Time { get; set; }

        public string? Ageing_Unit { get; set; }
    }
}
