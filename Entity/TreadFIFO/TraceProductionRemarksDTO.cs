﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.TreadFIFO
{
  public  class TraceProductionRemarksDTO
    {
        
        public int Remark_Id { get; set; }
       
        public string Remarks { get; set; }
       
        public int WorkCent_Id { get; set; }
        public int? Remark_Status { get; set; }
    }
}
