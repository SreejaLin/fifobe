﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.TreadFIFO
{
   public class TraceMaterialTransactionDTO
    {
      
        public int Material_Transaction_Id { get; set; }
       
        public int? Production_Id { get; set; }
        public int? Material_Status { get; set; }
        public DateTime? Start_Time { get; set; }
        public DateTime? End_Time { get; set; }
       
        public string? Created_By { get; set; }
        public DateTime? Created_Time { get; set; }
        public int? Trans_Status { get; set; }
    }
}
