﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Model;
using Microsoft.EntityFrameworkCore;
using Model.FGS_FIFO;
using Model.Trace_Transactions;
using Model.Tread_FIFO;
using Model.Trace_Masters;
using Model.TraceUsers;


namespace Entity.FGS_FIFO
{
    public class Trace_DBContext:DbContext
    {
        public Trace_DBContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<FGS_FIFO_Material_Master> FGS_FIFO_Material_Master { get; set; }
        public DbSet<FGS_FIFO_Material_Type_Master> FGS_FIFO_Material_Type_Master { get; set; }
        public DbSet<FGS_FIFO_Ageing_Master> FGS_FIFO_Ageing_Master { get; set; }

        public DbSet<FGS_FIFO_Batch_Type_Master> FGS_FIFO_Batch_Type_Master { get; set; }
        public DbSet<FGS_FIFO_Cust_Ageing_Mapping> FGS_FIFO_Cust_Ageing_Mapping { get; set; }
        public DbSet<FGS_FIFO_Customer_Master> FGS_FIFO_Customer_Master { get; set; }

        public DbSet<FGS_FIFO_Customer_Type_Master> FGS_FIFO_Customer_Type_Master { get; set; }
        public DbSet<FGS_FIFO_Daily_Despatch_Transaction> FGS_FIFO_Daily_Despatch_Transaction { get; set; }
        public DbSet<FGS_FIFO_Daily_Receipt_Transaction> FGS_FIFO_Daily_Receipt_Transaction { get; set; }

        public DbSet<FGS_FIFO_Despatch_Status_Master> FGS_FIFO_Despatch_Status_Master { get; set; }
        public DbSet<FGS_FIFO_Despatch_Request> FGS_FIFO_Despatch_Request { get; set; }
        public DbSet<FGS_FIFO_Location_Master> FGS_FIFO_Location_Master { get; set; }
        public DbSet<FGS_FIFO_Supplier_Master> FGS_FIFO_Supplier_Master { get; set; }


        //Trace Tread
        public DbSet<Trace_Production> Trace_Production { get; set; }
        public DbSet<Trace_Barcode_Generation> Trace_Barcode_Generation { get; set; }
        public DbSet<Trace_Hold> Trace_Hold { get; set; }
        public DbSet<Trace_Hold_Reasons> Trace_Hold_Reasons { get; set; }
        public DbSet<Trace_Hold_Release> Trace_Hold_Release { get; set; }
        public DbSet<Trace_Material_Transaction> Trace_Material_Transaction { get; set; }
        public DbSet<Trace_Print_Generation> Trace_Print_Generation { get; set; }
        public DbSet<Trace_Product_Ageing> Trace_Product_Ageing { get; set; }
        public DbSet<Trace_Production_Remarks> Trace_Production_Remarks { get; set; }
        public DbSet<Trace_Status_Master> Trace_Status_Master { get; set; }
        public DbSet<Trace_Taken_ToTB> Trace_Taken_ToTB { get; set; }
        public DbSet<Trace_User_Type> Trace_User_Type { get; set; }


        public DbSet<Trace_Material_Consumed_Actual> Trace_Material_Consumed_Actual { get; set; }
        public DbSet<Trace_Material_Consumed_Spec> Trace_Material_Consumed_Spec { get; set; }

        public DbSet<Trace_WorkCenter_Master> Trace_WorkCenter_Master { get; set; }
        public DbSet<Trace_Tag_Generation> Trace_Tag_Generation { get; set; }
        public DbSet<Trace_UWBTag_MHEMapping> Trace_UWBTag_MHEMapping { get; set; }
        public DbSet<Trace_Scrap_Entry> Trace_Scrap_Entry { get; set; }
        public DbSet<Trace_UWB_Anchor_Master> Trace_UWB_Anchor_Master { get; set; }
        public DbSet<Trace_Storage_Location_Master> Trace_Storage_Location_Master { get; set; }
        public DbSet<Trace_UWB_Tag_Master> Trace_UWB_Tag_Master { get; set; }
        public DbSet<Trace_MHE_Master> Trace_MHE_Master { get; set; }
        public DbSet<Trace_Material_Master> Trace_Material_Master { get; set; }
        public DbSet<Trace_Plant_Master> Trace_Plant_Master { get; set; }

        //Trace Users

        public DbSet<Trace_Application_Employee_Mapping > Trace_Application_Employee_Mapping { get; set; }
        public DbSet<Trace_Application_Master> Trace_Application_Masters { get; set; }
        public DbSet<Trace_Login_Details> Trace_Login_Details { get; set; }
        public DbSet<Trace_Login_Master > Trace_Login_Master { get; set; }
        public DbSet<Trace_Module_Master> Trace_Module_Master { get; set; }
        
//        protected override void OnModelCreating(ModelBuilder modelBuilder)
//        {

//        modelBuilder.Entity<FGS_FIFO_Material_Type_Master>()
//        .HasData(
//            new FGS_FIFO_Material_Type_Master { Material_Type_Id = 1, Material_Type_Name = "Tube", Material_Type_Status = 1 },
//            new FGS_FIFO_Material_Type_Master { Material_Type_Id = 2, Material_Type_Name = "Flap", Material_Type_Status = 1 }
//                );


//            modelBuilder.Entity<FGS_FIFO_Material_Master>().HasData(
//    new FGS_FIFO_Material_Master
//    {
//        Material_Id = 1,
//        Material_Code = "RUA1E0TUB0AA1",
//        Material_Description = "12.00-20 TUBE -D",
//        Material_Type_Id = 1,
//        EntryDate =Convert.ToDateTime("2021-03-27"),
//        Material_Status = 1
//    },
//    new FGS_FIFO_Material_Master
//    {
//        Material_Id = 2,
//        Material_Code = "RUA1E0TUB0AA1",
//        Material_Description = "12.00-20 TUBE -D",
//        Material_Type_Id = 1,
//        EntryDate =Convert.ToDateTime("2021-03-27"),
//        Material_Status = 1
//    },
//    new FGS_FIFO_Material_Master
//    {
//        Material_Id = 3,
//        Material_Code = "RUA1E0TUB0AA1",
//        Material_Description = "12.00-20 TUBE -D",
//        Material_Type_Id = 1,
//        EntryDate =Convert.ToDateTime("2021-03-27"),
//        Material_Status = 1
//    },
//    new FGS_FIFO_Material_Master
//    {
//        Material_Id = 4,
//        Material_Code = "RUA1E0TUB0AA1",
//        Material_Description = "12.00-20 TUBE -D",
//        Material_Type_Id = 1,
//        EntryDate =Convert.ToDateTime("2021-03-27"),
//        Material_Status = 1
//    },
//    new FGS_FIFO_Material_Master
//    {
//        Material_Id = 5,
//        Material_Code = "RUA1E0TUB0AA1",
//        Material_Description = "12.00-20 TUBE -D",
//        Material_Type_Id = 1,
//        EntryDate =Convert.ToDateTime("2021-03-27"),
//        Material_Status = 1
//    },


      


//       new FGS_FIFO_Material_Master
//       {
//           Material_Id = 6,
//           Material_Code = "RVA0S0FLP0A01",
//           Material_Description = "20N 20X7.5/7.0 IBA BIAS FLAP -D",
//           Material_Type_Id = 2,
//           EntryDate =Convert.ToDateTime("2021-03-27"),
//           Material_Status = 1

//       },

//       new FGS_FIFO_Material_Master
//       {
//           Material_Id = 7,
//           Material_Code = "RVA0S0FLP0A02",
//           Material_Description = "20N 20X7.5/7.0 SUPER IBA BIAS FLAP -D",
//           Material_Type_Id = 2,
//           EntryDate = Convert.ToDateTime("2021-03-27"),
//           Material_Status = 1


//       },

//            new FGS_FIFO_Material_Master
//            {
//                Material_Id = 8,
//                Material_Code = "RVAM00FLP0A02",
//                Material_Description = "20 M FLAP(HW)-D",
//                Material_Type_Id = 2,
//                EntryDate =Convert.ToDateTime("2021-03-27"),
//                Material_Status = 1



//            }

//);




//            modelBuilder.Entity<FGS_FIFO_Supplier_Master>().HasData(


//                        new FGS_FIFO_Supplier_Master { Supplier_Id = 1, Supplier_Code = "1003098", Supplier_Name = "EXEL RUBBER LIMITED", Supplier_Contact_No = "1234567890", EntryDate = Convert.ToDateTime("2021-03-27"),Supplier_Status = 1 }
//                       , new FGS_FIFO_Supplier_Master { Supplier_Id = 2, Supplier_Code = "1004121", Supplier_Name = "Apollo Tyres Ltd., Kalamassery", Supplier_Contact_No = "1234567890", EntryDate = Convert.ToDateTime("2021-03-27"), Supplier_Status = 1 }
//                       , new FGS_FIFO_Supplier_Master { Supplier_Id = 3, Supplier_Code = "2008506", Supplier_Name = "Classic Industries and Exports Ltd", Supplier_Contact_No = "1234567890", EntryDate = Convert.ToDateTime("2021-03-27"), Supplier_Status = 1 },
//                       new FGS_FIFO_Supplier_Master { Supplier_Id = 4, Supplier_Code = "4100002", Supplier_Name = "EXEL RUBBER LTD.", Supplier_Contact_No = "1234567890", EntryDate = Convert.ToDateTime("2021-03-27"), Supplier_Status = 1 },
//                       new FGS_FIFO_Supplier_Master { Supplier_Id = 5, Supplier_Code = "4100003", Supplier_Name = "Vilas Polymer Ltd", Supplier_Contact_No = "1234567890", EntryDate = Convert.ToDateTime("2021-03-27"), Supplier_Status = 1 },
//                       new FGS_FIFO_Supplier_Master { Supplier_Id = 6, Supplier_Code = "4100113", Supplier_Name = "FARSEEN RUBBER INDUSTRIES LTD.", Supplier_Contact_No = "1234567890", EntryDate = Convert.ToDateTime("2021-03-27"), Supplier_Status = 1 },
//                       new FGS_FIFO_Supplier_Master { Supplier_Id = 7, Supplier_Code = "4100115", Supplier_Name = "RUBBER KING TYRE PRIVATE LIMITED", Supplier_Contact_No = "1234567890", EntryDate = Convert.ToDateTime("2021-03-27"), Supplier_Status = 1 }

//           );
//            modelBuilder.Entity<FGS_FIFO_Location_Master>().HasData(new FGS_FIFO_Location_Master { Location_Id = 1, Location_Name = "1", Location_Is_Empty = 0, Location_Status = 1 },
//             new FGS_FIFO_Location_Master { Location_Id = 2, Location_Name = "2", Location_Is_Empty = 0, Location_Status = 1 },
//             new FGS_FIFO_Location_Master { Location_Id = 3, Location_Name = "3", Location_Is_Empty = 0, Location_Status=1 },
//              new FGS_FIFO_Location_Master { Location_Id = 4, Location_Name = "4", Location_Is_Empty = 0, Location_Status = 1 },
//               new FGS_FIFO_Location_Master { Location_Id = 5, Location_Name = "5", Location_Is_Empty = 0, Location_Status = 1 },

//                 new FGS_FIFO_Location_Master { Location_Id = 6, Location_Name = "6", Location_Is_Empty = 0, Location_Status = 1 },
//                 new FGS_FIFO_Location_Master { Location_Id = 7, Location_Name = "7", Location_Is_Empty = 0, Location_Status = 1 },
//                  new FGS_FIFO_Location_Master { Location_Id = 8, Location_Name = "8", Location_Is_Empty = 0, Location_Status = 1 },
//                   new FGS_FIFO_Location_Master { Location_Id = 9, Location_Name = "9", Location_Is_Empty = 0, Location_Status = 1 }
//                   );

//            modelBuilder.Entity<FGS_FIFO_Customer_Type_Master>().HasData(

//                new FGS_FIFO_Customer_Type_Master { Customer_Type_Id = 1, Customer_Type_Name = "OE", Customer_Type_Status = 0 },
//                   new FGS_FIFO_Customer_Type_Master { Customer_Type_Id = 2, Customer_Type_Name = "EXPORT", Customer_Type_Status = 1 },
//                   new FGS_FIFO_Customer_Type_Master { Customer_Type_Id = 3, Customer_Type_Name = "DOMESTIC", Customer_Type_Status=2 }
//                   );

//            modelBuilder.Entity<FGS_FIFO_Batch_Type_Master>().HasData(

//                new FGS_FIFO_Batch_Type_Master { Batch_Type_Id = 1, Batch_Type_Name = "BOOE", Batch_Type_Status = 1 },
//                new FGS_FIFO_Batch_Type_Master { Batch_Type_Id = 2, Batch_Type_Name = "BOEX", Batch_Type_Status = 1 },
//                new FGS_FIFO_Batch_Type_Master { Batch_Type_Id = 3, Batch_Type_Name = "BOAW", Batch_Type_Status = 1 }

//                );


//            modelBuilder.Entity<FGS_FIFO_Customer_Master>().HasData(

//                new FGS_FIFO_Customer_Master { Customer_Id = 1, Customer_Name = "Afghanistan", Customer_Address = "ADD1", Batch_Type_Id = 2, Customer_Type_Id = 1 },
//            new FGS_FIFO_Customer_Master { Customer_Id = 2, Customer_Name = "Australia", Customer_Address = "ADD2", Batch_Type_Id = 2, Customer_Type_Id = 1 },
//            new FGS_FIFO_Customer_Master { Customer_Id = 3, Customer_Name = "Azerbaijan", Customer_Address = "ADD3", Batch_Type_Id = 2, Customer_Type_Id = 1 },
//            new FGS_FIFO_Customer_Master { Customer_Id = 4, Customer_Name = "Bahrain", Customer_Address = "ADD4", Batch_Type_Id = 2, Customer_Type_Id = 1 },
//            new FGS_FIFO_Customer_Master { Customer_Id = 5, Customer_Name = "Bahrain", Customer_Address = "ADD4", Batch_Type_Id = 2, Customer_Type_Id = 1 }

//            );





//            modelBuilder.Entity <FGS_FIFO_Despatch_Status_Master > ().HasData(

//                new FGS_FIFO_Despatch_Status_Master { Desp_Status_Id = 1,	Desp_Status_Name = "Open"},
//			new FGS_FIFO_Despatch_Status_Master { Desp_Status_Id = 2,	Desp_Status_Name = "Partially dispatched" },
//			new FGS_FIFO_Despatch_Status_Master { Desp_Status_Id = 3,	Desp_Status_Name = "Cancel"},
//			new FGS_FIFO_Despatch_Status_Master { Desp_Status_Id = 4,	Desp_Status_Name = "Dispatched"}
						
//			);

//            modelBuilder.Entity<FGS_FIFO_Ageing_Master>().HasData(


//                new FGS_FIFO_Ageing_Master { Ageing_Id = 1, Material_Type_Id = 0, Material_Id = 1, Ageing_Months = 6, Status = 1 },
//        new FGS_FIFO_Ageing_Master { Ageing_Id = 2, Material_Type_Id = 0, Material_Id = 2, Ageing_Months = 5, Status = 1 },
//        new FGS_FIFO_Ageing_Master { Ageing_Id = 3, Material_Type_Id = 0, Material_Id = 3, Ageing_Months = 4, Status = 1 },
//        new FGS_FIFO_Ageing_Master { Ageing_Id = 4, Material_Type_Id = 0, Material_Id = 4, Ageing_Months = 4, Status = 1 },
//        new FGS_FIFO_Ageing_Master { Ageing_Id = 5, Material_Type_Id = 1, Material_Id = 6, Ageing_Months = 3, Status = 1 },
//        new FGS_FIFO_Ageing_Master { Ageing_Id = 6, Material_Type_Id = 1, Material_Id = 7, Ageing_Months = 6, Status = 1 }
//    );





//        }
    }
    
}
