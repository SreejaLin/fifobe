﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.TraceUsers
{
  public  class Trace_Module_MasterDTO
    {

        public int Id { get; set; }
        public int Module_Id { get; set; }
        public string Module_Name { get; set; }
        public int Application_Id { get; set; }
        public int Module_Status { get; set; }
    }
}
