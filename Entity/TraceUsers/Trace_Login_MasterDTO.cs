﻿using Model.TraceUsers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.TraceUsers
{
   public class Trace_Login_MasterDTO
    {
        public int Id { get; set; }
        public int Emp_Id { get; set; }
        public string User_Name { get; set; }
        public string Password { get; set; }
        public int Plant_Id { get; set; }
        public string UserRole { get; set; }
       public ICollection<Trace_Application_Employee_Mapping> ICTrace_Application_Employee_Mapping { get; set; }
    }
}
