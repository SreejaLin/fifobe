﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.TraceUsers
{
    public class Trace_Application_Employee_MappingDTO
    {


        public int Id { get; set; }
        public int Module_Id { get; set; }
        public int LogMastEmp_Id { get; set; }
        public int View { get; set; }
        public int Update { get; set; }
        public int Delete { get; set; }
        public int Add { get; set; }

    }
}
