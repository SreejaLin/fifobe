﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Model.Trace_Transactions;
using Model.Trace_Masters;

namespace Model.Trace_Transactions
{
    [Table("Trace_Material_Consumed_Actual")]
    public  class Trace_Material_Consumed_Actual
    {


        [Key]
        public int MC_Id { get; set; }
        [ForeignKey(nameof(Trace_Production))]
        [Required(ErrorMessage = "Production_Id is required")]
        public int Production_Id { get; set; }
        [ForeignKey(nameof(Trace_WorkCenter_Master))]
        [Required(ErrorMessage = "WorkCent_Id is required")]
        public int WorkCent_Id { get; set; }
        public DateTime? Production_Date { get; set; }
        [MaxLength(10)]
        public string? Production_Shift { get; set; }
     
        public int? Schedule_ID { get; set; }
        public int? Status { get; set; }
    }
}
