﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Model.Trace_Masters;

namespace Model.Trace_Transactions
{
    [Table("Trace_Production")]
    public class Trace_Production
    {


        [Key]
        public int Production_Id { get; set; }
        [ForeignKey(nameof(Trace_Material_Master))]
        [Required(ErrorMessage = "Material_Id is required")]
        public int Material_Id { get; set; }
        [ForeignKey(nameof(Trace_WorkCenter_Master))]
        [Required(ErrorMessage = "WorkCent_Id is required")]
        public int WorkCent_Id { get; set; }
        public DateTime? Prod_Date { get; set; }
        public DateTime? Prod_Time { get; set; }
        [MaxLength(250)]
        public string? CrewDetails { get; set; }
        [MaxLength(500)]
        public string? AdditionalFields { get; set; }
        [MaxLength(250)]
        public string? Remarks { get; set; }
        public int? Quantity { get; set; }
        public string? Unit_Of_Measurement { get; set; }
        [ForeignKey(nameof(Trace_MHE_Master))]
        [Required(ErrorMessage = "MHE_Id is required")]
        public int MHE_Id { get; set; }
        public int? Barcode_Id { get; set; }
        public int? Shift_Id { get; set; }
        public int? Status { get; set; }


    }
}
