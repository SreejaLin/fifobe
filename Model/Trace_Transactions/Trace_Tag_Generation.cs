﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Model.FGS_FIFO;
namespace Model.Trace_Transactions
{
    [Table("Trace_Tag_Generation")]
    public class Trace_Tag_Generation
    {
        [Key]
        public int TagId { get; set; }
        public string TagDetails { get; set; }
        public int Location_Id { get; set; }
        public string Printer_Name { get; set; }
        public string Printer_Ip { get; set; }
        public DateTime Printed_DateTime { get; set; }
        public string? Module_Name { get; set; }
        public int? User_Id { get; set; }
        public int Print_Status { get; set; }

       // [ForeignKey(nameof(FGS_FIFO_Daily_Receipt_Transaction))] 
        public int? InvTranId { get; set; }
      //  public FGS_FIFO_Daily_Receipt_Transaction FGS_FIFO_Daily_Receipt_Transaction { get; set; }

    }
}
