﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Model.Trace_Transactions;

namespace Model.Trace_Transactions
{
    [Table("Trace_Material_Consumed_Spec")]
    public class Trace_Material_Consumed_Spec
    {
//        MCS_Id
//   Production_Id
//Incoming_Material_Id
//Incoming_Material_Type


        [Key]
        public int MCS_Id { get; set; }
        [ForeignKey(nameof(Trace_Production))]
        [Required(ErrorMessage = "Production_Id is required")]
        public int Production_Id { get; set; }
        public int? Incoming_Material_Id { get; set; }
        [MaxLength(30)]
        public string? Incoming_Material_Type { get; set; }
        public int status { get; set; }

    }
}
