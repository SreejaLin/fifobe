﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Model.Trace_Masters;

namespace Model.Tread_FIFO
{
    [Table("Trace_Product_Ageing")]
   public class Trace_Product_Ageing
    {
  
        [Key]
        public int Product_Ageing_Id { get; set; }
        [ForeignKey(nameof(Trace_Material_Master))]
        [Required(ErrorMessage = "Material_Id is required")]
        public int? Material_ID { get; set; }
        public int? Ageing_Time { get; set; }
        public int? OverAgeing_Time { get; set; }
        [MaxLength(30)]
        public string? Ageing_Unit { get; set; }


    }
}
