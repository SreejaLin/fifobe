﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Model.Trace_Transactions;

namespace Model.Tread_FIFO
{
    [Table("Trace_Material_Transaction")]
    public class Trace_Material_Transaction
    {
        [Key]
        public int Material_Transaction_Id { get; set; }
        [ForeignKey(nameof(Trace_Production))]
        [Required(ErrorMessage = "Production_Id is required")]
        public int? Production_Id { get; set; }
        public int? Material_Status { get; set; }
        public DateTime? Start_Time { get; set; }
        public DateTime? End_Time { get; set; }
        [MaxLength(50)]
        public string? Created_By { get; set; }
        public DateTime? Created_Time { get; set; }
        public int? Trans_Status { get; set; }
    }
}
