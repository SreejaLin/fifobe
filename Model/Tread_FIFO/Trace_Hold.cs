﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Model.Trace_Transactions;

namespace Model.Tread_FIFO
{
    [Table("Trace_Hold")]
    public class Trace_Hold
    {
       
        [Key]
        public int Hold_Id { get; set; }

        [ForeignKey(nameof(Trace_Production))]
        [Required(ErrorMessage = "Production_ID is required")]
        public int Production_ID { get; set; }

        [ForeignKey(nameof(Trace_Hold_Reasons))]
        [Required(ErrorMessage = "Hold_Reason_Id is required")]
        public int? Hold_Reason_Id { get; set; }
        [MaxLength(50)]
        public string? Hold_By { get; set; }
        public DateTime? Hold_Time { get; set; }
        public DateTime? Hold_Date { get; set; }
        public int? Hold_Status { get; set; }



    }
}
