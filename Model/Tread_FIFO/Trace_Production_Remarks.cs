﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Model.Trace_Masters;

namespace Model.Tread_FIFO
{
    [Table("Trace_Prod_Remarks")]
    public class Trace_Production_Remarks
    {
        [Key]
        public int Remark_Id { get; set; }
        [Required(ErrorMessage = "Remarks is required")]
        [MaxLength(100)]
        public string Remarks { get; set; }
        //[ForeignKey(nameof(WorkCent_Id))]
        [ForeignKey(nameof(Trace_WorkCenter_Master))]
        [Required(ErrorMessage = "WorkCent_Id is required")]
        public int WorkCent_Id { get; set; }
        public int? Remark_Status { get; set; }
      
    }
}
