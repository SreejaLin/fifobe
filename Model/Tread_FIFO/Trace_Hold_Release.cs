﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Tread_FIFO
{
    [Table("Trace_Hold_Release")]
   public class Trace_Hold_Release
    {
      
        [Key]
        public int Hold_Release_Id { get; set; }
        [ForeignKey(nameof(Trace_Hold))]
        [Required(ErrorMessage = "Hold_Id is required")]
        public int Hold_Id { get; set; }
        [MaxLength(50)]
        public string? Realesed_By { get; set; }
        [Required(ErrorMessage = "Realesed_Count is required")]
        public int Realesed_Count { get; set; }
        public DateTime? Released_Time { get; set; }
        public DateTime? Realesed_date { get; set; }
        [MaxLength(250)]
        public string? Released_Reason { get; set; }

        public int status { get; set; }

    }
}
