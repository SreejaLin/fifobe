﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Model.Trace_Transactions;

namespace Model.Tread_FIFO
{
    [Table("Trace_Taken_ToTB")]
    public class Trace_Taken_ToTB
    {
              
        [Key]
        public int TakenToTb_Id { get; set; }
        [ForeignKey(nameof(Trace_Production))]
        [Required(ErrorMessage = "Production_Id is required")]
        public int? Production_Id { get; set; }
        public int? Ageing_Id { get; set; }
        public DateTime? TakenToTb_Date { get; set; }
        public DateTime? TakenToTB_Time { get; set; }
        [MaxLength(50)]
        public string? TakenToTB_By { get; set; }
        public int? TakenToTb_Status { get; set; }


    }
}
 