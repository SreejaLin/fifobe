﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Trace_Masters
{
    [Table("Trace_WorkCenter_Master")]
    public class Trace_WorkCenter_Master
    {


        [Key]
        public int WorkCent_Id { get; set; }

        public string? WorkCent_Name { get; set; }
        [ForeignKey(nameof(Trace_Plant_Master))]
        [Required(ErrorMessage = "Plant_Id is required")]
        public int Plant_Id { get; set; }
        public string? WorkCent_Type { get; set; }
        public int? Status { get; set; }

    }
}
