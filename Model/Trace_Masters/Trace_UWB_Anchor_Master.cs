﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Trace_Masters
{
    [Table("Trace_UWB_Anchor_Master")]
    public class Trace_UWB_Anchor_Master
    {

        [Key]
        public int UWB_Anchor_Id { get; set; }

        public string? UWD_Anchor_Name { get; set; }
        public string? UWD_Anchor_Make { get; set; }
        public DateTime? UWD_Anchor_Installed_Date { get; set; }
        public int? UWD_Anchor_Status { get; set; }
    }
}
