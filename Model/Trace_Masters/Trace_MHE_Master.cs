﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Trace_Masters
{
    [Table("Trace_MHE_Master")]
    public class Trace_MHE_Master
    {


        [Key]
        public int MHE_Id { get; set; }
        [ForeignKey(nameof(Trace_WorkCenter_Master))]
        [Required(ErrorMessage = "WorkCent_Id is required")]
        public int WorkCent_Id { get; set; }
        public string? MHE_Name { get; set; }
        public string? MHE_Type { get; set; }
        public int? MHE_Status { get; set; }
        public int? Max_Capacity { get; set; }
        public int? Unit_Of_Measurement { get; set; }

    }
}
