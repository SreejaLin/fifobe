﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Trace_Masters
{
    [Table("Trace_User_Type")]
   public class Trace_User_Type
    {
        [Key]
        public int User_Type_Id { get; set; }

        public string? User_Type_Name { get; set; }
    
        public int? User_Type_Status { get; set; }
    }
}
