﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Trace_Masters
{
    [Table("Trace_Material_Master")]
    public   class Trace_Material_Master
    {

        [Key]
        public int Material_Id { get; set; }

        public string? Material_Code { get; set; }
        [ForeignKey(nameof(Trace_WorkCenter_Master))]
        [Required(ErrorMessage = "WorkCent_Id is required")]
        public int WorkCent_Id { get; set; }
        public string? Material_Type { get; set; }
        public DateTime Material_CreationDate { get; set; }
        public string? Material_CreatedBy { get; set; }
        public int Status { get; set; }
       

    }
}
