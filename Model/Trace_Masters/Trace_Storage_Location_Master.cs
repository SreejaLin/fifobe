﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Trace_Masters
{
    [Table("Trace_Storage_Location_Master")]
    public class Trace_Storage_Location_Master
    {

        [Key]
        public int Storage_Loc_Id { get; set; }

        public string? ShopFloorLocation_Id { get; set; }
        public string? Storage_Loc_Name { get; set; }
  
    }
}
