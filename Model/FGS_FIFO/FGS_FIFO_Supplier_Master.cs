﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Model.FGS_FIFO
{
    [Table("FGS_FIFO_Supplier_Master")]
    public class FGS_FIFO_Supplier_Master

    {

        [Key]
        public int Supplier_Id { get; set; }

        [Required(ErrorMessage = "Supplier_Code is required")]
        [StringLength(100, ErrorMessage = "Supplier_Code can't be longer than 100 characters")]
        public string Supplier_Code { get; set; }

        [Required(ErrorMessage = "Supplier_Name is required")]
        [StringLength(100, ErrorMessage = "Supplier_Name cannot be longer than 100 characters")]
        public string Supplier_Name { get; set; }

        [Required(ErrorMessage = "Supplier_Contact_No is required")]
        [StringLength(50, ErrorMessage = "Supplier_Contact_No cannot be longer than 50 characters")]
        public string? Supplier_Contact_No { get; set; }

        [Required(ErrorMessage = "EntryDate is required")]
        public DateTime EntryDate { get; set; }

        public int Supplier_Status { get; set; }
    }
}





