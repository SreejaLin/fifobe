﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Model.FGS_FIFO
{
    [Table("FGS_FIFO_Daily_Receipt_Transaction")]
    public class FGS_FIFO_Daily_Receipt_Transaction

    {

        [Key]
        public int Inv_TransId { get; set; }

        [Required(ErrorMessage = "Barcode is required")]
        [StringLength(100, ErrorMessage = "Barcode can't be longer than 100 characters")]
        public string Barcode { get; set; }

        [ForeignKey(nameof(FGS_FIFO_Supplier_Master))]
        [Required(ErrorMessage = "Supplier_Id is required")]
        public int Supplier_Id { get; set; }
     
        [ForeignKey(nameof(FGS_FIFO_Material_Master))]
        [Required(ErrorMessage = "Material_Id is required")]
        public int Material_Id { get; set; }
    
        [ForeignKey(nameof(FGS_FIFO_Batch_Type_Master))]
        [Required(ErrorMessage = "Batch_Type_Id is required")]
        public int Batch_Type_Id { get; set; }
     
        [Required(ErrorMessage = "Quantity is required")]
        public int Quantity { get; set; }

        [Required(ErrorMessage = "Invoice_No is required")]
        [StringLength(150, ErrorMessage = "Invoice_No cannot be longer than 150 characters")]
        public string Invoice_No { get; set; }

        [Required(ErrorMessage = "Invoice_Date is required")]
        public DateTime Invoice_Date { get; set; }

        [ForeignKey(nameof(FGS_FIFO_Location_Master))]
        [Required(ErrorMessage = "Location_Id is required")]
        public int Location_Id { get; set; }
      
        [Required(ErrorMessage = "ProdDate is required")]
        public DateTime ProdDate { get; set; }
        
        [Required(ErrorMessage = "Exp_Date is required")]
        public DateTime Exp_Date { get; set; }
       
        [Required(ErrorMessage = "EntryDate is required")]
        public DateTime EntryDate { get; set; }

        
        public string Entered_By { get; set; }
      

        public int Status { get; set; }

        public virtual FGS_FIFO_Supplier_Master FGS_FIFO_Supplier_Master  { get; set;     }

        public virtual FGS_FIFO_Material_Master FGS_FIFO_Material_Master   {get;   set;  }

        public virtual FGS_FIFO_Batch_Type_Master FGS_FIFO_Batch_Type_Master { get; set; }

        public virtual FGS_FIFO_Location_Master FGS_FIFO_Location_Master { get; set; }




    }
}







