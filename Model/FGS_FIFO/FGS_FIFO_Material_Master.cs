﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Model.FGS_FIFO
{

    [Table("FGS_FIFO_Material_Master")]
    public class FGS_FIFO_Material_Master
    {
        [Key]
        public int Material_Id { get; set; }
        [Required(ErrorMessage = "Material ID is required")]
        public string Material_Code { get; set; }
        [Required(ErrorMessage = "Material Code is required")]
        public string Material_Description { get; set; }
        [Required(ErrorMessage = "Material_Description is required")]
        [ForeignKey(nameof(FGS_FIFO_Material_Type_Master))]
        public int Material_Type_Id { get; set; }
        [Required(ErrorMessage = "Material_Type_Id is required")]
        public DateTime EntryDate { get; set; }
        [Required(ErrorMessage = "Entry Date is required")]
        public int Material_Status { get; set; }
       
       public FGS_FIFO_Material_Type_Master FGS_FIFO_Material_Type_Master { get; set; }

    }

    } 
