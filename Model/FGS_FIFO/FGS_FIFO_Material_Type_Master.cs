﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.FGS_FIFO
{
    [Table("FGS_FIFO_Material_Type_Master")]
    public class FGS_FIFO_Material_Type_Master
    {
        [Key]
        public int Material_Type_Id { get; set; }
        [Required(ErrorMessage = "Material_Type_Id is required")]
        public string Material_Type_Name { get; set; }
        [Required(ErrorMessage = "Material_Type_Name is required")]
        public int Material_Type_Status { get; set; }



    }
}
