﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Model.FGS_FIFO
{
    [Table("FGS_FIFO_Cust_Ageing_Mapping")]
    public class FGS_FIFO_Cust_Ageing_Mapping

    {
        [Key]
        public int Cust_Ageing_Id { get; set; }

        [ForeignKey(nameof(FGS_FIFO_Customer_Master))]
        [Required(ErrorMessage = "Customer_Id is required")]
        public int Customer_Id { get; set; }
        public FGS_FIFO_Customer_Master FGS_FIFO_Customer_Master { get; set; }
        [ForeignKey(nameof(FGS_FIFO_Ageing_Master))]
        [Required(ErrorMessage = "Ageing_Id is required")]
        public int Ageing_Id { get; set; }
        public FGS_FIFO_Ageing_Master FGS_FIFO_Ageing_Master { get; set; }

        [Required(ErrorMessage = "EntryDate is required")]
        public DateTime? EntryDate { get; set; }

        public int Ageing_Status { get; set; }


    }
}



