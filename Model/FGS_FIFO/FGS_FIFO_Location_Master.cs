﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Model.FGS_FIFO
{
    [Table("FGS_FIFO_Location_Master")]
    public class FGS_FIFO_Location_Master

    {

        [Key]
        public int Location_Id { get; set; }

        [Required(ErrorMessage = "Location_Name is required")]
        [StringLength(100, ErrorMessage = "Location_Name cannot be longer than 100 characters")]
        public string Location_Name { get; set; }

        [Required(ErrorMessage = "Location_Is_Empty is required")]
        public int? Location_Is_Empty { get; set; }

        public int? Location_Status { get; set; }
    }


}



