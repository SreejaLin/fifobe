﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Model.FGS_FIFO
{
    [Table("FGS_FIFO_Customer_Type_Master")]
    public class FGS_FIFO_Customer_Type_Master

    {
        [Key]
        public int Customer_Type_Id { get; set; }
   
        [Required(ErrorMessage = "Customer_Type_Name is required")]
        [StringLength(100, ErrorMessage = "Customer_Type_Name cannot be longer than 100 characters")]
        public string? Customer_Type_Name { get; set; }
                                 
        public int? Customer_Type_Status { get; set; }
    }
}





