﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Model.FGS_FIFO
{
    [Table("FGS_FIFO_Batch_Type_Master")]
    public class FGS_FIFO_Batch_Type_Master


    {
        [Key]
        public int Batch_Type_Id { get; set; }
   
        [Required(ErrorMessage = "Batch_Type_Name is required")]
        [StringLength(100, ErrorMessage = "Batch_Type_Name cannot be longer than 100 characters")]
        public string Batch_Type_Name { get; set; }
     
        public int? Batch_Type_Status { get; set; }
    }
}

