﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.TraceUsers
{
    [Table("Trace_Application_Master")]
    public class Trace_Application_Master
    {
   

        [Key]
        public int Id { get; set; }
        public int Application_Id { get; set; }
        public string Application_Name { get; set; }
        public int Application_Status { get; set; }
        public int Plant_Id { get; set; }

    }
}
