﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.TraceUsers
{
    [Table("Trace_Module_Master")]
    public   class Trace_Module_Master
    {
        

        [Key]
        public int Id { get; set; }
        public int Module_Id { get; set; }
        public string Module_Name { get; set; }
        public int Application_Id { get; set; }
        public int Module_Status { get; set; }
    }
}
