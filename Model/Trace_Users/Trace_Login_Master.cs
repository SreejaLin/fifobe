﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.TraceUsers
{
    [Table("Trace_Login_Master")]
    public class Trace_Login_Master
    {
       

        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "Emp_Id is required")]


        public int Emp_Id { get; set; }
        public string User_Name { get; set; }
        public string Password { get; set; }
        public int Plant_Id { get; set; }
        public string UserRole { get; set; }
      public ICollection<Trace_Application_Employee_Mapping> AppEmpMapRelation { get; set; }

    }
}
